<?php
/* SVN FILE: $Id: test_plugin_app_controller.php,v 1.1 2010-03-19 15:41:00 diegoq Exp $ */
/**
 * Test Suite TestPlugin AppController
 *
 * Long description for file
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) Tests <https://trac.cakephp.org/wiki/Developement/TestSuite>
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 *  Licensed under The Open Group Test Suite License
 *  Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          https://trac.cakephp.org/wiki/Developement/TestSuite CakePHP(tm) Tests
 * @package       cake
 * @subpackage    cake.tests.cases.libs
 * @since         CakePHP(tm) v 1.2.0.5432
 * @version       $Rev$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date: 2010-03-19 15:41:00 $
 * @license       http://www.opensource.org/licenses/opengroup.php The Open Group Test Suite License
 */
class TestPluginAppController extends AppController { }
?>