<?php
/* SVN FILE: $Id: test_plugin_comment.php,v 1.1 2010-03-19 15:41:00 diegoq Exp $ */
/**
 * Test App Comment Model
 *
 *
 *
 * PHP versions 4 and 5
 *
 * CakePHP :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2006-2008, Cake Software Foundation, Inc.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2006-2008, Cake Software Foundation, Inc.
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP Project
 * @package       cake
 * @subpackage    cake.cake.libs.
 * @since         CakePHP v 1.2.0.7726
 * @version       $Revision: 1.1 $
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date: 2010-03-19 15:41:00 $
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
class TestPluginComment extends TestPluginAppModel {
	var $useTable = 'comments';
	var $name = 'TestPluginComment';
}
?>