<?php
/* SVN FILE: $Id: sequencial_nocache.ctp,v 1.1 2010-03-19 15:40:56 diegoq Exp $ */
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.pages
 * @since         CakePHP(tm) v 0.10.0.1076
 * @version       $Revision: 1.1 $
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date: 2010-03-19 15:40:56 $
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
?>
<h1>Content</h1>
<cake:nocache>
	<p>D. In View File</p>
	<?php $this->log('4. in view file') ?>
</cake:nocache>
