<?php
/* SVN FILE: $Id: persister_one.php,v 1.1 2010-03-19 15:40:59 diegoq Exp $ */
/**
 * Test App Comment Model
 *
 *
 *
 * PHP versions 4 and 5
 *
 * CakePHP :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2006-2010, Cake Software Foundation, Inc.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2006-2010, Cake Software Foundation, Inc.
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP Project
 * @package       cake
 * @subpackage    cake.tests.test_app.models
 * @since         CakePHP v 1.2.0.7726
 * @version       $Revision: 1.1 $
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date: 2010-03-19 15:40:59 $
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
class PersisterOne extends AppModel {
	var $useTable = 'posts';
	var $name = 'PersisterOne';

	var $actsAs = array('PersisterOneBehavior', 'TestPlugin.TestPluginPersisterOne');

	var $hasMany = array('Comment', 'TestPlugin.TestPluginComment');
}
?>