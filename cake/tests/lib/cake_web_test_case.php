<?php
/* SVN FILE: $Id: cake_web_test_case.php,v 1.1 2010-03-19 15:40:55 diegoq Exp $ */
/**
 * CakeWebTestCase a simple wrapper around WebTestCase
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) Tests <https://trac.cakephp.org/wiki/Developement/TestSuite>
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 *  Licensed under The Open Group Test Suite License
 *  Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          https://trac.cakephp.org/wiki/Developement/TestSuite CakePHP(tm) Tests
 * @package       cake
 * @subpackage    cake.cake.tests.lib
 * @since         CakePHP(tm) v 1.2.0.4433
 * @version       $Revision: 1.1 $
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date: 2010-03-19 15:40:55 $
 * @license       http://www.opensource.org/licenses/opengroup.php The Open Group Test Suite License
 */
/**
 * Ignore base class.
 */
	SimpleTest::ignore('CakeWebTestCase');
/**
 * Simple wrapper for the WebTestCase provided by SimpleTest
 *
 * @package       cake
 * @subpackage    cake.cake.tests.lib
 */
class CakeWebTestCase extends WebTestCase {
}
?>