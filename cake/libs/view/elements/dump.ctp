<?php
/* SVN FILE: $Id: dump.ctp,v 1.1 2010-03-19 15:41:02 diegoq Exp $ */
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.elements
 * @since         CakePHP(tm) v 0.10.5.1782
 * @version       $Revision: 1.1 $
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date: 2010-03-19 15:41:02 $
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
?>
<div id="cakeControllerDump">
	<h2><?php __('Controller dump:'); ?></h2>
	<pre>
		<?php echo h(print_r($controller, true)); ?>
	</pre>
</div>