<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


?>

<form  id="form_listado" method="post" onsubmit="return false;" action="<?=$html->url("/listado/guardar/".$consulta_id) ?>">
<div class="ui-widget">	
	<?= $form->input("Listado.nombre",array('type' => 'text','label' => 'Nombre')); ?>
	<?= $form->input("Listado.descripcion",array('type' => 'textarea','label' => 'Descripción')); ?>
	
	<?= $form->submit("Guardar", array("class"=>"ui-state-default ui-corner-all")); ?>
</div>	
</form>
