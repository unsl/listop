<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


?>


	<?php if(count($listados)>0): ?>
	
	<div class="button_link" id="links_lab">  
		<a href="<?=$html->url("/lab") ?>" title="Consultas" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-wrench"></span>Lab</a>
	</div>
	<br>
	<div>
	<table id="tabla_listados" width="98%">
		<thead>
			<tr>
				<th>Fecha</th>
				<th>Nombre</th>
				<th>Descripcion</th>
				<th>Op.</th>
			</tr>
		</thead>
		
		<tbody>
			<?php if($listados) foreach($listados as $l): ?>
			<tr>	
				<td><?= $l['Listado']['created'] ?></td>
				<td><a class="link_ver_listado" href="<?= $html->url("/listado/ver/".$l['Listado']['id']) ?>"><?= $l['Listado']['nombre'] ?> </a></td>
				<td><?= $l['Listado']['descripcion'] ?>&nbsp;</td>
				<td>					 
					<a class="link_borrar_listado" href="<?= $html->url("/listado/borrar/".$l['Listado']['id']) ?>" title="Borrar" > Borrar </a>
				</td>
			</tr>
			<?php endforeach; ?>
			
		</tbody>
	</table>
	</div>
	<?php endif; ?>

	
	<script type="text/javascript" charset="UTF-8">
		$("#tabla_listados").ready( function() {
			$("#tabla_listados").dataTable(
				{
					"sScrollY": 200,
					"sScrollX": "100%",
					//"sScrollXInner": "110%",
					"bProcessing": false,
					"bServerSide": false,
					"sPaginationType":"full_numbers",
					"bFilter": true,
					"bSort": true,
					"bJQueryUI": true
					 
				}
			);

			$(".link_ver_listado").live('click',
					function () {
						$("#listados_box").load(this.href);
						return false;		
					}
			);
			
			$(".link_borrar_listado").live('click',
					function () {
						if (confirm("¿Confirma que quiere eliminar el listado? \n No es posible deshacer esta operación.")) {
							$("#listados_box").load(this.href);
						}
						return false;		
					}
			);	
		});

		function borrar_lista() {

		}
		
	</script>
	