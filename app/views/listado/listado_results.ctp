<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


?>

<style type="text/css">
	/*demo page css*/
	
	.button_link a {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
	.button_link a span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}	
</style>	


<div class="button_link" id="links_listado_results">
	<a id="link_lista_listados" onclick= "$('#listados_box').load(this.href); return false;" href="<?=$html->url("/listado/lista_usuario/")?>" title="Volver a la lista." class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-circle-triangle-w"></span>Lista</a>  
	<a href="<?=$html->url("/listado/excel/".$listado_id) ?>" title="Descargar el resultado en una planilla." class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-circle-arrow-s"></span>Descargar</a>
</div>

<br>

<table id="listado_table" width="100%">
<thead> 
	<tr>
	<?php
		//Encabezado de la tabla
		foreach($results[0] as $col => $val) :
			echo "<th>".$col."</th>";
		endforeach;
	?>
	</tr>
</thead>

<tbody>
	<?php foreach($results as $r): ?>
	<tr>
	<?php foreach($r as $c => $v): ?>
			<td><?= $v ?></td>	
	<?php endforeach; ?>
	</tr>
	<?php endforeach; ?>
</tbody>

</table>


<script type="text/javascript">

function makeDataTable_listado() {
	
	$('#listado_table').dataTable({
			 "bProcessing": true,
			 "bServerSide": true,
			 "sPaginationType":"full_numbers",
			 "bFilter": false,
			 "bProcessing": true,
			 "bSort": false,
			 "bJQueryUI": true,  
			 "sAjaxSource": "<?= $html->url("/listado/dataTableResults/".$listado_id) ?>",
			 "fnServerData": function ( sSource, aoData, fnCallback ) {
								$.ajax( {
									"dataType": 'json', 
									"type": "POST", 
									"url": sSource, 
									"data": aoData, 
									"success": fnCallback
								} );
							}
											 
		 	}
 	);


}

$("#listado_table").ready(function (){makeDataTable_listado();});


</script>