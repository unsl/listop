<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="65%" class="texto_logo" > <b>Consultas y Estadisticas</b> </td>

            <td width="23%" class="button_link">
            	<?php if(isset($usuario)): ?>
                	<?= $usuario ?>
                <?php endif; ?>
            </td>
            <td align="center" class="button_link">
            	
	            <a  href="<?= $html->url("/lista_consultas") ?>" title="Lista de consultas." class="ui-state-default ui-corner-all">
	                <span class="ui-icon ui-icon-home ui-state-default ui-corner-all"></span>
	            </a>    
                
            </td>
            <td width="6%" align="center" class="button_link">
                <a href="<?= $html->url("/usuario/logout") ?>" title="Salir del sistema." class="ui-state-default ui-corner-all">
                	<span class="ui-icon ui-icon-power ui-state-default ui-corner-all"></span>
                </a>
            </td>
        </tr>
</table>