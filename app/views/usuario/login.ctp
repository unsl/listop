<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


?>

<!-- 
<div align="center">
	<form method="post" class="formulario" id="login_form" action="<?= $html->url("/usuario/login") ?>">
	
		
		<table>
			<tr>
				<td>Usuario:</td><td><?= $form->text("Usuario.usuario",array('id'=>'Usuario.usuario')) ?></td>
			</tr>
			<tr>
				<td>Contrase&ntilde;a:</td><td><?php echo $form->password('Usuario.password', array('size' => 20,'id'=>'Cuenta.password','value'=>'')); ?></td>
			</tr>
			<?php if(isset($ERROR) && $ERROR):?>
			<tr>
				<td colspan="2" class="error-message"><?= $ERROR ?></td>
			</tr>
			<?php endif;?>
			<tr>
				<td colspan="2" align="right"><?= $form->submit("Entrar")?></td>
			</tr>
		</table>
	
	</form>
</div>
-->


<!-- 
<div class="ui-overlay">
	<div class="ui-widget-overlay"></div>
	<div class="ui-widget-shadow ui-corner-all" style="width: 302px; height: 152px; position: absolute; left: 50px; top: 30px;"></div>
</div>
-->

<!--
<div style="position: absolute; width: 280px; height: 130px;left: 50px; top: 30px; padding: 10px;" class="ui-widget ui-widget-content ui-corner-all">
-->
<div align="center">
<div style=" width: 280px; height: 130px;left: 50px; top: 30px; padding: 10px;" class="ui-widget ui-widget-content ui-corner-all">
		<div class="ui-dialog-content ui-widget-content" style="background: none; border: 2;">
			
			<form method="post" class="formulario" id="login_form" action="<?= $html->url("/usuario/login") ?>">
				<table>
					<tr>
						<td>Usuario:</td><td><?= $form->text("Usuario.usuario",array('id'=>'Usuario.usuario')) ?></td>
					</tr>
					<tr>
						<td>Contrase&ntilde;a:</td><td><?php echo $form->password('Usuario.password', array('size' => 20,'id'=>'Cuenta.password','value'=>'')); ?></td>
					</tr>
					<?php if(isset($ERROR) && $ERROR):?>
					<tr>
						<td colspan="2" class="error-message"><?= $ERROR ?></td>
					</tr>
					<?php endif;?>
					<tr>
						<td colspan="2" align="right"><?= $form->submit("Entrar")?></td>
					</tr>
				</table>
			
			</form>
		</div>
</div>
</div>



