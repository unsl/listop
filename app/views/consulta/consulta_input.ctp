<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


?>



<form  id="form_consulta" method="post" onsubmit="return false;" action="<?=$html->url("/consulta/input/".$consulta_id) ?>" enctype="multipart/form-data">

	<?php if(count($input_par)): ?>
	<fieldset>
		<legend>Parametros</legend>
		<table>
		
		<?php foreach($input_par as $p): ?>
		<tr>
			<td>
				<?= $form->input($p['field'],$p); ?>
			</td>
		</tr>
		<?php endforeach; ?>
		</table>
	</fieldset>
	<br>
	<?php endif; ?>
	
	<?php if(count($filter_options)): ?>
	<fieldset>
		<legend>Opciones</legend>
		<div id='div_opciones'>
			<table>
			<?php foreach($filter_options as $o): ?>
			<tr>
				<td>
					<?= $form->input($o['field'],$o); ?>
				</td>
			</tr>
			<?php endforeach; ?>
			</table>
		</div>
	</fieldset>
	<?php endif; ?>
	
	<?php if(isset($cache_param)): ?>
	
		<div>
			<?= $form->input($cache_param['field'],$cache_param); ?>
		</div>
		
	<?php endif; ?>
	
	<?php if(isset($model)):
		echo $form->hidden($model.".name");		
	 endif; ?>
	<div>
		<?= $form->submit("Ejecutar Consulta"); ?>
	</div>
	
</form>

<script type="text/javascript" charset="UTF-8">

	<?php if($showResults): ?>
		<?php if($showNoData): ?>
			$("#form_consulta").ready(showNoData);
		<?php else: ?>
			$("#form_consulta").ready(enableResults);
		<?php endif; ?>	
	<?php else: ?>
		$("#form_consulta").ready(disableResults);
	<?php endif; ?>


	$("[data-cascade-parent]").each(function() {
			var consulta_id = <?= $consulta_id ?>;
			var p_id = "#" + $(this).attr("data-cascade-parent");
			var data_url = URL_BASE + "consulta/cascade_parent_change/"+ consulta_id + "/" + $(this).attr("id");
			
			$(this).cascade(p_id, {						
						ajax: { 
							url: data_url,
							data: $("#form_consulta").serializeArray(),
					    },
		    			template: function(item) {	return "<option value='" + item.Value + "'>" + item.Text + "</option>";}
					    			  			
					}
			);
		}
	);

	$("[data-input-date]").each(function() {
		$(this).datepicker(
				{ 
					dateFormat: $(this).attr("data-input-date"),
					changeMonth: true,
					changeYear: true,
					appendText: '(' + $(this).attr("data-input-date") +')'
				 });
	});
	
</script>