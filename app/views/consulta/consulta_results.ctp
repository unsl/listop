<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


?>

<script type="text/javascript">

function makeAjaxForm_listado() {
	var options = {
	        target: "#marco_guardar",
	        success: makeAjaxForm_listado
	};

	$('#marco_guardar form').ajaxForm(options);
}

function cerrarDialogoGuardar() {
	$('#dialogo_guardar').dialog('close');
}

$(function(){
	$('#dialogo_guardar').dialog({
		autoOpen: false, 
		modal: true, 
		resizable: true, 
		closeOnEscape: false, 
		draggable: true
	
	});

	$('#link_guardar').click(function(){

		var url = this.href;

		$('#marco_guardar').load(url,function(){
			makeAjaxForm_listado();
			$('#dialogo_guardar').dialog('open');
		});
		
		return false;
	});
});
</script>

<div id="dialogo_guardar">
	<div id="marco_guardar">
		Aca va el contenido del form guardar!
	</div>
</div>



<div class="button_link"> 
<!-- 
<a href="<?=$html->url("/consulta/excel/".$consulta_id)?>" title="Descargar el resultado en una planilla."><?= $html->image('planilla.png', array('alt' => 'Descargar resultados en una planilla.','border'=>0)) ?></a>
 -->

<a href="<?=$html->url("/consulta/excel/".$consulta_id)?>" title="Descargar el resultado en una planilla." class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-circle-arrow-s"></span>Descargar</a>  
<a href="<?=$html->url("/listado/guardar/".$consulta_id)?>" id="link_guardar" title="Guardar lista de resultados." class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-disk"></span>Guardar</a>

</div>

<br>
Ultima actualizacion: <?= $cachetime ?>

<div>
<table id="results_table" width="100%">
<thead> 
	<tr>
	<?php
		//Encabezado de la tabla
		foreach($results[0] as $col => $val) :
			echo "<th>".$col."</th>";
		endforeach;
	?>
	</tr>
</thead>
<tbody>
<!-- 
	<?php foreach($results as $r): ?>
	<tr>
	<?php foreach($r as $c => $v): ?>
			<td><?= $v ?></td>	
	<?php endforeach; ?>
	</tr>
	<?php endforeach; ?>
 -->
</tbody>

</table>
</div>
