<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


?>

<script type="text/javascript">
			
			function makeAjaxForm_consulta(){
				var options = {
			        target: '#consulta_input',
			        beforeSubmit: function (formData, jqForm, options) {showWorking();},
			        success: formReturnCallback
			    };
			    
			    $('#form_consulta').ajaxForm(options);
			}
			
			function enableResults(){
				$('#consulta_results').load(
					'<?= $html->url("/consulta/results/".$consulta_id) ?>', null, makeDataTable
					
				);
			}	
			
			function formReturnCallback() {
				makeAjaxForm_consulta();
				hideWorking();
			}
			
			function makeDataTable() {

				$('#tabs').tabs('enable', 1);
				$('#tabs').tabs('select', 1);

				$('#results_table').dataTable( {
						 "bProcessing": true,
						 "bServerSide": true,
						 "sPaginationType":"full_numbers",
						 "bFilter": false,
						 "bProcessing": true,
						 "bSort": false,
						 "bJQueryUI": true,  
						 "sAjaxSource": "<?= $html->url("/consulta/dataTableResults/".$consulta_id) ?>",
						 "fnServerData": function ( sSource, aoData, fnCallback ) {
											$.ajax( {
												"dataType": 'json', 
												"type": "POST", 
												"url": sSource, 
												"data": aoData, 
												"success": fnCallback
											} );
										},
							//"bAutoWidth": true,
							"sScrollY": "100px",
							"bScrollCollapse": true,
							//"bScrollInfinite": false,
		        			"sScrollX": "100%",
		        			"sScrollXInner": "300%"
														 
					 	} );
				
			}
			
			function disableResults(){
				$('#tabs').tabs('disable', 1);	
			}
			
			function showWorking(){
				$('#working_dialog').dialog("open");
			}

			function showNoData(){
				$('#nodata_dialog').dialog("open");
			}
			
			function hideWorking(){
				$('#working_dialog').dialog("close");
			}
			
			$(document).ready(function() {
				$('#tabs').tabs();
				$('#tabs').tabs('disable', 1);
				$('#working_dialog').dialog({
						autoOpen: false, 
						modal: true, 
						resizable: false, 
						closeOnEscape: false, 
						draggable: false,
						open: function() {
							$(this).parents(".ui-dialog:first").find(".ui-dialog-titlebar-close").remove();
						}
				});
				
				$('#nodata_dialog').dialog({
					autoOpen: false, 
					modal: true, 
					resizable: false, 
					closeOnEscape: false, 
					draggable: false,
					buttons: {
						"Aceptar": function() { 
							$(this).dialog("close"); 
						}
					}
				
				});
				makeAjaxForm_consulta();
			});			
					
</script>

<?= $this->requestAction("/consulta/path/".$consulta_id) ?>



<div id="working_dialog" title="Procesando..." >
	<p><strong>Su consulta esta siendo procesada.</strong></p>
	<?= $html->image("bar-loader.gif",array('border'=>0)) ?>
	<p><strong>Por favor aguarde...</strong></p>
</div>

<div id="nodata_dialog" title="Sin Resultados" >
	<div class="ui-widget">
		<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
			<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<strong>Su consulta no obtuvo resultados.</strong> </p>
		</div>

	</div>
	
</div>

<div id="tabs">
	<ul>
		<li><a href="#tabs-c">Opciones de Consulta</a></li>
		<li><a href="#tabs-r">Resultados</a></li>		
	</ul>
	
	<div id="tabs-c">
		<div id="consulta_input">
			<?= $this->requestAction("/consulta/input/".$consulta_id) ?>
		</div>
	</div>
	
	<div id="tabs-r">
		<div id="consulta_results">
		
		</div>
	</div>		
</div>


