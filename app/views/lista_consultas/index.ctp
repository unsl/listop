<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


?>

<div id="tabs">
	<ul>
		<li><a href="#tabs-c">Consultas Generales</a></li>
		<li><a href="#tabs-l">Listados del Usuario</a></li>
		<li><a href="#tabs-u">Consultas del Usuario</a></li>		
	</ul>



<div id="tabs-c">
<?php if(count($consultas)>0): ?>

<table id="tabla_consultas" width="98%">
<thead>
	<tr>
		<th>Consulta</th>
		<th>Descripcion</th>
	</tr>
</thead>

<tbody>
	<?php if($consultas) foreach($consultas as $c): ?>
	<tr>	
		<td><a href="<?= $html->url("/consulta/index/".$c['Consulta']['id']) ?>"><?= $c['Consulta']['nombre'] ?> </a></td>
		<td><?= $c['Consulta']['descripcion'] ?>&nbsp;</td>
	</tr>
	<?php endforeach; ?>
	
</tbody>

</table>

<?php endif; ?>
</div>

<div id="tabs-l">
	<div id="listados_box">
	<?php echo $this->requestAction("/listado/lista_usuario"); ?>
	</div>
</div>

<div id="tabs-u">
<?php if(count($consultasGrupos)>0): ?>

<table id="tabla_consultasG" width="98%">
<thead>
	<tr>
		<th>Consulta</th>
		<th>Descripcion</th>
	</tr>
</thead>

<tbody>
	<?php if($consultas) foreach($consultasGrupos as $c): ?>
	<tr>	
		<td><a href="<?= $html->url("/consulta/index/".$c['Consulta']['id']) ?>"><?= $c['Consulta']['nombre'] ?> </a></td>
		<td><?= $c['Consulta']['descripcion'] ?>&nbsp;</td>
	</tr>
	<?php endforeach; ?>
	
</tbody>
</table>
<?php endif; ?>

</div>






</div>

<script type="text/javascript" charset="UTF-8">

	
	$("#tabla_consultasG").ready( function() {
		$("#tabla_consultasG").dataTable(
			{
				"bProcessing": false,
				"bServerSide": false,
				"sPaginationType":"full_numbers",
				"bFilter": true,
				"bSort": true,
				"bJQueryUI": true,  
			}
		);
		
	});

	$("#tabla_consultas").ready( function() {
		$("#tabla_consultas").dataTable(
			{
				"bProcessing": false,
				"bServerSide": false,
				"sPaginationType":"full_numbers",
				"bFilter": true,
				"bSort": true,
				"bJQueryUI": true,  
			}
		);
		
	});

	

	$(function(){$('#tabs').tabs();});
</script>
