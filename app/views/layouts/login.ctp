<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


?>

<html>

<head>
	<title>Consultas Estadisticas</title>

	<link type="text/css" href="<?php echo $html->url("/js/jquery-ui/css/ui-lightness/jquery-ui-1.8rc3.custom.css"); ?>" rel="stylesheet" />
	<link type="text/css" href="<?php echo $html->url("/js/dataTables-1.6/media/css/demo_table_jui.css"); ?>" rel="stylesheet" />
	<link type="text/css" href="<?php echo $html->url("/css/estadistica.css"); ?>" rel="stylesheet" />
	<?php echo $javascript->link("jquery-ui/js/jquery-1.4.2.min"); ?>
	<?php echo $javascript->link("jquery-ui/js/jquery-ui-1.8rc3.custom.min"); ?>
	<?php echo $javascript->link("jquery.form"); ?>
	<?php echo $javascript->link("dataTables-1.6/media/js/jquery.dataTables"); ?>
</head>
<body >

<div id="encabezado">
	
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="65%" class="texto_logo" > Consultas y Estadisticas </td>

            <td width="23%">
            	
            </td>
            <td width="6%" align="center">
                
            </td>
            <td width="6%" align="center">
                
            </td>
        </tr>
    </table>
</div>
<hr>

<div id="contenido">
	<?= $content_for_layout ?>
</div>


<div id="pie">
UNSL
</div>
</body>
</html>