<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


?>

<div class="button_link" id="links_listado_results">  
	<a href="<?=$html->url("/lab/excel") ?>" title="Descargar el resultado en una planilla." class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-circle-arrow-s"></span>Descargar</a>
	<a href="#" id="link_guardar" title="Guardar lista de resultados." class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-disk"></span>Guardar</a>
</div>

<br>

<table id="lab_listado_table" width="98%">
<thead> 
	<tr>
	<?php
		//Encabezado de la tabla
		foreach($results[0] as $col => $val) :
			echo "<th>".$col."</th>";
		endforeach;
	?>
	</tr>
</thead>

<tbody>
	<?php foreach($results as $r): ?>
	<tr>
	<?php foreach($r as $c => $v): ?>
			<td><?= $v ?></td>	
	<?php endforeach; ?>
	</tr>
	<?php endforeach; ?>
</tbody>

</table>