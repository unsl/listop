<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


?>

<?php echo $javascript->link("app/lab/lab"); ?>

<style type="text/css">
		
		#tabla_listas {
			width: 98%;
		}
		#tabla_listas tr td {
			width: 50%;
		}
		
		.caja_listas {
			width: 99%;
			height: 250px; 
			padding: 1px;
			overflow: auto;
			margin-top: 0px;
		}
		
		.ventana_listas {
			width: 98%;
			padding: 0px;
			border: 1px solid;
			
		}
		
		.lab-lista-icon {
			float:right;
		}
		.lab-lista {
			list-style: none; width: 350px; padding: 0px; margin:0px;
		}
		
		
		#tool_bar_lab ul {margin: 0; padding: 0;}
		#tool_bar_lab ul li {margin: 2px; position: relative; padding: 4px; cursor: pointer; float: left;  list-style: none;}
		#tool_bar_lab ul span.ui-icon {float: left; margin: 0 4px;}
				
</style>

<div id="working-dialog-lab" title="Procesando..." style="display:none;"> 
	<p><strong>Su consulta esta siendo procesada.</strong></p>
	<?= $html->image("bar-loader.gif",array('border'=>0)) ?>
	<p><strong>Por favor aguarde...</strong></p>
</div>

<div id="div-dialog-guardar"  style="display:none;">
	<div id="div-guardar">
	
	</div>
</div>

<div class="button_link" id="links_lab">  
	<a href="<?=$html->url("/") ?>" title="Consultas" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-circle-triangle-w"></span>Consultas</a>
</div>

<br>

<div id="tabs">
	<ul>
		<li><a href="#tab-lab">Lab</a></li>
		<li><a href="#tab-res">Resultados</a></li>		
	</ul>
	
	<div id="tab-lab">
		
		<div id="tool_bar_lab"  class="ui-widget ui-helper-clearfix ui-state-highlight ui-corner-all">
			<ul >
				<!-- 
				<li id="boton_listas" class="ui-state-default ui-corner-all" title="Listados"><span class="ui-icon ui-icon-newwin"></span>Listas</li>
				 -->
				<li id="boton_procesar" class="ui-state-default ui-corner-all" title="Procesar"><span class="ui-icon ui-icon-gear"></span>Procesar</li>				
			</ul>
		</div>
		<br>
		
		<div class="ui-helper-clearfix" >
			
			<table id="tabla_listas">
				<tr>
					<td>
						<div class="ventana_listas ui-corner-all">
							<div class="ui-widget-header">Principal</div>
							<div id="listas_res" class="caja_listas" >
								
							</div>
						</div>	
					</td>
					<td>
						<div class="ventana_listas ui-corner-all">
							<div class="ui-widget-header">Origen</div>
							<div id="listas_origen" class="caja_listas">
								
								<ul id="listas">
									<?php 
									$Cleaner = new DataCleaner;
									foreach($listados as $l): ?>
									<li class="lab-lista" id="lista_<?= $l["Listado"]["id"] ?>" data-list-id="<?= $l["Listado"]["id"] ?>">
										<div class="ui-widget ui-widget-content ui-corner-all">
											
											<div class="ui-widget-header ui-helper-clearfix ui-corner-all">
												<?= $l["Listado"]["nombre"] ?>
												<span class="ui-icon ui-icon-circle-plus lab-lista-icon" lab-lista-icon></span>
											</div>
											
											<div class="campos-lista" style="display:none;">
												<ul>
												<?php
													$campos = $Cleaner->base64_unserialize($l["Listado"]["campos"]);
													foreach($campos as $k => $c): ?>
													<li data-list-id="<?= $l["Listado"]["id"] ?>" data-list-field="<?= $k ?>">
														<?= $c ?>					
													</li>
												<?php endforeach; ?>
												</ul>
											</div>
										</div>
									</li>
										
									<?php endforeach; ?>
								</ul>
							</div>
						</div>
					</td>
				</tr>
				
				<tr>
					<td>
						<div class="ventana_listas ui-corner-all">
							<div class="ui-widget-header">Que este 
								 
								<select id="conector_mas" name="conector_mas">
									<option value="AND">en TODAS las listas (AND)</option>
									<option value="OR">en al menos UNA lista (OR)</option>
									<option value="JOIN">JOIN (experimental)</option>
								</select>
								
							</div>
							<div id="listas_mas" class="caja_listas">
								
							</div>
						</div>
					</td>
					<td>
						<div class="ventana_listas ui-corner-all">
							<div class="ui-widget-header">Que no este en estas listas.</div>
							<div id="listas_menos" class="caja_listas">
								
							</div>
						</div>
					</td>
				</tr>
				
			</table>
		</div>
	</div>
	
	<div id="tab-res">
	</div>
</div>