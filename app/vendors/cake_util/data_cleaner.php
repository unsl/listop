<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/



uses("sanitize");

class DataCleaner {

	function escapeData($ar)
	{
		$sa = new Sanitize;

		if (is_array($ar)) {
			foreach ($ar as $k => $v) {
					$ar[$k] = DataCleaner::escapeData($v);
			}
			return $ar;
		} else {
			$ar = trim($ar);
			$ar = $sa->escape($ar);
			return $ar;
		}
	}

	function setBlankToNull($ar)
	{
		if (is_array($ar)) {
			foreach ($ar as $k => $v) {
					$ar[$k] = DataCleaner::setBlankToNull($v);
			}

			return $ar;
		} else {
			if ($ar == '') {
				$ar = NULL;
			}
			return $ar;
		}
	}

	function escapeHtml($ar)
	{
		$sa = new Sanitize;

		if (is_array($ar)) {
			foreach ($ar as $k => $v) {
					$ar[$k] = DataCleaner::escapeHtml($v);
			}
			return $ar;
		} else {
			$ar = $sa->html($ar);
			return $ar;
		}
	}
	
	function iso2utf8($ar)
	{
	$sa = new Sanitize;

		if (is_array($ar)) {
			foreach ($ar as $k => $v) {
					$ar[$k] = DataCleaner::iso2utf8($v);
			}
			return $ar;
		} else {
			
			//if (!mb_check_encoding($ar,"UTF-8")) {
			if (!($this->check_utf8($ar))) {	
				$ar = utf8_encode($ar);
			} 

			if (($ar == null) ||($ar === null)){
				$ar = "";
			}
			
			return $ar;
		}
	}
	
	function utf82iso($ar)
	{
	$sa = new Sanitize;

		if (is_array($ar)) {
			foreach ($ar as $k => $v) {
					$ar[$k] = DataCleaner::utf82iso($v);
			}
			return $ar;
		} else {
			$ar = utf8_decode($ar);
			return $ar;
		}
	}

	
	
	function check_utf8($str) {
		if(!is_object($str) && !is_array($str)){
		   $len = strlen($str);
		   for($i = 0; $i < $len; $i++){
		       $c = ord($str[$i]);
		       if ($c > 128) {
		           if (($c > 247)) return false;
		           elseif ($c > 239) $bytes = 4;
		           elseif ($c > 223) $bytes = 3;
		           elseif ($c > 191) $bytes = 2;
		           else return false;
		           if (($i + $bytes) > $len) return false;
		           while ($bytes > 1) {
		               $i++;
		               $b = ord($str[$i]);
		               if ($b < 128 || $b > 191) return false;
		               $bytes--;
		           }
		       }
		   }
		}
	   return true;
	} // end of check_utf8
	
	
	function base64_unserialize($str){
	    $ary = unserialize($str);
	    if (is_array($ary)){
	        foreach ($ary as $k => $v){
	            if (is_array(unserialize($v))){
	                $ritorno[$k]=$this->base64_unserialize($v);
	            }else{
	                $ritorno[$k]=base64_decode($v);
	            }
	        }
	    }else{
	        return false;
	    }
	    return $ritorno;
	}
	
	function base64_serialize($ary){
	    if (is_array($ary)){
	        foreach ($ary as $k => $v){
	            if (is_array($v)){
	                $ritorno[$k]=$this->base64_serialize($v);
	            }else{
	                $ritorno[$k]=base64_encode($v);
	            }
	        }
	    }else{
	        return false;
	    }
	    return serialize ($ritorno);
	}
	
}






?>
