<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/



//vendor("cake_util/data_cleaner");
App::import('Vendor', 'cake_util/data_cleaner');

class AppModel extends Model
{
	var $cleanData = true;
	var $blankToNull = true;
	var $depData = false;
	var $depValidate = array();

 	function beforeValidate()
 	{

 		if ($this->cleanData) {
 			$Cleaner = new DataCleaner;
	 		$this->data[$this->name] = $Cleaner->escapeData($this->data[$this->name]);
 		}

 		$this->_depValidate();

		return true;
 	}

 	function beforeSave()
 	{

 		if ($this->blankToNull) {
 			$Cleaner = new DataCleaner;
	 		$this->data[$this->name] = $Cleaner->setBlankToNull($this->data[$this->name]);
 		}

 		return true;
 	}


 	function _depValidate()
 	{
 		if (is_array($this->depValidate)) foreach ($this->depValidate as $field => $valArray) {
			foreach ($valArray as $dep) {
				$depField = $dep["depField"];

				if (isset($this->data[$this->name][$depField]))
				{

					if (isset($dep["depValue"])) {
						if (!is_array($dep["depValue"])) {
							$depValue = array($dep["depValue"]);
						} else {
							$depValue = $dep["depValue"];
						}

						if ( in_array($this->data[$this->name][$depField], $depValue)) {
							unset($dep["depField"]);
							unset($dep["depValue"]);
							$this->validate[$field][] = $dep;

						}

					} else {
						unset($dep["depField"]);
						$this->validate[$field][] = $dep;
					}
				}
 			}
 		}
 	}

}

?>
