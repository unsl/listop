<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

App::import('Vendor', 'cake_util/data_cleaner');

class AppController extends Controller
{
	var $helpers = array('Html','Javascript', 'Form');
	var $components = array('RequestHandler', 'Session');
	var $controlarSesion = true;
	var $layout = "estadistica";

	function beforeFilter()
	{
		if ($this->controlarSesion){
			$this->checkLogin();
		}
	}

	function checkLogin()
    {
        if (!$this->Session->check("Usuario"))
        {
            // Force the user to login
            if($this->RequestHandler->isAjax()){
            	$this->redirect('/usuario/redireccionar_cliente/'. urlencode('/usuario'));
            	//exit;
            } else {
            	$this->redirect('/usuario');
           }
        }
    }
    
	function set($key,$val, $encode=true)
	{
		if ($encode) {
			$Cleaner = new DataCleaner;
			parent::set($key,$Cleaner->iso2utf8($val));
		} else {
			parent::set($key,$val);
		}
	}
	
}

?>