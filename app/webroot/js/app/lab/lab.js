 
/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


/*****************************************************/
var gSelRes = false;
//var URL_BASE= "/devel/estadisticas"; // se supone que esta variable la setea el layout.
$(inicializar);
/*****************************************************/




function inicializar() {
	
	var $listas = $('#listas'), $trash = $('#trash');
	
	$('#tabs').tabs();
	
	$('li',$listas).draggable({
		cancel: '.ui-icon',// clicking an icon won't initiate dragging
		revert: 'invalid', // when not dropped, the item will revert back to its initial position
		helper: 'clone',
		opacity: 0.75,
		cursor: 'move'
		//cursor: 'crosshair'	
	});
	
	$('#working-dialog-lab').dialog({
		autoOpen: false, 
		modal: true, 
		resizable: false, 
		closeOnEscape: false, 
		draggable: false
	});
	
	$('#div-dialog-guardar').dialog({
		autoOpen: false, 
		modal: true, 
		resizable: false, 
		closeOnEscape: false, 
		draggable: false
	});
	
	//$("#boton_listas").click(function(){$("#div_listas").dialog("open");});
	$("#boton_procesar").click(function(){procesar();});
	
	$(".lab-lista-icon").click(function(){ocultar_mostrar_lista($(this).parent().parent());});
	
	$('#listas_res').droppable({
		accept: '#listas > li',
		drop: function(ev, ui) {
		lista_res(ui.draggable);
		}
	});
	
	$('#listas_mas').droppable({
		accept: '#listas > li',
		drop: function(ev, ui) {
			lista_mas(ui.draggable);
		}
	});
	
	$('#listas_menos').droppable({
		accept: '#listas > li',
		drop: function(ev, ui) {
		lista_menos(ui.draggable);
		}
	});
	
}

function lista_res($item) {
	var $lista = $('#listas_res');
	var $ul = $('ul',$lista).length ? $('ul',$lista) : $('<ul><ul/>').appendTo($lista);
	
	if (!$('li',$ul).length){
		$item.appendTo($ul);
		
		$sel = $("<select/>");
		var $campos = $('li',$item).map(function(i,e){return "<option value='"+ i + "'>" + $(this).text() + "</option>";}).get().join(" ");
		
		//Agregamos un campo en blanco
		$sel.html("<option value='-1'> </option>"+$campos);
		
		gSelRes = $sel;
		
	}
	
}

function lista_mas($item) {	
	if (gSelRes) {
		var $lista = $('#listas_mas');
		var $ul = $('#listas_mas > ul').length ? $('#listas_mas > ul') : $("<ul></ul>").appendTo($lista);
		agregarSelect($item,true);
		$item.appendTo($ul);
	} else {
		alert("Se debe seleccionar la lista principal!");
	}
}

function lista_menos($item) {
	if (gSelRes) {
		var $lista = $('#listas_menos');
		var $ul = $('#listas_menos > ul').length ? $('#listas_menos > ul') : $('<ul><ul/>').appendTo($lista);
		agregarSelect($item);
		$item.appendTo($ul);
	} else {
		alert("Se debe seleccionar la lista principal!");
	}
}


function agregarSelect($item, addCheck) {
	$('li',$item).each(function(){
		
		if(addCheck){
			chk = $("<input type='checkbox'>");
			chk.attr("name", $(this).attr("data-list-id") + "_" + $(this).attr("data-list-field"));
			chk.appendTo(this);
		}
		
		$("<span> = </span>").prependTo(this);
		var s = gSelRes.clone();
		//Poner aca bien el nombre del campo
		s.attr("name", $(this).attr("data-list-id") + "_" + $(this).attr("data-list-field"));
		s.prependTo(this);
	});	
}

function procesar(){
	
	$('#working-dialog-lab').dialog("open");
	
	//id de la lista resultado
	// con esto vamos especificamente al primer li
	var res = $("#listas_res > ul > li").map(
			function(i,e){
				return $(this).attr("data-list-id");
			}).get();
	
	//Campos y listas mas
	var mas = $("select",$("#listas_mas")).serializeArray();
	var campos_mas = $("input",$("#listas_mas")).serializeArray();
	var conector_mas = $("#conector_mas").serializeArray();
	
	//Campos y listas menos
	var menos = $("select",$("#listas_menos")).serializeArray();

	//armamos el objeto con los parametros
	var par = {res:res, mas:mas, menos:menos, campos_mas:campos_mas, conector_mas:conector_mas};
	//var par = {res:res, mas:mas, menos:menos, campos_mas:campos_mas};

	$.post(URL_BASE + "lab/procesar",par,
			function(response){
				if (response.status == "OK") {
					
					$("#tab-res").load(URL_BASE + "lab/listado", makeTableListado);
					$('#tabs').tabs('select', 1);
					
				} else {
				
				}
				$('#working-dialog-lab').dialog("close");
				
			},
			"json");
}

function makeTableListado() {
	$('#lab_listado_table').dataTable( {
			 "bProcessing": true,
			 "bServerSide": true,
			 "sPaginationType":"full_numbers",
			 "bFilter": false,
			 "bProcessing": true,
			 "bSort": false,
			 "bJQueryUI": true,  
			 "sAjaxSource": URL_BASE + "lab/listado_results",
			 "fnServerData": function ( sSource, aoData, fnCallback ) {
								$.ajax( {
									"dataType": 'json', 
									"type": "POST", 
									"url": sSource, 
									"data": aoData, 
									"success": fnCallback
								} );
							}
											 
		 	} );
	
	$("#link_guardar").click(
		function () {
			guardar_lab();
			return false;
		}
	);
	
}

function ocultar_mostrar_lista(lista) {

	var d = $(".campos-lista",lista);
	var i = $(".lab-lista-icon",lista);
	
	if (d.css("display")=="none") {
		$(".lab-lista-icon",lista).removeClass("ui-icon-circle-plus").addClass("ui-icon-circle-minus");
	} else {
		$(".lab-lista-icon",lista).removeClass("ui-icon-circle-minus").addClass("ui-icon-circle-plus");
	}
	
	d.toggle();
		
}

function makeAjaxForm_guardar() {
	var options = {
	        target: "#div-guardar",
	        success: makeAjaxForm_guardar
	};

	$('#div-form-guardar-lab form').ajaxForm(options);
}



function guardar_lab() {
	$("#div-guardar").load(
	URL_BASE + "lab/guardar",
	function() {
		makeAjaxForm_guardar();
		$("#div-dialog-guardar").dialog("open");
	}
	);

}

function cerrar_guardar_lab(){
	$("#div-dialog-guardar").dialog("close");
}