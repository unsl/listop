<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


class AppError extends ErrorHandler
{
    /**
     * HTTP response codes array
     *
     * @var array
     * @see http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
     **/
    var $codes = array(
		200 => 'OK',
		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found ja ja',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Time-out',
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		502 => 'Bad Gateway',
		503 => 'Service Unavailable',
		504 => 'Gateway Time-out'
	);
	
	
	public function error($params)
    {
		extract($params, EXTR_OVERWRITE);
		if (!isset($name)) {
		    $name = $this->codes[$code];
	    }
	    if (!isset($url)) {
			$url = $this->controller->here;
		}
	    // set header
	    header("HTTP/1.x $code $name");
		$this->controller->set(array(
			'code' => $code,
			'name' => $name,
			'message' => $message,
			'title' => $code . ' ' . $name,
			'request' => $url
		));
		if ($this->controller->RequestHandler->isXml()) {
		    $this->controller->RequestHandler->renderAs($this->controller, 'xml');
		}
		$this->_outputMessage('error');
    }

    
}