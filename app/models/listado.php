<?php 

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


class Listado extends AppModel {
	var $name = "Listado";
	var $useTable = "listados";
	
	/**
	 * @var array
	 * Es la lista indexada por campos.
	 */
	var $LISTA_INDEXADA;
	
	/**
	 * @var array 
	 * La clave es el campo de esta lista y el valor es el campo de la lista
	 * resultado con la que se debe unificar.
	 */
	var $CAMPOS;  
	
	
	/*
	 * @var array
	 * Son los nombres de las columas de esta lista
	 */
	var $COLUMNAS; 
	
	var $validate = array(
	
		"nombre" => array(
 				array(
 						"allowEmpty"=> false,
						"rule" 		=>VALID_NOT_EMPTY,
						"message"	=>"Debe indicar un nombre para este resultado."
				),
		)
	);
	
	
	function cruzar_listas($id,$MAS,$CAMPOS_MAS, $CONECTOR_MAS, $MENOS) {
		
		$Cleaner = new DataCleaner;
		
		$lm = $this->find("first",array("conditions"=>array("id"=>$id)));
		$datos = $Cleaner->base64_unserialize($lm["Listado"]["listado"]);
		$LISTA = $datos["results"];
				
		//Abrimos todas las listas a cruzar
		$lMas = array();
		foreach($MAS as $l => $c) {
			$modelo = new Listado();
			$modelo->_abrir_e_indexar($l,$c);
			$lMas[$l] = $modelo;  
		}
		
		$lMenos = array();
		foreach($MENOS as $l => $c) {
			$modelo = new Listado();
			$modelo->_abrir_e_indexar($l,$c);
			$lMenos[$l] = $modelo;  
		}
		
		//ahora hacemos el cruce:
		$LISTA_RESULTADO = array();
		
		foreach($LISTA as $L) {
			
			// LISTAS MAS ////////////////////////
			$ok_mas = true;
			if (count($lMas)>0) {
				// EN todas las listas
				if (trim($CONECTOR_MAS) == "AND") {
					$ok_mas = false;
					foreach($lMas as $l_id => $m) {
						
						//Obtenemos las columnas y en caso de que no esten seteadas las ponemos en blanco para normalizar el registro.
						$campos = $m->COLUMNAS;
						foreach ($campos as $k => $v) {
							if ($CAMPOS_MAS[$l_id][$k] == "on") {
								if (trim($L[$v]) == "") {
									$L[$v] = "";
								}
							}
						}
						
						$r = $m->buscar($L);
						if(!$r) {
							$ok_mas = false;
							break; 	
						}else{
							$ok_mas = true;
							//Vemos si hay que agregar al resultado un campo de esta lista.
						
							foreach ($campos as $k => $v) {
								if ($CAMPOS_MAS[$l_id][$k] == "on") {
									$L[$v] = $r[$v];
								}
							}
						} 
					}	
				}
				//En al menos una lista
				elseif (trim($CONECTOR_MAS) == "OR") {
					$ok_mas = false;
					foreach($lMas as $l_id => $m) {
						
						//Obtenemos las columnas y en caso de que no esten seteadas las ponemos en blanco para normalizar el registro.
						$campos = $m->COLUMNAS;
						foreach ($campos as $k => $v) {
							if ($CAMPOS_MAS[$l_id][$k] == "on") {
								if (trim($L[$v]) == "") {
									$L[$v] = "";
								}
							}
						}
						
						$r = $m->buscar($L);
						if(!$r) {
							continue; 
						}else{
							$ok_mas = true;
							//Vemos si hay que agregar al resultado un campo de esta lista.							
							foreach ($campos as $k => $v) {
								if ($CAMPOS_MAS[$l_id][$k] == "on") {
									$L[$v] = trim($r[$v]) != "" ? $r[$v] : "";
								}
							}
						} 
					}
				}
				if (!$ok_mas) continue; // Si no pasa el control listas mas ya no se agrega
			}
			// FIN LISTAS MAS ////////////////////////
			
			
			
			// LISTAS MENOS ////////////////////////
			$ok_menos = true;
			//vemos que no este en las que no tiene que estar
			foreach($lMenos as $m) {
				$r = $m->buscar($L);
				if($r) { //Si esta no hay que agregarlo
					$ok_menos = false;
					break;					
				}
			}
			if (!$ok_menos) continue; //Si esta en alguna lista no se agrega y al proximo! 
			// FIN LISTAS MENOS ////////////////////////
			
			
			$LISTA_RESULTADO[]=$L;
			
		}
		
		return $LISTA_RESULTADO;
		
	}
	
	function _abrir_e_indexar($id,$campos){
		$Cleaner = new DataCleaner;
		
		$this->CAMPOS = $campos;
		
		$cond = array("id" => $id);
		$lm = $this->find("first",array("conditions"=>$cond));
		$datos = $Cleaner->base64_unserialize($lm["Listado"]["listado"]);
		$L = $datos["results"];
		
		$this->COLUMNAS = $Cleaner->base64_unserialize($lm["Listado"]["campos"]);
		
		foreach($L as $registro) {
			$idx = $this->_obtener_indice($registro,array_keys($campos));
			$this->LISTA_INDEXADA[$idx]=$registro;	
		}
	}
	
	function _obtener_indice($registro,$campos){
		$subr = array();
		$keys = array_keys($registro);
		
		foreach($campos as $c) {
			$k = $keys[$c];
			$subr[]=$registro[$k];
		}
		return implode("=",$subr);
	}

	function buscar($registro) {
		$idx = $this->_obtener_indice($registro,$this->CAMPOS);
		if(isset($this->LISTA_INDEXADA[$idx])) {
			return $this->LISTA_INDEXADA[$idx];
		} else {
			return FALSE;
		}
	}
	
	
function cruzar_listas_x_join($id,$MAS,$CAMPOS_MAS, $MENOS) {
		
		$Cleaner = new DataCleaner;
		
		$lm = $this->find("first",array("conditions"=>array("id"=>$id)));
		$datos = $Cleaner->base64_unserialize($lm["Listado"]["listado"]);
		$LISTA = $datos["results"];
				
		//Abrimos todas las listas a cruzar
		$lMas = array();
		foreach($MAS as $l => $c) {
			$modelo = new Listado();
			$modelo->_abrir_e_indexar_x_join($l,$c);
			$lMas[$l] = $modelo;  
		}
		
		$lMenos = array();
		foreach($MENOS as $l => $c) {
			$modelo = new Listado();
			$modelo->_abrir_e_indexar($l,$c);
			$lMenos[$l] = $modelo;  
		}
		
		//ahora hacemos el cruce:
		$LISTA_RESULTADO = array();
		
		// LISTAS MENOS ////////////////////////
		foreach($LISTA as $L) {
			$ok_menos = true;
			//vemos que no este en las que no tiene que estar
			foreach($lMenos as $m) {
				$r = $m->buscar($L);
				if($r) { //Si esta no hay que agregarlo
					$ok_menos = false;
					break;					
				}
			}
			if (!$ok_menos) {
				continue; //Si esta en alguna lista no se agrega y al proximo! 
			} else {
				$LISTA_RESULTADO[]=$L;
			}
		
		}
		// FIN LISTAS MENOS ////////////////////////
		
		
		
			// LISTAS MAS (JOIN) ///////////////////////
			if (count($lMas)>0) {
				// EN todas las listas
				foreach($lMas as $l_id => $l_join) {
					
					$LISTA_RESULTADO = $this->_next_join($LISTA_RESULTADO,$l_join,$CAMPOS_MAS[$l_id]);
					//Obtenemos las columnas y en caso de que no esten seteadas las ponemos en blanco para normalizar el registro.
				}
			}	
					/*
					$campos = $m->COLUMNAS;
					foreach ($campos as $k => $v) {
						if ($CAMPOS_MAS[$l_id][$k] == "on") {
							if (trim($L[$v]) == "") {
								$L[$v] = "";
							}
						}
					}
					
					$R = $m->buscar($L);
					if($R) {
						foreach ($R as $r) {
							$LR = $L;	}	
				
		
							//Vemos si hay que agregar al resultado un campo de esta lista.
							foreach ($campos as $k => $v) {
								if ($CAMPOS_MAS[$l_id][$k] == "on") {
									$LR[$v] = $r[$v];
								}
							}
							$LISTA_RESULTADO[]=$LR;
						}
					} 
					*/
			
		return $LISTA_RESULTADO;
		
	}
	
	function _next_join($lista_resultados, $lista_join, $CAMPOS)
	{
		
		$RESULTADO = array();
		
		foreach($lista_resultados as $L) {
		
			//Obtenemos las columnas y en caso de que no esten seteadas las ponemos en blanco para normalizar el registro.
			$campos = $lista_join->COLUMNAS;
			foreach ($campos as $k => $v) {
				if ($CAMPOS[$k] == "on") {
					if (trim($L[$v]) == "") {
						$L[$v] = "";
					}
				}
			}
			
			
			$R = $lista_join->buscar($L);
			if($R) {
				foreach ($R as $r) {
					$LR = $L;
					//Vemos si hay que agregar al resultado un campo de esta lista.
					foreach ($campos as $k => $v) {
						if ($CAMPOS[$k] == "on") {
							$LR[$v] = $r[$v];
						}
					}
					$RESULTADO[]=$LR;
				}
			}
		 
		}
		
		return $RESULTADO;
		
	}
	
	
	// a diferencia del abrir_e_indexar, este no unifica registros, y 
	// mantiene el conjunto de todos los que hacen match indexados.
	function _abrir_e_indexar_x_join($id,$campos){
		$Cleaner = new DataCleaner;
		
		$this->CAMPOS = $campos;
		
		$cond = array("id" => $id);
		$lm = $this->find("first",array("conditions"=>$cond));
		$datos = $Cleaner->base64_unserialize($lm["Listado"]["listado"]);
		$L = $datos["results"];
		
		$this->COLUMNAS = $Cleaner->base64_unserialize($lm["Listado"]["campos"]);
		
		foreach($L as $registro) {
			$idx = $this->_obtener_indice($registro,array_keys($campos));
			$this->LISTA_INDEXADA[$idx][]=$registro;	
		}
	}
	
}


?>