<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


class ConsultaMaterias extends Consultamodel {
	
	
	var $name = "ConsultaMaterias";
	var $cacheLifeTime = 0;
	
	var $filter_options = array (
			array(
				'field' => 'ConsultaMaterias.plan_vigente',
				'type' => 'checkbox',
				'label' => 'Solo Plan Vigente de cada carrera',
				'title'  => 'Incluir solo las materias correspondientes al plan vigente.',
				'options' => array(),
			),
			array(
				'field' => 'ConsultaMaterias.cod_mat',
				'type' => 'text',
				'label' => 'Cod. Mat.',
				'tip'  => 'Puede agregar un filtro por el codigo de la materia.',
				'options' => array(),
			),
			array(
				'field' => 'ConsultaMaterias.nombre',
				'type' => 'text',
				'label' => 'Nombre',
				'tip'  => 'Puede agregar un filtro por nombre de la materia.',
				'options' => array(),
			),
			array(
				'field' => 'ConsultaMaterias.tipo_materia',
				'type' => 'select',
				'multiple'=>true,
				'label' => 'Tipo de Materia',
				'tip'  => 'Puede agregar un filtro por el tipo de la materia.',
				'options' => array('N'=>'Normal', 'G'=> 'General','O'=>'Optativa'),
			),
			array(
				'field' => 'ConsultaMaterias.aniod',
				'type' => 'text',
				'label' => 'Año de Cursada Desde',
				'tip'  => '',
				'options' => array(),
			),
			
			array(
				'field' => 'ConsultaMaterias.anioh',
				'type' => 'text',
				'label' => 'Año de Cursada Hasta',
				'tip'  => '',
				'options' => array(),
			),
			
			array(
				'field' => 'ConsultaMaterias.horastd',
				'type' => 'text',
				'label' => 'Horas Tot. Desde',
				'tip'  => '',
				'options' => array(),
			),
			array(
				'field' => 'ConsultaMaterias.horasth',
				'type' => 'text',
				'label' => 'Horas Tot. Hasta',
				'tip'  => '',
				'options' => array(),
			),
			array(
				'field' => 'ConsultaMaterias.horassd',
				'type' => 'text',
				'label' => 'Horas Sem. Desde',
				'tip'  => '',
				'options' => array(),
			),
			array(
				'field' => 'ConsultaMaterias.horassh',
				'type' => 'text',
				'label' => 'Horas Sem. Hasta',
				'tip'  => '',
				'options' => array(),
			),
			
	);
	
	var $validate = array(    
		'carrera' => array(
				'una_o_mas'=> array(
					'rule' => array('multiple', array('min' => 1)),
					'message' => 'Seleccione al menos una carrera.'
				),    
		),
		'tipo_materia' => array(
				'una_o_mas'=> array(
					'rule' => array('multiple', array('min' => 1)),
					'message' => 'Seleccione al menos un tipo de materia.'
				),    
		),
		'aniod' => array(
				'numero'=> array(
					'allowEmpty' => true,
					'rule' => NUMERIC,
					'message' => 'Debe ingresar un numero entero.'
				),    
		),
		'anioh' => array(
				'numero'=> array(
					'allowEmpty' => true,
					'rule' => NUMERIC,
					'message' => 'Debe ingresar un numero entero.'
				),    
		),
		'horastd' => array(
				'numero'=> array(
					'allowEmpty' => true,
					'rule' => NUMERIC,
					'message' => 'Debe ingresar un numero entero.'
				),    
		)
		,
		'horasth' => array(
				'numero'=> array(
					'allowEmpty' => true,
					'rule' => NUMERIC,
					'message' => 'Debe ingresar un numero entero.'
				),    
		)
		,
		'horassd' => array(
				'numero'=> array(
					'allowEmpty' => true,
					'rule' => NUMERIC,
					'message' => 'Debe ingresar un numero entero.'
				),    
		),
		'horassh' => array(
				'numero'=> array(
					'allowEmpty' => true,
					'rule' => NUMERIC,
					'message' => 'Debe ingresar un numero entero.'
				),    
		)
	);
	
	
	function getData() {
 		
		App::import('Model', 'MateGuarani');
		$Model = new MateGuarani();
		$Model->useDbConfig = $this->getDbFac($this->data['ConsultaMaterias']['facultad']);
		
 		$sql = "select
 					c.carrera ,c.nombre as nombre_carrera
 					,m.plan,m.materia, m.nombre_materia, m.nombre_reducido, m.periodo_dictado 
 					,m.carga_horaria_tot, m.horas_semanales, m.tipo_materia, m.anio_de_cursada  
				    
				FROM sga_carreras c, sga_planes p, sga_atrib_mat_plan m
				WHERE m.sale_listado = 'S'
				and c.unidad_academica = p.unidad_academica
				and c.carrera = p.carrera
				and p.unidad_academica = m.unidad_academica
				and p.carrera = m.carrera
				and p.plan = m.plan
				and p.version_actual = m.version
				
				%s
				ORDER BY c.nombre, m.plan, m.anio_de_cursada, m.periodo_dictado, m.nombre_materia
 				";
		
 		$extra = " ";
 		
 		$cars = $this->data['ConsultaMaterias']['carrera'];
		if (is_array($cars) && (count($cars)>0)) {

 			$lista_cars = implode(' , ', array_map(wrap_coma,$cars));
 		
 			$extra .= " and p.carrera IN (".$lista_cars.") ";
 			
 		}
 		
		if (trim($this->data['ConsultaMaterias']['plan']) != "") {
 			$extra .= sprintf(" and p.plan = '%s' ", $this->data['ConsultaMaterias']['plan']);
 		}
 		
 		if ($this->data['ConsultaMaterias']['plan_vigente'] == 1) {
 			$extra .= " and p.plan = c.plan_vigente ";
 		} 
			
		if(trim($this->data['ConsultaMaterias']['nombre']) != "") {
			$extra .= " and m.nombre_materia LIKE '%" . $this->data['ConsultaMaterias']['nombre'] ."%' ";
		}
		
		$tip = $this->data['ConsultaMaterias']['tipo_materia'];
		if (is_array($tip) && (count($tip)>0)) {
 			$lista_tipos = implode(' , ', array_map(wrap_coma,$tip));
 			$extra .= " and m.tipo_materia IN (".$lista_tipos.") ";
 		}
		
 		
		if(trim($this->data['ConsultaMaterias']['aniod']) != "") {
			$extra .= " and m.anio_de_cursada + 0 >= " . $this->data['ConsultaMaterias']['aniod'] ." ";
		}
		
		if(trim($this->data['ConsultaMaterias']['anioh']) != "") {
			$extra .= " and m.anio_de_cursada + 0 <= " . $this->data['ConsultaMaterias']['anioh'] ." ";
		}
 		
		if(trim($this->data['ConsultaMaterias']['cod_mat']) != "") {
			$extra .= " and m.materia = '" . $this->data['ConsultaMaterias']['cod_mat'] ."' ";
		}
 		
		if(trim($this->data['ConsultaMaterias']['horastd']) != "") {
			$extra .= " and m.carga_horaria_tot + 0 >= " . $this->data['ConsultaMaterias']['horastd'] ." ";
		}
		
		if(trim($this->data['ConsultaMaterias']['horasth']) != "") {
			$extra .= " and m.carga_horaria_tot + 0 <= " . $this->data['ConsultaMaterias']['horasth'] ." ";
		}
		
		if(trim($this->data['ConsultaMaterias']['horassd']) != "") {
			$extra .= " and m.horas_semanales + 0 >= " . $this->data['ConsultaMaterias']['horassd'] ." ";
		}
		
		if(trim($this->data['ConsultaMaterias']['horassh']) != "") {
			$extra .= " and m.horas_semanales + 0 <= " . $this->data['ConsultaMaterias']['horassh'] ." ";
		}
		
 		$sql = sprintf($sql,
 						$extra
 						);
 		
 		$res = $Model->query($sql);
 		
		return $res;
 		
 	}
	
 	function mapRow($row) {
 		
 		switch ($row[0]["tipo_materia"]) {
 			case 'N' : {
 				$tipo = "Normal";
 				break;
 			}
 			case 'G' : {
 				$tipo = "General";
 				break;
 			}
 			case 'O' : {
 				$tipo = "Optativa";
 				break;
 			}
 			default: {
 				$tipo = $row[0]["tipo_materia"];
 				break;
 			}
 		}
 		
 		return array(
 			"Cod. Car." => $row[0]["carrera"],
 			"Carrera" => $row[0]["nombre_carrera"],
 			"Plan" => $row[0]["plan"],
 			"Cod. Mat." => $row[0]["materia"],
 			"Materia" => $row[0]["nombre_materia"],
 			"Año de Cur." => $row[0]["anio_de_cursada"],
 			"Periodo" => $row[0]["periodo_dictado"],
 			"Tipo" => $tipo,
 			"Horas Tot." => $row[0]["carga_horaria_tot"],
 			"Horas Sem." => $row[0]["horas_semanales"],
 		);
 		
 	}
	
	function getInputParams() {
 		
 		App::import("Model","Carrera");
 		$MC = new Carrera();
 		$MC->useDbConfig = $this->getDbFac($this->data['ConsultaMaterias']['facultad']);
 		
 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
 		
 		$opt_car = array(
				'field' => 'ConsultaMaterias.carrera',
				'type' => 'select',
 				'multiple'=>true,
 				'size' => 10,
				'label' => 'Carrera',
				'title'  => 'Filtro por carrera',
				'options' => $cars,
 				'data-cascade-parent' => 'ConsultaMateriasFacultad'
		);
		
		
		
		$opt_fac = array(
				'field' => 'ConsultaMaterias.facultad',
				'type' => 'select',
				'label' => 'Facultad',
				'title'  => 'Facultad que desea consultar.',
				'options' =>$this->session_usr["lista_fac"],
		);
		
		$opt_plan = array(
				'field' => 'ConsultaMaterias.plan',
				'type' => 'text',
				'label' => 'Plan',
				'title'  => 'Restringir a un unico plan',
				'options' => array(),
		);
		
 		$this->input_params[] = $opt_fac;
		$this->input_params[] = $opt_car;
		$this->input_params[] = $opt_plan;
 		
 		return $this->input_params;
 		
 	}
	
	function cascade_parent_change($child_id, $parent_value) {
		
 		App::import('Vendor', 'cake_util/data_cleaner');
 		App::import('Vendor', 'cake_util/cascade_element');
 		
 		if ($child_id = "ConsultaMateriasCarrera") {
 			
 			$EL = array();
 			
 			App::import("Model","Carrera");
	 		$MC = new Carrera();
	 		$MC->useDbConfig = $this->getDbFac($parent_value);
	 		
	 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
	 		
	 		 
	 		
	 		$Cleaner = new DataCleaner;
			
	 		foreach ($cars as $k=>$c) {
	 			$e = new CascadeElement;
	 			$e->When = $Cleaner->iso2utf8($parent_value);
	 			$e->Value = $Cleaner->iso2utf8($k);
	 			$e->Text = $Cleaner->iso2utf8($c);
	 			$EL[] = $e;
	 		}
	 		
	 		return $EL;
 			
 		}		
 		
 	}
	
}

?>