<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

	

	class ConsultaCarreras extends ConsultaModel {
		
		var $name = "ConsultaCarreras";
		//var $useTable = false;
		
		
		
		var $filter_options = array(
			array(
				'field' => 'ConsultaCarreras.codigo',
				'type' => 'text',
				'label' => 'Cod. Car.',
				'tip'  => 'Puede agregar un filtro por el código de una carrera.',
				'options' => array(),
			),
			array(
				'field' => 'ConsultaCarreras.nombre',
				'type' => 'text',
				'label' => 'Nombre',
				'tip'  => 'Puede agregar un filtro por el nombre de la carrera.',
				'options' => array(),
			),
			
			array(
				'field' => 'ConsultaCarreras.plan',
				'type' => 'text',
				'label' => 'Plan Vigente',
				'tip'  => 'Puede agregar un filtro por el plan vigente.',
				'options' => array(),
			),
			
			array(
				'field' => 'ConsultaCarreras.estado',
				'type' => 'select',
				'label' => 'Estado',
				'tip'  => 'Puede agregar un filtro por el estado de la carrera.',
				'options' => array('T'=>'Todos', 'A'=>'Activa', 'S'=>'Suspendida'),
			),
		);
		
		function getData() {
			App::import('Model', 'Carrera');
			$carModel = new Carrera;
			$carModel->useDbConfig = $this->getDbFac($this->data['ConsultaCarreras']['facultad']);
			
			$cond = array();
			if(trim($this->data['ConsultaCarreras']['codigo']) != "") {
				$cond[] = " Carrera.carrera = '" . $this->data['ConsultaCarreras']['codigo'] ."' ";
			}	
			if(trim($this->data['ConsultaCarreras']['nombre']) != "") {
				$cond[] = " Carrera.nombre LIKE '%" . $this->data['ConsultaCarreras']['nombre'] ."%' ";
			}
			if(trim($this->data['ConsultaCarreras']['plan']) != "") {
				$cond[] = " Carrera.plan_vigente = '" . $this->data['ConsultaCarreras']['plan'] ."' ";
			}
			if($this->data['ConsultaCarreras']['estado'] != 'T') {
				$cond[] = " Carrera.estado = '" . $this->data['ConsultaCarreras']['estado'] ."' ";
			}
			
			return $carModel->find("all", array("conditions"=>$cond));
			
		}
		
		function mapRow($row) {
			
			switch($row['Carrera']['estado']) {
				case 'A': {
					$estado = "Activa";
					break;
				}
				case 'S': {
					$estado = "Suspendida";
					break;
				}
				default: {
					$estado = $row['Carrera']['estado'];
					break;
				}
			}
			
			return array(
				'Cod. Car.' => $row['Carrera']['carrera'],
				'Nombre' => $row['Carrera']['nombre'],
				'Nombre Reducido' => $row['Carrera']['nombre_reducido'],
				'Plan Vigente' => $row['Carrera']['plan_vigente'],
				'Estado' => $estado,
			
			);
		}
		
		function getInputParams() {
			$input_params = array (
				array(
					'field' => 'ConsultaCarreras.facultad',
					'type' => 'select',
					'label'=> 'Facultad ',
					'title'  => 'Indique la facultad',
					'options' =>$this->session_usr["lista_fac"],
				),
			);
			return $input_params;
		}
			
	}

?>