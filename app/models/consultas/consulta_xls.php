<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


class ConsultaXls extends ConsultaModel {
	
	
	var $name = "ConsultaXls";
	var $cacheLifeTime = 0; //Sin cache
	
	var $actsAs = array(
		'MeioUpload.MeioUpload' => array(
			'xlsfile' => array(
				'create_directory' => true,
				'dir' => '/tmp/consulta_xls',
				'allowedMime' => array('application/vnd.ms-excel'),
				'allowedExt' => array('.xls'),
				'maxSize' => '4 Mb'
			)
		),
	);
	
	var $input_params = array (
		array(
				'field' => 'ConsultaXls.xlsfile',
				'type' => 'file',
				'label' => 'Archivo Excel',
				'title'  => 'Seleccione un archivo de su disco para consultar.',
				'options' => array(),
			),
	);
	
	var $filter_options = array (
			
	);
	
	
	function getData() {
		
		App::import('Vendor','excel/oleread');
		App::import('Vendor','excel/reader');
		
		//print_r($this->data);
		
		$data = new Spreadsheet_Excel_Reader();
		$data->setOutputEncoding('CP1251');
		$data->read($this->data['ConsultaXls']['xlsfile']['tmp_name']);
		
		
		//print_r($data);
		//print_r($data->formatRecords);
		
		$COL = array();
		for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
				$COL[$j] = $data->sheets[0]['cells'][1][$j];
		}
		
		//print_r($COL);
		
		$res = array();
		
		for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
			for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
				$res[$i][$COL[$j]] = $data->sheets[0]['cells'][$i][$j];
			}
		}
		
		//print_r($res);
		
		return $res;
		
	}
	
	function mapRow($row) {
		return $row;
	}
	
}



?>