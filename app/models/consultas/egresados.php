<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


class Egresados extends ConsultaModel {
	
	var $name = "Egresados";
	var $cacheLifeTime = 864000; //24 horas (en segundos)
	
	var $filter_options = array (
			
			array(
				'field' => 'Egresados.nro_egresado',
				'type' => 'text',
				'label' => 'Nro. Egresado',
				'title'  => 'Nro. Egresado',
			),
			array(
				'field' => 'Egresados.legajo',
				'type' => 'text',
				'label' => 'Legajo',
				'title'  => 'Legajo',
			),
			array(
				'field' => 'Egresados.apellido',
				'type' => 'text',
				'label' => 'Apellido',
				'tip'  => 'Puede agregar un filtro por apellido',
				'options' => array(),
			),
			array(
				'field' => 'Egresados.nombre',
				'type' => 'text',
				'label' => 'Nombre',
				'tip'  => 'Puede agregar un filtro por nombre',
				'options' => array(),
			),
			array(
				'field' => 'Egresados.documento',
				'type' => 'text',
				'label' => 'Documento',
				'tip'  => 'Puede agregar un filtro por el DNI',
				'options' => array(),
			),
			array(
				'field' => 'Egresados.sexo',
				'type' => 'select',
				'label' => 'Sexo',
				'tip'  => '',
				'options' => array(
					'0' => 'TODOS',
					'1' => 'Masculino',
					'2' => 'Femenino',
				),
			),
			array(
				'field' => 'Egresados.fechad',
				'type' => 'text',
				'label' => 'Egreso Desde',
				'title'  => 'Fecha desde donde filtrar',
				'data-input-date' => 'yy-mm-dd'
			),
			array(
				'field' => 'Egresados.fechah',
				'type' => 'text',
				'label' => 'Egreso Hasta',
				'title'  => 'Fecha hasta donde filtrar',
				'data-input-date' => 'yy-mm-dd'
			),
			array(
				'field' => 'Egresados.promediod',
				'type' => 'text',
				'label' => 'Promedio Desde',
			),
			array(
				'field' => 'Egresados.promedioh',
				'type' => 'text',
				'label' => 'Promedio Hasta',
			),
			array(
				'field' => 'Egresados.promediosad',
				'type' => 'text',
				'label' => 'Promedio (s/a) Desde',
			),
			array(
				'field' => 'Egresados.promediosah',
				'type' => 'text',
				'label' => 'Promedio (s/a) Hasta',
			),
	);

//	var $validate = array(    
//		'carrera' => array(
//				'una_o_mas'=> array(
//					'rule' => array('multiple', array('min' => 1)),
//					'message' => 'Seleccione al menos una carrera.'
//				),    
//		)
//	);
	
	
function getInputParams() {
 		
 		App::import("Model","Carrera");
 		$MC = new Carrera();
 		
 		$MC->useDbConfig = $this->getDbFac($this->data['Egresados']['facultad']);
 		
 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
 		
 		 
 		
 		$opt_car = array(
				'field' => 'Egresados.carrera',
				'type' => 'select',
 				'multiple'=>true,
 				'size' => 10,		
				'label' => 'Carrera',
				'title'  => 'Filtro por carrera',
				'options' => $cars,
 				'data-cascade-parent' => 'EgresadosFacultad'
		);
		
		$opt_fac = array(
				'field' => 'Egresados.facultad',
				'type' => 'select',
				'label' => 'Facultad',
				'title'  => 'Facultad que desea consultar.',
				'options' =>$this->session_usr["lista_fac"],
		);
		
 		$this->input_params[] = $opt_fac;
		$this->input_params[] = $opt_car;
		
 		
 		return $this->input_params;
 		
 	}
	
	function getData() {
 		
 		$sql = "select  t.titulo, t.nombre as nombre_titulo, t.nivel, t.duracion 
				        ,ta.legajo, ta.plan, ta.fecha_egreso, ta.prom_general, ta.prom_sin_aplazos
				        ,p.apellido, p.nombres, p.nro_documento, p.sexo 
				        ,c.nombre as nombre_carrera
				         
				        
				        ,ta.nro_regis_dipl_fac,
						p.tipo_documento,   
						ta.resol_rectorado,   
						ta.carrera,   
						ta.juramento,   
						ta.nro_expediente,   
						ta.estado,   
						t.nombre_femenino,   
						t.tipo_de_titulo,
						p.fecha_nacimiento,
						p.nacionalidad
				        
				from sga_titulos_otorg ta, sga_titulos t, sga_carreras c, sga_personas p 
				where 
				ta.unidad_academica = t.unidad_academica
				and ta.titulo = t.titulo
				and ta.unidad_academica = c.unidad_academica
				and ta.carrera = c.carrera
				and ta.unidad_academica = p.unidad_academica 
				and ta.nro_inscripcion = p.nro_inscripcion
				  
				%s
				
				ORDER BY p.apellido, p.nombres 
 				";
		
 		$extra = " ";
 		
 		$cars = $this->data['Egresados']['carrera'];
 		
 		
 		
 		if (is_array($cars) && (count($cars)>0)) {
 		
 			$lista_cars = implode(' , ', array_map(wrap_coma,$cars));
 		
 			
 			$extra .= " and ta.carrera IN (".$lista_cars.") ";
 			
 		}
 		
		if (trim($this->data['Egresados']['nro_egresado']) != "") {
 			$extra .= sprintf(" and ta.nro_regis_dipl_fac = '%s'", $this->data['Egresados']['nro_egresado']);
 		}
 		
		if (trim($this->data['Egresados']['legajo']) != "") {
 			$extra .= sprintf(" and ta.legajo = '%s'", $this->data['Egresados']['legajo']);
 		}
 		
 		
		if(trim($this->data['Egresados']['apellido']) != "") {
			$extra .=  " and p.apellido LIKE '" . $this->data['Egresados']['apellido'] ."%' ";
		}
			
		if(trim($this->data['Egresados']['nombre']) != "") {
			$extra .= " and p.nombres LIKE '%" . $this->data['Egresados']['nombre'] ."%' ";
		}

		if(trim($this->data['Egresados']['documento']) != "") {
			$extra .= " and p.nro_documento = '" . $this->data['Egresados']['documento'] ."' ";
		}
		
		if($this->data['Egresados']['sexo'] != 0) {
			$extra .= "and p.sexo = " . $this->data['Egresados']['sexo'];
		}
 		
		//Fecha
 		if (trim($this->data['Egresados']['fechad']) != "") {
 			$extra .= sprintf(" and ta.fecha_egreso >= datetime(%s) YEAR TO DAY ", $this->data['Egresados']['fechad']);
 		}
 		
 		if (trim($this->data['Egresados']['fechah']) != "") {
 			$extra .= sprintf(" and ta.fecha_egreso <= datetime(%s) YEAR TO DAY ", $this->data['Egresados']['fechah']);
 		}
 		
 		//Promedios
		if (trim($this->data['Egresados']['promediod']) != "") {
 			$extra .= sprintf(" and ta.prom_general + 0 >= %s ", $this->data['Egresados']['promediod']);
 		}
		if (trim($this->data['Egresados']['promedioh']) != "") {
 			$extra .= sprintf(" and ta.prom_general + 0 <= %s ", $this->data['Egresados']['promedioh']);
 		}
 		
		if (trim($this->data['Egresados']['promediosad']) != "") {
 			$extra .= sprintf(" and ta.prom_sin_aplazos + 0 >= %s ", $this->data['Egresados']['promediosad']);
 		}
		if (trim($this->data['Egresados']['promediosah']) != "") {
 			$extra .= sprintf(" and ta.prom_sin_aplazos + 0 <= %s ", $this->data['Egresados']['promediosah']);
 		}
 		
 		
 		$sql = sprintf($sql,
 						$extra
 						);
 		
 		
 						
 		App::import('Model', 'MateGuarani');
		$Model = new MateGuarani();
		$Model->useDbConfig = $this->getDbFac($this->data['Egresados']['facultad']);
		
 		$res = $Model->query($sql);
 		
 		//echo $sql;
 		
		return $res;
 		
 	}
 	
 	
	function mapRow($row) {
	
 		
 		
 		switch ($row[0]["sexo"]) {
 			case 1 : { 
 				$sexo = "M";
 				break;		
 			}
 			case 2 : { 
 				$sexo = "F";
 				break;		
 			}	
 		}
 		
		switch ($row[0]["nacionalidad"]) {
 			case 1 : { 
 				$nac = "Argentino";
 				break;		
 			}
 			case 2 : { 
 				$nac = "Extranjero";
 				break;		
 			}
			case 3 : { 
 				$nac = "Naturalizado";
 				break;		
 			}
			case 4 : { 
 				$nac = "Por Opción";
 				break;		
 			}	
 		}
 		

/* 		
 		select  t.titulo, t.nombre as nombre_titulo, t.nivel, t.duracion 
				        ,ta.legajo, ta.plan, ta.fecha_egreso, ta.prom_general, ta.prom_sin_aplazos
				        ,p.apellido, p.nombres, p.nro_documento 
				        ,c.nombre as nombre_carrera
 */		
 		
 		
 //		ta.nro_regis_dipl_fac,
//p.tipo_documento,   


 		return array(
 			
 		
 			"Nro. Egresado" => $row[0]["nro_regis_dipl_fac"], //usa Marcelo
 			"Legajo" => $row[0]["legajo"],
 			"Apellido" => $row[0]["apellido"],
 			"Nombres" => $row[0]["nombres"],
 			"Fecha Nacimiento" => $row[0]["fecha_nacimiento"],
 			"Nacionalidad" => $nac,
 			"Tipo Doc." => $row[0]["tipo_documento"], //usa Marcelo
 			"Documento" => $row[0]["nro_documento"],
 			"Sexo"	=> $sexo,
 			"Fecha de Egreso" => $row[0]["fecha_egreso"],
 			"Nombre Titulo" => $row[0]["nombre_titulo"],
 			"Cod Car." => $row[0]["carrera"], //usa Marcelo
 			"Carrera" => $row[0]["nombre_carrera"],
 			"Plan" => $row[0]["plan"],
 			"Promedio" => sprintf("%0.2f",$row[0]["prom_general"]),
 			"Promedio S/A" => sprintf("%0.2f",$row[0]["prom_sin_aplazos"]),
 			"Juramento" => $row[0]["juramento"],
 			"Expediente" => $row[0]["nro_expediente"],
 			"Estado" => $row[0]["estado"],
 			"Nombre Fem." => $row[0]["nombre_femenino"],
 			"Tipo Titulo" => $row[0]["tipo_de_titulo"],
 		);
 	}
	

function cascade_parent_change($child_id, $parent_value) {
		
 		App::import('Vendor', 'cake_util/data_cleaner');
 		App::import('Vendor', 'cake_util/cascade_element');
 		
 		if ($child_id = "EgresadosCarrera") {
 			
 			$EL = array();
 			
 			App::import("Model","Carrera");
	 		$MC = new Carrera();
	 		$MC->useDbConfig = $this->getDbFac($parent_value);
	 		
	 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
	 		
	 		$Cleaner = new DataCleaner;
			
	 		foreach ($cars as $k=>$c) {
	 			$e = new CascadeElement;
	 			$e->When = $Cleaner->iso2utf8($parent_value);
	 			$e->Value = $Cleaner->iso2utf8($k);
	 			$e->Text = $Cleaner->iso2utf8($c);
	 			$EL[] = $e;
	 		}
	 		
	 		return $EL;
 			
 		}
 		
 		
 	}
 	
	
}



?>