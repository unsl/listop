<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


/**
 * Esta consulta retorna un listado por carrera y anio de estudio, 
 * con todas los examenes que tiene un alumno. 
 */

App::import('Vendor', 'cake_util/cascade_element');

ini_set("memory_limit", "1024M");
ini_set("max_execution_time", "3600");

class SiuAprobadasPlan extends ConsultaModel {
	
	var $name = "SiuAprobadasPlan";
	var $cacheLifeTime = 864000; //24 horas (en segundos)
	
	var $filter_options = array (
			array(
				'field' => 'SiuAprobadasPlan.legajo',
				'type' => 'text',
				'label' => 'Legajo',
				'title'  => 'Legajo',
			),
			array(
				'field' => 'SiuAprobadasPlan.diferenciad',
				'type' => 'text',
				'label' => 'Dif. Desde',
				'title'  => '',
			),
			array(
				'field' => 'SiuAprobadasPlan.diferenciah',
				'type' => 'text',
				'label' => 'Dif. Hasta',
				'title'  => 'Legajo',
			),
			
	); 
		
	var $validate = array(    
		'carrera' => array(
				'una_o_mas'=> array(
					'rule' => array('multiple', array('min' => 1)),
					'message' => 'Seleccione al menos una carrera.'
				),    
		)
	);
	
 	function getData() {
 		
 		
 		App::import('Model', 'MateGuarani');
		$Model = new MateGuarani();
		$Model->useDbConfig = $this->getDbFac($this->data['SiuAprobadasPlan']['facultad']);
		
 		/*
 		$sql = "SELECT h.legajo, h.carrera, h.plan, 0 as materias_plan , count(h.nota) as aprobadas, 0 as diferencia  
				from vw_hist_academica h 
				where h.resultado = 'A'
				%s 
				group by  h.legajo, h.carrera, h.plan 
				order by  h.legajo, h.carrera, h.plan 
				into tmp_exa_legajo ";
 		
 		$Model->query($sql);
 		*/
 		
 		
 		$sql = "SELECT h.legajo, h.carrera, h.plan, 0 as materias_plan , count(h.nota) as aprobadas, 0 as diferencia, MAX(h.fecha) as ultima_fecha  
				from vw_hist_academica h 
				where h.resultado = 'A'
				%s 
				group by  h.legajo, h.carrera, h.plan 
				
				into temp tmp_exa_legajo  
 				";
		
 		$extra = " ";
		
 		
 		$cars = $this->data['SiuAprobadasPlan']['carrera'];
		if (is_array($cars) && (count($cars)>0)) {
 			
 			
 			$lista_cars = implode(' , ', array_map(wrap_coma,$cars));
 		
 			
 			$extra .= " and h.carrera IN (".$lista_cars.") ";
 			
 		}
 		
 		if (trim($this->data['SiuAprobadasPlan']['legajo']) != "") {
 			$extra .= sprintf(" and h.legajo = '%s'", $this->data['SiuAprobadasPlan']['legajo']);
 		}
 		
 		
 		
 		/*
	 	//Forma ap.
 		if (trim($this->data['ConsultaAlumnosHistoaca']['formaap']) != 'T') {
 			$extra .= sprintf(" and h.forma_aprobacion = '%s' ", $this->data['ConsultaAlumnosHistoaca']['formaap']);
 		}
 		*/
 		
 		
 		
 		
 		//Plan
		if (trim($this->data['SiuAprobadasPlan']['plan']) != "") {
 			$extra .= sprintf(" and h.plan = '%s' ", $this->data['SiuAprobadasPlan']['plan']);
 		}
 		
 		$sql = sprintf($sql,
 						$extra
 						);
 		
 		$Model->query($sql);
 		
 		
 		$sql = "update tmp_exa_legajo set materias_plan = 
				(
				select count(*) 
				from sga_planes pl, sga_atrib_mat_plan mp
				where 
				pl.unidad_academica = mp.unidad_academica
				and pl.carrera = mp.carrera
				and pl.plan = mp.plan
				and pl.version_actual = mp.version
				and mp.tipo_materia <> 'O'
				and pl.carrera = tmp_exa_legajo.carrera
				and pl.plan = tmp_exa_legajo.plan
				) 				
 		"; 
 		
 		
 		$Model->query($sql);
 		
 		$sql = "SELECT legajo, carrera, plan, materias_plan, aprobadas, materias_plan - aprobadas as diferencia, ultima_fecha 
 				FROM tmp_exa_legajo 
 				WHERE 1 = 1 
 				%s
 		";
 		
 		$extra = "";
 		
 		if (trim($this->data['SiuAprobadasPlan']['diferenciad']) != "") {
 			$extra .= sprintf(" and (materias_plan - aprobadas) > %s", $this->data['SiuAprobadasPlan']['diferenciad']);
 		}
 		
 		if (trim($this->data['SiuAprobadasPlan']['diferenciah']) != "") {
 			$extra .= sprintf(" and (materias_plan - aprobadas) < %s", $this->data['SiuAprobadasPlan']['diferenciah']);
 		}
 		
 		$sql = sprintf($sql,$extra);
 		
 		$res = $Model->query($sql);
 		
		return $res;
 		
 	}
 		
 	function mapRow($row) {
 		
 		return array(
 			"Legajo" => $row[0]["legajo"],
 			"Carrera" => $row[0]["carrera"],
 			"Plan" => $row[0]["plan"],
 			"Materias Plan"  => $row[0]["materias_plan"],
 			"Aprobadas"  => $row[0]["aprobadas"],
 			"Diferencia"  => $row[0]["diferencia"],
 			"Ultima Fecha" => $row[0]["ultima_fecha"]
 		);
 	}
 	
 	function getInputParams() {
 		
 		App::import("Model","Carrera");
 		$MC = new Carrera();
 		
 		$MC->useDbConfig = $this->getDbFac($this->data['SiuAprobadasPlan']['facultad']);
 		
 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
 		
 		 
 		
 		$opt_car = array(
				'field' => 'SiuAprobadasPlan.carrera',
				'type' => 'select',
 				'multiple'=>true,
 				'size' => 10,
				'label' => 'Carrera',
				'title'  => 'Filtro por carrera',
				'options' => $cars,
 				'data-cascade-parent' => 'SiuAprobadasPlanFacultad'
		);
 		
		
		
		$opt_fac = array(
				'field' => 'SiuAprobadasPlan.facultad',
				'type' => 'select',
				'label' => 'Facultad',
				'title'  => 'Facultad que desea consultar.',
				'options' =>$this->session_usr["lista_fac"],
		);
		
		$opt_plan = array(
				'field' => 'SiuAprobadasPlan.plan',
				'type' => 'text',
				'label' => 'Plan',
				'title'  => 'Restringir a un unico plan',
				'options' => array(),
		);
		
 		$this->input_params[] = $opt_fac;
		$this->input_params[] = $opt_car;
		$this->input_params[] = $opt_plan;
 		
 		
 		return $this->input_params;
 		
 	}
 	
 	
 	
 	
 	function cascade_parent_change($child_id, $parent_value) {
		
 		App::import('Vendor', 'cake_util/data_cleaner');
 		
 		if ($child_id = "SiuAprobadasPlanCarrera") {
 			
 			$EL = array();
 			
 			App::import("Model","Carrera");
	 		$MC = new Carrera();
	 		$MC->useDbConfig = $this->getDbFac($parent_value);
	 		
	 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
	 		
	 		$Cleaner = new DataCleaner;
			
	 		foreach ($cars as $k=>$c) {
	 			$e = new CascadeElement;
	 			$e->When = $Cleaner->iso2utf8($parent_value);
	 			$e->Value = $Cleaner->iso2utf8($k);
	 			$e->Text = $Cleaner->iso2utf8($c);
	 			$EL[] = $e;
	 		}
	 		
	 		return $EL;
 			
 		}
 		
 		
 	}
	
}
?>