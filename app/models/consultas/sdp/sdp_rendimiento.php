<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


class SdpRendimiento extends ConsultaModel {
	
	var $name = "SdpRendimiento";
	var $cacheLifeTime = 864000; //24 horas (en segundos)
	var $cacheLifeTime = 0;
	
	var $filter_options = array (

			
			array(
				'field' => 'SdpRendimiento.apellido',
				'type' => 'text',
				'label' => 'Apellido',
				'tip'  => 'Puede agregar un filtro por apellido',
				'options' => array(),
			),
			array(
				'field' => 'SdpRendimiento.nombre',
				'type' => 'text',
				'label' => 'Nombre',
				'tip'  => 'Puede agregar un filtro por nombre',
				'options' => array(),
			),
			array(
				'field' => 'SdpRendimiento.documento',
				'type' => 'text',
				'label' => 'Documento',
				'tip'  => 'Puede agregar un filtro por el DNI',
				'options' => array(),
			),
			array(
				'field' => 'SdpRendimiento.sexo',
				'type' => 'select',
				'label' => 'Sexo',
				'tip'  => '',
				'options' => array(
					'0' => 'TODOS',
					'1' => 'Masculino',
					'2' => 'Femenino',
				),
			),
			array(
				'field' => 'SdpRendimiento.condicion',
				'type' => 'select',
				'label' => 'Condición',
				'tip'  => '',
				'options' => array(
					'0' => 'TODOS',
					'1' => 'Por Equivalencia',
					'2' => 'Mayor de 25 sin secundario',
				),
			),
	);

	var $validate = array(    
		
	);
	
	
function getInputParams() {
 		
 		App::import("Model","Carrera");
 		$MC = new Carrera();
 		
 		$MC->useDbConfig = $this->getDbFac($this->data['SdpRendimiento']['facultad']);
 		
 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
 		
 		App::import("Model","Centro");
 		$Centro = new Centro();
 		$Centro->useDbConfig = $this->getDbFac($this->data['SdpReinscriptos']['facultad']);
 		$centros = $Centro->find("list",array("order"=>"nombre"));
 		
 		
 		$opt_car = array(
				'field' => 'SdpRendimiento.carrera',
				'type' => 'select',
 				'multiple'=>true,
 				'size' => 10,		
				'label' => 'Carrera',
				'title'  => 'Filtro por carrera',
				'options' => $cars,
 				'data-cascade-parent' => 'SdpRendimientoFacultad'
		);
		
		$opt_centros = array(
				'field' => 'SdpRendimiento.centro',
				'type' => 'select',
 				'multiple'=>true,
 				'size' => 4,		
				'label' => 'Centro',
				'title'  => 'Filtro por centro',
				'options' => $centros,
 				'data-cascade-parent' => 'SdpRendimientoFacultad'
		);
		
		$opt_fac = array(
				'field' => 'SdpRendimiento.facultad',
				'type' => 'select',
				'label' => 'Facultad',
				'title'  => 'Facultad que desea consultar.',
				'options' =>$this->session_usr["lista_fac"],
		);
		
		$keys = range(2008,date("Y"));
		$vals = range(2008,date("Y"));
		$years = array_combine($keys,$vals);
		
		
 		$opt_lectivos = array(
				'field' => 'SdpRendimiento.lectivo',
				'type' => 'select',
				'label' => 'Lectivo',
				'title'  => 'Lectivo',
				'options' => $years
		);
		
		$opt_tipocar = array(
				'field' => 'SdpRendimiento.tipocar',
				'type' => 'select',
				'multiple'=>true,
 				'size' => 2,
				'label' => 'Tipo de Carrera',
				'title'  => 'Tipo de carrera',
				'options' => array('C'=>'Grado','R'=>'Pre-Grado'),
		);
		
		$this->input_params[] = array(
				'field' => 'SdpRendimiento.tipores',
				'type' => 'select',
				'label' => 'Resultados',
				'tip'  => '',
				'options' => array('L'=>'Lista de datos','R'=>'Cuadro resumido', 'RT'=>'Cuadro resumido con totales'),
			);
		
 		$this->input_params[] = $opt_fac;
		$this->input_params[] = $opt_car;
		$this->input_params[] = $opt_tipocar;
		$this->input_params[] = $opt_centros;
		$this->input_params[] = $opt_lectivos;
		
 		return $this->input_params;
 		
 	}
	
	function getData() {
 		
		App::import('Model', 'MateGuarani');
		$Model = new MateGuarani();
		$Model->useDbConfig = $this->getDbFac($this->data['SdpRendimiento']['facultad']);
		
		// Actas de examen y promocion (solo las del año de interes)
		$sql = "
				select e.acta, e.tipo_acta, e.anio_academico, e.turno_examen, e.mesa_examen from sga_actas_examen e 
				where e.anio_academico = %s
				
				union
				
				select '' || acta as acta, p.tipo as tipo_acta, c.anio_academico, '' as turno_examen, '' as mesa_examen 
				from sga_actas_promo p, sga_comisiones c
				where p.comision = c.comision and c.anio_academico = %s
								
				into temp tmp_actas_exaprom 
				
				";
		$sql = sprintf($sql,$this->data['SdpRendimiento']['lectivo'],$this->data['SdpRendimiento']['lectivo']);
		$Model->query($sql);			
 

		//Reinscriptos contra materias aprobadas en un año indicado
		$sql = "select h.legajo, count(h.materia) as materias FROM vw_hist_academica h, tmp_actas_exaprom a   
			where 
			h.resultado = 'A'
			and h.tipo_acta = a.tipo_acta
			and h.acta = a.acta 
			group by h.legajo
			into temp tmp_legajo_materias;
			";
		$sql = sprintf($sql);
		$Model->query($sql);

		
		
		// Rendimiento (REINSCRIPTOS)
		$sql = "
		select
			'%s' as tipores 
			,r.anio_academico
		    ,al.legajo
		    ,m.materias as materias_aprobadas
		    ,r.anio_academico as anio_reinscripcion
		    ,pi.anio_academico as anio_inscripcion
		    ,p.unidad_academica
		    ,p.apellido, p.nombres, p.tipo_documento, p.nro_documento
		    ,case when p.sexo=1 then 'M' when p.sexo=2 then 'F' end as sexo
		    ,case when p.nacionalidad = 2 then 'Extranjero' else 'Argentino' end as nacionalidad
		    ,c.carrera as cod_car
		    ,c.nombre as carrera
		    ,t.nombre as tipo_car 
		    ,s.nombre as centro
		    ,case when a.tipo_ingreso = 3 then 'S' else 'N' end as mayor_25
		    ,case when a.tipo_ingreso in (4,5,6,9,10) then 'S' else 'N' end as por_equivalencia
		          
		from sga_carrera_aspira a, sga_carreras c, sga_tipos_carrera t
		, sga_periodo_insc pi, sga_personas p, outer sga_sedes s
		, sga_alumnos al , sga_reinscripcion r, outer tmp_legajo_materias m
		
		where a.carrera=c.carrera and a.periodo_inscripcio=pi.periodo_inscripcio and a.nro_inscripcion=p.nro_inscripcion 
		and s.sede=a.sede
		and c.tipo_de_carrera = t.tipo_de_carrera
		and al.nro_inscripcion = a.nro_inscripcion 
		and al.carrera = a.carrera 
		and al.legajo = r.legajo
		
		and r.anio_academico > pi.anio_academico -- ¿con esto aseguramos que es reinscripto?
		and al.legajo = m.legajo
		and t.tipo_de_carrera in ('C', 'R') -- Solo las de pre-grado y grado
		
		%s
		
		";
		
// into temp tmp_reinscriptos_materias;
// -- drop table tmp_reinscriptos_materias;
// -- ---------------------------------------------------------------
		
		
 		$extra = " ";
 		
 		$cars = $this->data['SdpRendimiento']['carrera'];
 		if (is_array($cars) && (count($cars)>0)) {
 			$lista_cars = implode(' , ', array_map(wrap_coma,$cars));
 			$extra .= " and c.carrera IN (".$lista_cars.") ";
 		}
 		
 		////////////////////
		$tcar = $this->data['SdpRendimiento']['tipocar'];
 		if (is_array($tcar) && (count($tcar)>0)) {
 			$lista_tcar = implode(' , ', array_map(wrap_coma,$tcar));
 			$extra .= " and t.tipo_de_carrera IN (".$lista_tcar.") ";
 		}
 		
		$centros = $this->data['SdpRendimiento']['centro'];
 		if (is_array($centros) && (count($centros)>0)) {
 			$lista_cen = implode(' , ', array_map(wrap_coma,$centros));
 			$extra .= " and a.sede IN (".$lista_cen.") ";
 		}
 		
		if(trim($this->data['SdpRendimiento']['condicion']) == "2") {
			$extra .=  " and a.tipo_ingreso = 3 ";
		}
 		
		if(trim($this->data['SdpRendimiento']['condicion']) == "1") {
			$extra .=  " and a.tipo_ingreso in (4,5,6,9,10) ";
		}
		/////////////////////////////////
 		
		if(trim($this->data['SdpRendimiento']['apellido']) != "") {
			$extra .=  " and p.apellido LIKE '" . $this->data['SdpRendimiento']['apellido'] ."%' ";
		}
			
		if(trim($this->data['SdpRendimiento']['nombre']) != "") {
			$extra .= " and p.nombres LIKE '%" . $this->data['SdpRendimiento']['nombre'] ."%' ";
		}

		if(trim($this->data['SdpRendimiento']['documento']) != "") {
			$extra .= " and p.nro_documento = '" . $this->data['SdpRendimiento']['documento'] ."' ";
		}
		
		if($this->data['SdpRendimiento']['sexo'] != 0) {
			$extra .= "and p.sexo = " . $this->data['SdpRendimiento']['sexo'];
		}
 		
		//Lectivo
 		if(trim($this->data['SdpRendimiento']['lectivo']) != "") {
 			$extra .= sprintf(" and r.anio_academico + 0 = %s ", $this->data['SdpRendimiento']['lectivo'] );
 		}
		
 		
 		$sql = sprintf($sql,
 						$this->data['SdpRendimiento']['tipores'],
 						$extra
 						);
 						
 		
		
		
		
		// Vemos si quieren el resumen o el detalle
		if(($this->data['SdpRendimiento']['tipores'] == 'R') || ($this->data['SdpRendimiento']['tipores'] == 'RT')) {
			$sql .= " into temp tmp_reinscriptos_materias ";
			
			$Model->query($sql);
			
			// Agregar dinamicaente los centros!
			
			
			$sql = " SELECT distinct(nombre) as nombre FROM sga_sedes ";
			$sedes = $Model->query($sql);
			$sumSedes = "";
			foreach($sedes as $s) {
				$sumSedes .= sprintf(" ,SUM(CASE WHEN centro = '%s'  THEN 1 ELSE 0 END) as %s ",
							 $s[0]["nombre"],
							 str_replace(" ","_",$s[0]["nombre"])
							 );
			}
			
			
			$sql = "
				select
				    'R' as tipores
				    ,anio_academico
				    ,carrera , tipo_car
				    %s
				    ,SUM(CASE WHEN sexo = 'F'  THEN 1 ELSE 0 END) as Mujeres
				    ,SUM(CASE WHEN sexo = 'M'  THEN 1 ELSE 0 END) as Hombres
				    ,SUM(CASE WHEN materias_aprobadas is null and sexo = 'F' THEN 1 ELSE 0 END) as mujeres_materias_0
				    ,SUM(CASE WHEN materias_aprobadas is null and sexo = 'M' THEN 1 ELSE 0 END) as hombres_materias_0
				    ,SUM(CASE WHEN materias_aprobadas = 1 and sexo = 'F' THEN 1 ELSE 0 END) as mujeres_materia_1
				    ,SUM(CASE WHEN materias_aprobadas = 1 and sexo = 'M' THEN 1 ELSE 0 END) as hombres_materia_1
				    ,SUM(CASE WHEN materias_aprobadas = 2 and sexo = 'F' THEN 1 ELSE 0 END) as mujeres_materias_2
				    ,SUM(CASE WHEN materias_aprobadas = 2 and sexo = 'M' THEN 1 ELSE 0 END) as hombres_materias_2
				    ,SUM(CASE WHEN materias_aprobadas = 3 and sexo = 'F' THEN 1 ELSE 0 END) as mujeres_materias_3
				    ,SUM(CASE WHEN materias_aprobadas = 3 and sexo = 'M' THEN 1 ELSE 0 END) as hombres_materias_3
				    ,SUM(CASE WHEN materias_aprobadas = 4 and sexo = 'F' THEN 1 ELSE 0 END) as mujeres_materias_4
				    ,SUM(CASE WHEN materias_aprobadas = 4 and sexo = 'M' THEN 1 ELSE 0 END) as hombres_materias_4
				    ,SUM(CASE WHEN materias_aprobadas = 5 and sexo = 'F' THEN 1 ELSE 0 END) as mujeres_materias_5
				    ,SUM(CASE WHEN materias_aprobadas = 5 and sexo = 'M' THEN 1 ELSE 0 END) as hombres_materias_5
				    ,SUM(CASE WHEN materias_aprobadas >= 6  and sexo = 'F' THEN 1 ELSE 0 END) as mujeres_6_o_mas
				    ,SUM(CASE WHEN materias_aprobadas >= 6  and sexo = 'M' THEN 1 ELSE 0 END) as hombres_6_o_mas
				    ,count(*) as total_alumnos
				from tmp_reinscriptos_materias
				GROUP BY anio_academico,carrera, tipo_car
				ORDER BY anio_academico,carrera, tipo_car
			";
			$sql = sprintf($sql,$sumSedes);
			$res = $Model->query($sql);
			
			if ($this->data['SdpRendimiento']['tipores'] == 'RT') {
				$sql = "
					select
					    'R' as tipores
					    ,'TOTALES' as anio_academico
					    ,'' as carrera , '' as tipo_car
					    %s
					    ,SUM(CASE WHEN sexo = 'F'  THEN 1 ELSE 0 END) as Mujeres
					    ,SUM(CASE WHEN sexo = 'M'  THEN 1 ELSE 0 END) as Hombres
					    ,SUM(CASE WHEN materias_aprobadas is null and sexo = 'F' THEN 1 ELSE 0 END) as mujeres_materias_0
					    ,SUM(CASE WHEN materias_aprobadas is null and sexo = 'M' THEN 1 ELSE 0 END) as hombres_materias_0
					    ,SUM(CASE WHEN materias_aprobadas = 1 and sexo = 'F' THEN 1 ELSE 0 END) as mujeres_materia_1
					    ,SUM(CASE WHEN materias_aprobadas = 1 and sexo = 'M' THEN 1 ELSE 0 END) as hombres_materia_1
					    ,SUM(CASE WHEN materias_aprobadas = 2 and sexo = 'F' THEN 1 ELSE 0 END) as mujeres_materias_2
					    ,SUM(CASE WHEN materias_aprobadas = 2 and sexo = 'M' THEN 1 ELSE 0 END) as hombres_materias_2
					    ,SUM(CASE WHEN materias_aprobadas = 3 and sexo = 'F' THEN 1 ELSE 0 END) as mujeres_materias_3
					    ,SUM(CASE WHEN materias_aprobadas = 3 and sexo = 'M' THEN 1 ELSE 0 END) as hombres_materias_3
					    ,SUM(CASE WHEN materias_aprobadas = 4 and sexo = 'F' THEN 1 ELSE 0 END) as mujeres_materias_4
					    ,SUM(CASE WHEN materias_aprobadas = 4 and sexo = 'M' THEN 1 ELSE 0 END) as hombres_materias_4
					    ,SUM(CASE WHEN materias_aprobadas = 5 and sexo = 'F' THEN 1 ELSE 0 END) as mujeres_materias_5
					    ,SUM(CASE WHEN materias_aprobadas = 5 and sexo = 'M' THEN 1 ELSE 0 END) as hombres_materias_5
					    ,SUM(CASE WHEN materias_aprobadas >= 6  and sexo = 'F' THEN 1 ELSE 0 END) as mujeres_6_o_mas
					    ,SUM(CASE WHEN materias_aprobadas >= 6  and sexo = 'M' THEN 1 ELSE 0 END) as hombres_6_o_mas
					    ,count(*) as total_alumnos
					from tmp_reinscriptos_materias
				";
				
				$sql = sprintf($sql,$sumSedes);
				$resTot = $Model->query($sql);
				
				$res[] = $resTot[0];
			}
			
		} else {
		
		 	$res = $Model->query($sql);
 		
		}
 		
 		//echo $sql;
 		
		return $res;
 		
 	}
 	
 	
	function mapRow($row) {
	
 		if ($row[0]["tipores"] == 'L') {
	 			
	 		return array(
	 			"Lectivo" 	=> $row[0]["anio_academico"],
	 			"Centro" 	=> $row[0]["centro"],
	 			"Apellido" 	=> $row[0]["apellido"],
	 			"Nombres"  	=> $row[0]["nombres"],
	 			"Documento"	=> $row[0]["nro_documento"],
	 			"Sexo"		=> $row[0]["sexo"],
	 			"Carrera" 	=> $row[0]["carrera"],
	 			"Tipo de Carrera"=> $row[0]["tipo_car"],
	 			"Materias aprobadas"=> $row[0]["materias_aprobadas"],
	 		);
 		}
 		
		if (($row[0]["tipores"] == 'R') ||($row[0]["tipores"] == 'RT')) {

			unset($row[0]["tipores"]);
			
			$res = array();
			foreach($row[0] as $k=>$v) {
				$res[Inflector::humanize($k)] = $v;
			}
			return $res;
 		}
 		
 	}
	

	function cascade_parent_change($child_id, $parent_value) {
		
 		App::import('Vendor', 'cake_util/data_cleaner');
 		App::import('Vendor', 'cake_util/cascade_element');
 		
 		if ($child_id == "SdpRendimientoCarrera") {
 			
 			$EL = array();
 			
 			App::import("Model","Carrera");
	 		$MC = new Carrera();
	 		$MC->useDbConfig = $this->getDbFac($parent_value);
	 		
	 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
	 		
	 		$Cleaner = new DataCleaner;
			
	 		foreach ($cars as $k=>$c) {
	 			$e = new CascadeElement;
	 			$e->When = $Cleaner->iso2utf8($parent_value);
	 			$e->Value = $Cleaner->iso2utf8($k);
	 			$e->Text = $Cleaner->iso2utf8($c);
	 			$EL[] = $e;
	 		}
	 		
	 		return $EL;
 			
 		}
 		
		if ($child_id == "SdpRendimientoCentro") {
 			App::import("Model","Centro");
	 		$Centro = new Centro();
	 		$Centro->useDbConfig = $this->getDbFac($parent_value);
	 		$centros = $Centro->find("list",array("order"=>"nombre"));
	 		$EL = array();
	 		$Cleaner = new DataCleaner;
 			foreach ($centros as $k=>$c) {
	 			$e = new CascadeElement;
	 			$e->When = $Cleaner->iso2utf8($parent_value);
	 			$e->Value = $Cleaner->iso2utf8($k);
	 			$e->Text = $Cleaner->iso2utf8($c);
	 			$EL[] = $e;
	 		}
	 		return $EL;
 		}
 		
 	}
	
}