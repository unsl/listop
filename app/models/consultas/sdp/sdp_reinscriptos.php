<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


class SdpReinscriptos extends ConsultaModel {
	
	
	var $name = "SdpReinscriptos";
	var $cacheLifeTime = 864000; //24 horas (en segundos)
	

	
	var $filter_options = array (
			
			
			array(
				'field' => 'SdpReinscriptos.apellido',
				'type' => 'text',
				'label' => 'Apellido',
				'tip'  => 'Puede agregar un filtro por apellido',
				'options' => array(),
			),
			array(
				'field' => 'SdpReinscriptos.nombre',
				'type' => 'text',
				'label' => 'Nombre',
				'tip'  => 'Puede agregar un filtro por nombre',
				'options' => array(),
			),
			array(
				'field' => 'SdpReinscriptos.documento',
				'type' => 'text',
				'label' => 'Documento',
				'tip'  => 'Puede agregar un filtro por el DNI',
				'options' => array(),
			),
			array(
				'field' => 'SdpReinscriptos.sexo',
				'type' => 'select',
				'label' => 'Sexo',
				'tip'  => '',
				'options' => array(
					'0' => 'TODOS',
					'1' => 'Masculino',
					'2' => 'Femenino',
				),
			),
			array(
				'field' => 'SdpReinscriptos.condicion',
				'type' => 'select',
				'label' => 'Condición',
				'tip'  => '',
				'options' => array(
					'0' => 'TODOS',
					'1' => 'Por Equivalencia',
					'2' => 'Mayor de 25 sin secundario',
				),
			),
			array(
				'field' => 'SdpReinscriptos.regularidad',
				'type' => 'select',
				'label' => 'Regularidad actual',
				'tip'  => '',
				'options' => array(
					'0' => 'TODOS',
					'1' => 'Regular',
					'2' => 'No Regular',
				),
			),
	);

	var $validate = array(    
		
	);
	
	
function getInputParams() {
 		
 		App::import("Model","Carrera");
 		$MC = new Carrera();
 		$MC->useDbConfig = $this->getDbFac($this->data['SdpReinscriptos']['facultad']);
 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
 		
 		App::import("Model","Centro");
 		$Centro = new Centro();
 		$Centro->useDbConfig = $this->getDbFac($this->data['SdpReinscriptos']['facultad']);
 		$centros = $Centro->find("list",array("order"=>"nombre"));
 		
 		$opt_car = array(
				'field' => 'SdpReinscriptos.carrera',
				'type' => 'select',
 				'multiple'=>true,
 				'size' => 10,		
				'label' => 'Carrera',
				'title'  => 'Filtro por carrera',
				'options' => $cars,
 				'data-cascade-parent' => 'SdpReinscriptosFacultad'
		);
		
		
		$opt_centros = array(
				'field' => 'SdpReinscriptos.centro',
				'type' => 'select',
 				'multiple'=>true,
 				'size' => 4,		
				'label' => 'Centro',
				'title'  => 'Filtro por centro',
				'options' => $centros,
 				'data-cascade-parent' => 'SdpReinscriptosFacultad'
		);
		
		$opt_fac = array(
				'field' => 'SdpReinscriptos.facultad',
				'type' => 'select',
				'label' => 'Facultad',
				'title'  => 'Facultad que desea consultar.',
				'options' =>$this->session_usr["lista_fac"],
		);
		
		$keys = range(2008,date("Y"));
		$vals = range(2008,date("Y"));
		$years = array_combine($keys,$vals);
		
		$opt_lectivos = array(
				'field' => 'SdpReinscriptos.lectivo',
				'type' => 'select',
				'label' => 'Lectivo',
				'title'  => 'Lectivo',
				'options' => $years
		);
		
		$opt_tipocar = array(
				'field' => 'SdpReinscriptos.tipocar',
				'type' => 'select',
				'multiple'=>true,
 				'size' => 2,
				'label' => 'Tipo de Carrera',
				'title'  => 'Tipo de carrera',
				'options' => array('C'=>'Grado','R'=>'Pre-Grado'),
		);
		
		$this->input_params[] = array(
				'field' => 'SdpReinscriptos.tipores',
				'type' => 'select',
				'label' => 'Resultados',
				'tip'  => '',
				'options' => array('L'=>'Lista de datos','R'=>'Cuadro resumido', 'RT'=>'Cuadro resumido con totales'),
			);
		
 		$this->input_params[] = $opt_fac;
		$this->input_params[] = $opt_car;
		$this->input_params[] = $opt_tipocar;
		$this->input_params[] = $opt_centros;
		$this->input_params[] = $opt_lectivos;
		
 		return $this->input_params;
 		
 	}
	
	function getData() {
 		
 		$sql = "select
 					'%s' as tipores
 					,al.legajo 
				    ,r.anio_academico as anio_reinscripcion
				    ,pi.anio_academico  as anio_inscripcion
				    ,p.unidad_academica
				    ,p.apellido, p.nombres, p.tipo_documento, p.nro_documento
				    ,case when p.sexo=1 then 'M' when p.sexo=2 then 'F' end as sexo
				    ,case when p.nacionalidad = 2 then 'Extranjero' else 'Argentino' end as nacionalidad
				    ,c.carrera as cod_car
				    ,c.nombre as carrera
				    ,t.nombre as tipo_car 
				    ,s.nombre as centro
				    ,case when a.tipo_ingreso = 3 then 'S' else 'N' end as mayor_25
				    ,case when a.tipo_ingreso in (4,5,6,9,10) then 'S' else 'N' end as por_equivalencia
				          
				from sga_carrera_aspira a, sga_carreras c, sga_tipos_carrera t
				, sga_periodo_insc pi, sga_personas p, outer sga_sedes s
				, sga_alumnos al , sga_reinscripcion r
				
				where a.carrera=c.carrera and a.periodo_inscripcio=pi.periodo_inscripcio and a.nro_inscripcion=p.nro_inscripcion 
				and s.sede=a.sede
				and c.tipo_de_carrera = t.tipo_de_carrera
				and al.nro_inscripcion = a.nro_inscripcion 
				and al.carrera = a.carrera 
				and al.legajo = r.legajo
				and r.anio_academico > pi.anio_academico -- ¿con esto aseguramos que es reinscripto?
				

				%s
				
 				";
		
 		$extra = " ";
 		
 		$cars = $this->data['SdpReinscriptos']['carrera'];
 		if (is_array($cars) && (count($cars)>0)) {
 			$lista_cars = implode(' , ', array_map(wrap_coma,$cars));
 			$extra .= " and c.carrera IN (".$lista_cars.") ";
 		}
 		
 		////////////////////
		$tcar = $this->data['SdpReinscriptos']['tipocar'];
 		if (is_array($tcar) && (count($tcar)>0)) {
 			$lista_tcar = implode(' , ', array_map(wrap_coma,$tcar));
 			$extra .= " and t.tipo_de_carrera IN (".$lista_tcar.") ";
 		} else {
 			//Si no se indica una se filtra por las de grado y pregrado
 			$extra .= " and t.tipo_de_carrera IN ('C', 'R') ";
 		}
 		
		$centros = $this->data['SdpReinscriptos']['centro'];
 		if (is_array($centros) && (count($centros)>0)) {
 			$lista_cen = implode(' , ', array_map(wrap_coma,$centros));
 			$extra .= " and a.sede IN (".$lista_cen.") ";
 		}
 		
		if(trim($this->data['SdpReinscriptos']['condicion']) == "2") {
			$extra .=  " and a.tipo_ingreso = 3 ";
		}
 		
		if(trim($this->data['SdpReinscriptos']['condicion']) == "1") {
			$extra .=  " and a.tipo_ingreso in (4,5,6,9,10) ";
		}
		
		if ($this->data['SdpReinscriptos']['regulares'] == 1) {
 			$extra .= " and al.regular = 'S' ";
 		}

 		if ($this->data['SdpReinscriptos']['regulares'] == 2) {
 			$extra .= " and al.regular = 'N' ";
 		} 
		/////////////////////////////////
 		
		if(trim($this->data['SdpReinscriptos']['apellido']) != "") {
			$extra .=  " and p.apellido LIKE '" . $this->data['SdpReinscriptos']['apellido'] ."%' ";
		}

		
		
		if(trim($this->data['SdpReinscriptos']['nombre']) != "") {
			$extra .= " and p.nombres LIKE '%" . $this->data['SdpReinscriptos']['nombre'] ."%' ";
		}

		if(trim($this->data['SdpReinscriptos']['documento']) != "") {
			$extra .= " and p.nro_documento = '" . $this->data['SdpReinscriptos']['documento'] ."' ";
		}
		
		if($this->data['SdpReinscriptos']['sexo'] != 0) {
			$extra .= "and p.sexo = " . $this->data['SdpReinscriptos']['sexo'];
		}
 		
		//Lectivo
 		if(trim($this->data['SdpReinscriptos']['lectivo']) != "") {
 			$extra .= sprintf(" and r.anio_academico = %s ", $this->data['SdpReinscriptos']['lectivo'] );
 		}
		
 		
 		$sql = sprintf($sql,
 						$this->data['SdpReinscriptos']['tipores'],
 						$extra
 						);
 						
 		App::import('Model', 'MateGuarani');
		$Model = new MateGuarani();
		$Model->useDbConfig = $this->getDbFac($this->data['SdpReinscriptos']['facultad']);
		
		
		
		// Vemos si quieren el resumen o el detalle
		if(($this->data['SdpReinscriptos']['tipores'] == 'R') || ($this->data['SdpReinscriptos']['tipores'] == 'RT')) {
			$sql .= " into temp tmp_reinscriptos ";
			//pr($sql);
			$Model->query($sql);
			
			// Agregar dinamicaente los centros!
			
			
			$sql = " SELECT distinct(nombre) as nombre FROM sga_sedes ";
			$sedes = $Model->query($sql);
			$sumSedes = "";
			foreach($sedes as $s) {
				$sumSedes .= sprintf(" ,SUM(CASE WHEN centro = '%s'  THEN 1 ELSE 0 END) as %s ",
							 $s[0]["nombre"],
							 str_replace(" ","_",$s[0]["nombre"])
							 );
			}
			
			
			$sql = "
				select
				    'R' as tipores
				    ,anio_reinscripcion
				    ,carrera , tipo_car
				    %s
				    ,SUM(CASE WHEN sexo = 'F'  THEN 1 ELSE 0 END) as Mujeres
				    ,SUM(CASE WHEN sexo = 'M'  THEN 1 ELSE 0 END) as Hombres
				    ,SUM(CASE WHEN nacionalidad = 'Argentino'  THEN 1 ELSE 0 END) as Argentinos
				    ,SUM(CASE WHEN nacionalidad = 'Extranjero'  THEN 1 ELSE 0 END) as Extranjeros
				    ,SUM(CASE WHEN mayor_25 = 'S'  THEN 1 ELSE 0 END) as mayores_25
				    ,SUM(CASE WHEN por_equivalencia = 'S'  THEN 1 ELSE 0 END) as por_equivalencia
				    ,count(*) as total_alumnos
				from tmp_reinscriptos
				GROUP BY anio_reinscripcion,carrera, tipo_car
				ORDER BY anio_reinscripcion,carrera, tipo_car
			";
			$sql = sprintf($sql,$sumSedes);
			//pr($sql);
			//die();
			$res = $Model->query($sql);
			
			if ($this->data['SdpReinscriptos']['tipores'] == 'RT') {
				$sql = "
					select
					    'R' as tipores
					    ,'TOTALES' as anio_reinscripcion
					    ,'' as carrera , '' as tipo_car
					    %s
					    ,SUM(CASE WHEN sexo = 'F'  THEN 1 ELSE 0 END) as Mujeres
					    ,SUM(CASE WHEN sexo = 'M'  THEN 1 ELSE 0 END) as Hombres
					    ,SUM(CASE WHEN nacionalidad = 'Argentino'  THEN 1 ELSE 0 END) as Argentinos
					    ,SUM(CASE WHEN nacionalidad = 'Extranjero'  THEN 1 ELSE 0 END) as Extranjeros
					    ,SUM(CASE WHEN mayor_25 = 'S'  THEN 1 ELSE 0 END) as mayores_25
					    ,SUM(CASE WHEN por_equivalencia = 'S'  THEN 1 ELSE 0 END) as por_equivalencia
					    ,count(*) as total_alumnos
					from tmp_reinscriptos
				";
				
				$sql = sprintf($sql,$sumSedes);
				$resTot = $Model->query($sql);
				
				$res[] = $resTot[0];
			}
			
			$Model->query("DROP TABLE tmp_reinscriptos ");
			
		} else {
		
			//pr($sql);
			//die();
		 	$res = $Model->query($sql);
 		
		}
 		
 		//echo $sql;
 		
		return $res;
 		
 	}
 	
 	
	function mapRow($row) {
	
 		if ($row[0]["tipores"] == 'L') {
	 			
	 		return array(
	 			"Legajo" 	=> $row[0]["legajo"],
	 			"Lectivo Reinsc." 	=> $row[0]["anio_reinscripcion"],
	 			"Lectivo Insc." 	=> $row[0]["anio_inscripcion"],
	 			"Centro" 	=> $row[0]["centro"],
	 			"Apellido" 	=> $row[0]["apellido"],
	 			"Nombres"  	=> $row[0]["nombres"],
	 			"Documento"	=> $row[0]["nro_documento"],
	 			"Sexo"		=> $row[0]["sexo"],
	 			"Nacionalidad" => $row[0]["nacionalidad"],
	 			"Mayor de 25 sin sec." => $row[0]["mayor_25"],
	 			"Por Equivalencia" => $row[0]["por_equivalencia"],
	 			"Carrera" 	=> $row[0]["carrera"],
	 			"Tipo de Carrera"=> $row[0]["tipo_car"],
	 		);
 		}
 		
		if (($row[0]["tipores"] == 'R') ||($row[0]["tipores"] == 'RT')) {

			unset($row[0]["tipores"]);
			
			$res = array();
			foreach($row[0] as $k=>$v) {
				$res[Inflector::humanize($k)] = $v;
			}
			return $res;
 		}
 		
 	}
	

function cascade_parent_change($child_id, $parent_value) {
		
 		App::import('Vendor', 'cake_util/data_cleaner');
 		App::import('Vendor', 'cake_util/cascade_element');
 		
 		if ($child_id == "SdpReinscriptosCarrera") {
 			
 			$EL = array();
 			
 			App::import("Model","Carrera");
	 		$MC = new Carrera();
	 		$MC->useDbConfig = $this->getDbFac($parent_value);
	 		
	 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
	 		
	 		$Cleaner = new DataCleaner;
			
	 		foreach ($cars as $k=>$c) {
	 			$e = new CascadeElement;
	 			$e->When = $Cleaner->iso2utf8($parent_value);
	 			$e->Value = $Cleaner->iso2utf8($k);
	 			$e->Text = $Cleaner->iso2utf8($c);
	 			$EL[] = $e;
	 		}
	 		
	 		return $EL;
 			
 		}
 		
 		if ($child_id == "SdpReinscriptosCentro") {
 			App::import("Model","Centro");
	 		$Centro = new Centro();
	 		$Centro->useDbConfig = $this->getDbFac($parent_value);
	 		$centros = $Centro->find("list",array("order"=>"nombre"));
	 		$EL = array();
	 		$Cleaner = new DataCleaner;
 			foreach ($centros as $k=>$c) {
	 			$e = new CascadeElement;
	 			$e->When = $Cleaner->iso2utf8($parent_value);
	 			$e->Value = $Cleaner->iso2utf8($k);
	 			$e->Text = $Cleaner->iso2utf8($c);
	 			$EL[] = $e;
	 		}
	 		return $EL;
 		}
 		
 	}
 	
}