<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


/**
 * Esta consulta retorna un listado por carrera y año de estudio, 
 * con todas las regularidades (incluyendo aprobadas) que tiene un alumno, 
 */


class Cursadas extends ConsultaModel {
	
	var $name = "Cursadas";
	var $cacheLifeTime = 864000; //24 horas (en segundos)
	
	var $filter_options = array (
			array(
				'field' => 'Cursadas.legajo',
				'type' => 'text',
				'label' => 'Legajo',
				'title'  => 'Legajo',
			),
			array(
				'field' => 'Cursadas.fechad',
				'type' => 'text',
				'label' => 'Fecha Reg. Desde',
				'title'  => 'Fecha desde donde filtrar',
				'data-input-date' => 'yy-mm-dd'
			),
			array(
				'field' => 'Cursadas.fechah',
				'type' => 'text',
				'label' => 'Fecha Reg. Hasta',
				'title'  => 'Fecha hasta donde filtrar',
				'data-input-date' => 'yy-mm-dd'
			),
			array(
				'field' => 'Cursadas.vigenciard',
				'type' => 'text',
				'label' => 'Vigencia Reg. Desde',
				'title'  => 'Fecha desde donde filtrar',
				'data-input-date' => 'yy-mm-dd'
			),
			array(
				'field' => 'Cursadas.vigenciarh',
				'type' => 'text',
				'label' => 'Vigencia Reg. Hasta',
				'title'  => 'Fecha hasta donde filtrar',
				'data-input-date' => 'yy-mm-dd'
			),
	); 
	
		
		
	var $validate = array(
		'carrera' => array(
				'una_o_mas'=> array(
					'rule' => array('multiple', array('min' => 1)),
					'message' => 'Seleccione al menos una carrera.'
				),    
		),

 	);
	
 	function getData() {
 		
 		App::import('Model', 'MateGuarani');
		$Model = new MateGuarani();
 		
		$Model->useDbConfig = $this->getDbFac($this->data['Cursadas']['facultad']);

 		$sql = "select DISTINCT
			cu.fecha_regularidad || '' as fecha_regularidad, cu.fin_vigencia_regul || '' as fin_vigencia_regul, cu.resultado || '' as resultado, cu.estado || '' as estado
			,cu.legajo, cu.plan, m.nombre_materia, m.anio_de_cursada as anio, m.periodo_dictado, ca.nombre as nombre_carrera				
			, m.materia, m.carrera, m.version, m.unidad_academica
			, cu.origen, cu.nota
			, c.anio_academico, cu.materia
			, CASE WHEN cu.origen = 'C' THEN '' || cu.acta_regular WHEN cu.origen = 'P'  THEN '' || cu.acta_promocion ELSE '' END as acta
			from sga_cursadas cu, sga_atrib_mat_plan m 
			,sga_carreras ca , sga_comisiones c
			
			where 
			cu.comision = c.comision
			 
 			%s 
 			%s 
 			
			and cu.unidad_academica = m.unidad_academica 
			and cu.carrera = m.carrera 
			and cu.materia = m.materia 
			and cu.plan = m.plan 
			and cu.version = m.version
			and m.unidad_academica = ca.unidad_academica
			and m.carrera = ca.carrera

 		";
 		 		
 		$extra = " ";
 		
 		$cars = $this->data['Cursadas']['carrera'];
		if (is_array($cars) && (count($cars)>0)) {
 			
 			$lista_cars = implode(' , ', array_map(wrap_coma,$cars));
 		
 			$extra .= " and cu.carrera IN (".$lista_cars.") ";
 			
 		}
 		
 		if (trim($this->data['Cursadas']['legajo']) != "") {
 			$extra .= sprintf(" and cu.legajo = '%s'", $this->data['Cursadas']['legajo']);
 		}
 		
 		if (trim($this->data['Cursadas']['fechad']) != "") {
 			$extra .= sprintf(" and cu.fecha_regularidad >= datetime(%s) YEAR TO DAY ", $this->data['Cursadas']['fechad']);
 		}
 		
 		if (trim($this->data['Cursadas']['fechah']) != "") {
 			$extra .= sprintf(" and cu.fecha_regularidad <= datetime(%s) YEAR TO DAY ", $this->data['Cursadas']['fechah']);
 		}
 		
 		if (trim($this->data['Cursadas']['vigenciard']) != "") {
 			$extra .= sprintf(" and cu.fin_vigencia_regul >= datetime(%s) YEAR TO DAY ", $this->data['Cursadas']['vigenciard']);
 		}
 		if (trim($this->data['Cursadas']['vigenciarh']) != "") {
 			$extra .= sprintf(" and cu.fin_vigencia_regul <= datetime(%s) YEAR TO DAY ", $this->data['Cursadas']['vigenciarh']);
 		}
 		
 		//Plan
		if (trim($this->data['Cursadas']['plan']) != "") {
 			$extra .= sprintf(" and cu.plan = '%s' ", $this->data['Cursadas']['plan']);
 		}
 		
 		
 		//Lectivo
 		if($this->data['Cursadas']['lectivod'] <> 1959) {
 			$sql .= sprintf(" and c.anio_academico + 0 >= %s ", $this->data['Cursadas']['lectivod'] );
 		}
 		
 		if($this->data['Cursadas']['lectivoh'] <> 1959) {
 			$sql .= sprintf(" and c.anio_academico + 0 <= %s ", $this->data['Cursadas']['lectivoh'] );
 		}
 		
 		//Acta
 		if(trim($this->data['Cursadas']['acta']) != "") {
 			$sql .= sprintf(" and (cu.acta_regular = '%s' OR cu.acta_promocion = '%s') ", $this->data['Cursadas']['acta'], $this->data['Cursadas']['acta'] );
 		}
 		
	 	//Materia
 		if(trim($this->data['Cursadas']['materia']) != "") {
 			$sql .= sprintf(" and cu.materia = '%s' ", $this->data['Cursadas']['materia'] );
 		}
 		
 		//Resultado 		
 		$res = $this->data['Cursadas']['resultado'];
		if (is_array($res) && (count($res)>0)) {
 			
 			$lista_res = implode(' , ', array_map(wrap_coma,$res));
 		
 			$extra .= " and cu.resultado IN (".$lista_res.") ";
 			
 		}
 		
 		
 		//Origen
// 		if(trim($this->data['Cursadas']['origen']) != "T") {
// 			$sql .= sprintf(" and cu.origen = '%s' ", $this->data['Cursadas']['origen'] );
// 		}
 		
 		$ori = $this->data['Cursadas']['origen'];
		if (is_array($ori) && (count($ori)>0)) {
 			
 			$lista_ori = implode(' , ', array_map(wrap_coma,$ori));
 		
 			$extra .= " and cu.origen IN (".$lista_ori.") ";
 			
 		}
 		
		$anio = "";
 		if($this->data['Cursadas']['anio'] <> 0) {
 			$anio = sprintf(" and m.anio_de_cursada = %s ", $this->data['Cursadas']['anio'] );
 		}
 		
 		$sql = sprintf($sql,
 						$anio,
 						$extra
 						);
 		
 		
 		$res = $Model->query($sql);
 		
		return $res;
 		
 	}
 		
 	function mapRow($row) {

 		switch (trim($row[0]["resultado"])) {
 			case 'A' : {
 				$resultado = "Aprobado";
 				break;
 			}
 			case 'R' : {
 				$resultado = "Reprobado";
 				break;
 			}
 			case 'U' : {
 				$resultado = "Libre";
 				break;
 			}
 			case 'P' : {
 				$resultado = "Promoción";
 				break;
 			}
 			default : $resultado = $row[0]["resultado"];
 		}
 		
 		switch (trim($row[0]["origen"])) {
 			case 'C' : {
 				$origen = "Cursada";
 				break;
 			}
 			case 'P' : {
 				$origen = "Promoción";
 				break;
 			}
 			case 'E' : {
 				$origen = "Equivalencia";
 				break;
 			}
 			default : $origen = $row[0]["origen"];
 		}
 		
 		return array(
 			"Legajo" => $row[0]["legajo"],
 			"Carrera" => $row[0]["nombre_carrera"],
 			"Plan" => $row[0]["plan"],
 			"Año" => $row[0]["anio"],
 			"Cod. Mat." => $row[0]["materia"],
 			"Materia" => $row[0]["nombre_materia"],
 			"Lectivo" => $row[0]["anio_academico"],
 			"Período" => $row[0]["periodo_dictado"],
 			"Fecha regularidad" => $row[0]["fecha_regularidad"], 
 			"Fin regularidad" => $row[0]["fin_vigencia_regul"],
 			"Resultado" => $resultado,
 			"Origen" => $origen,
 			"Nota" => $row[0]["nota"],
 			"Estado" => $row[0]["estado"],
 			"Acta" => $row[0]["acta"]
 			
 		
 		);
 	}
 	
 	function getInputParams() {
 		
 		App::import("Model","Carrera");
 		App::import("Model","Plan");
 		$MC = new Carrera();
 		
 		$MC->useDbConfig = $this->getDbFac($this->data['Cursadas']['facultad']);
 		
 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
 		$opt_car = array(
				'field' => 'Cursadas.carrera',
				'type' => 'select',
 				'multiple'=>true,
 				'size' => 10,
				'label' => 'Carrera',
				'title'  => 'Filtro por carrera',
				'options' => $cars,
 				'data-cascade-parent' => 'CursadasFacultad'
		);
		
		
		$opt_plan = array(
				'field' => 'Cursadas.plan',
				'type' => 'text',
				'label' => 'Plan',
				'title'  => 'Restringir a un unico plan',
				'options' => array(),
		);
		
 		
 		
		$keys = range(0,6);
		$vals = range(0,6);
		$years = array_combine($keys,$vals);
		$years[0] = "Todos";
		$opt_anio = array(
				'field' => 'Cursadas.anio',
				'type' => 'select',
				'label' => 'Año en la Carrera',
				'title'  => 'Filtro por año',
				'options' => $years,
		);
		
		$opt_fac = array(
				'field' => 'Cursadas.facultad',
				'type' => 'select',
				'label' => 'Facultad',
				'title'  => 'Facultad que desea consultar.',
				'options' =>$this->session_usr["lista_fac"],
		);
		
 		$this->input_params[] = $opt_fac;
 		$this->input_params[] = $opt_car;
 		$this->input_params[] = $opt_plan;
 		$this->input_params[] = $opt_anio;
 		
 		return $this->input_params;
 		
 	}
 	
	function getFilterOptions() {
 		
 		$keys = range(1959,date("Y"));
		$vals = range(1959,date("Y"));
		$years = array_combine($keys,$vals);
		$years[1959] = "Todos";
		
 		$this->filter_options[] = array(
				'field' => 'Cursadas.lectivod',
				'type' => 'select',
				'label' => 'Lectivo Desde',
				'title'  => 'Lectivo Desde',
				'options' => $years
			);

		$this->filter_options[] = array(
				'field' => 'Cursadas.lectivoh',
				'type' => 'select',
				'label' => 'Lectivo Hasta',
				'title'  => 'Lectivo Hasta',
				'options' => $years
			);	
		
		$this->filter_options[] = array(
				'field' => 'Cursadas.materia',
				'type' => 'text',
				'label' => 'Cod. Mat.',
				'title'  => 'Materia',
			);
			
		$this->filter_options[] = array(
				'field' => 'Cursadas.acta',
				'type' => 'text',
				'label' => 'Acta',
				'title'  => 'Acta',
			);

		$this->filter_options[] = array(
				'field' => 'Cursadas.resultado',
				'type' => 'select',
 				'multiple'=>true,
 				'size' => 4,
				'label' => 'Resultado',
				'title'  => 'Resultado',
				'options' => array('A'=>'Aprobado', 'R'=>'Reprobado', 'U'=>'Libre', 'P'=>'Promoción'),
			);	

		$this->filter_options[] = array(
				'field' => 'Cursadas.origen',
				'type' => 'select',
				'multiple'=>true,
 				'size' => 3,
				'label' => 'Origen',
				'title'  => 'Origen',
				'options' => array('C'=>'Cursada', 'P'=>'Promoción', 'E'=>'Equivalencia' ),
			);	
			
		return $this->filter_options;
 	}
 	
 	
	function cascade_parent_change($child_id, $parent_value) {
		
 		App::import('Vendor', 'cake_util/data_cleaner');
 		App::import('Vendor', 'cake_util/cascade_element');
 		
 		if ($child_id = "CursadasCarrera") {
 			
 			$EL = array();
 			
 			App::import("Model","Carrera");
	 		$MC = new Carrera();
	 		$MC->useDbConfig = $this->getDbFac($parent_value);
	 		
	 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
	 		
	 		$Cleaner = new DataCleaner;
			
	 		foreach ($cars as $k=>$c) {
	 			$e = new CascadeElement;
	 			$e->When = $Cleaner->iso2utf8($parent_value);
	 			$e->Value = $Cleaner->iso2utf8($k);
	 			$e->Text = $Cleaner->iso2utf8($c);
	 			$EL[] = $e;
	 		}
	 		
	 		return $EL;	
 		
 		}
 		
	}
	
}
?>