<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


/**
 * Esta consulta retorna un listado por carrera y anio de estudio, 
 * con todas los examenes que tiene un alumno. 
 */

App::import('Vendor', 'cake_util/cascade_element');

ini_set("memory_limit", "1024M");
ini_set("max_execution_time", "3600");

class ConsultaAlumnosHistoaca extends ConsultaModel {
	
	var $name = "ConsultaAlumnosHistoaca";
	var $cacheLifeTime = 864000; //24 horas (en segundos)
	
	var $filter_options = array (
			array(
				'field' => 'ConsultaAlumnosHistoaca.legajo',
				'type' => 'text',
				'label' => 'Legajo',
				'title'  => 'Legajo',
			),
			array(
				'field' => 'ConsultaAlumnosHistoaca.resultado',
				'type' => 'select',
				'label' => 'Resultado',
				'title'  => 'Resultado',
				'options' => array('T'=>'Todos','A'=>'Aprobado', 'R'=>'Reprobado'),
			),
			/*
			array(
				'field' => 'ConsultaAlumnosHistoaca.formaap',
				'type' => 'select',
				'label' => 'Forma Ap.',
				'title'  => 'Forma de Aprobación',
				'options' => array('T' =>'Todos', 'Examen'=>'Exámen', 'Promoción'=>'Promoción', 'Equivalencia'=>'Equivalencia' ),
			),
			*/
			array(
				'field' => 'ConsultaAlumnosHistoaca.notad',
				'type' => 'text',
				'label' => 'Nota Desde',
				'title'  => 'Nota desde donde filtrar',
			),
			array(
				'field' => 'ConsultaAlumnosHistoaca.notah',
				'type' => 'text',
				'label' => 'Nota Hasta',
				'title'  => 'Nota hasta donde filtrar',
			),
			array(
				'field' => 'ConsultaAlumnosHistoaca.fechad',
				'type' => 'text',
				'label' => 'Fecha Desde',
				'title'  => 'Fecha desde donde filtrar',
				'data-input-date' => 'yy-mm-dd'
			),
			array(
				'field' => 'ConsultaAlumnosHistoaca.fechah',
				'type' => 'text',
				'label' => 'Fecha Hasta',
				'title'  => 'Fecha hasta donde filtrar',
				'data-input-date' => 'yy-mm-dd'
			),
			
	); 
	
		
		
	var $validate = array(    
		'carrera' => array(
				'una_o_mas'=> array(
					'rule' => array('multiple', array('min' => 1)),
					'message' => 'Seleccione al menos una carrera.'
				),    
		)
	);
	
 	function getData() {
 		
 		
 		App::import('Model', 'MateGuarani');
		$Model = new MateGuarani();
		$Model->useDbConfig = $this->getDbFac($this->data['ConsultaAlumnosHistoaca']['facultad']);
		
 		
 		$sql = "select acta, tipo_acta, anio_academico, turno_examen, mesa_examen from sga_actas_examen

				union
				
				select '' || acta as acta, p.tipo as tipo_acta, c.anio_academico, '' as turno_examen, '' as mesa_examen 
				from sga_actas_promo p, sga_comisiones c
				where p.comision = c.comision
				
				into temp tmp_actas_exaprom ";
 		
 		$Model->query($sql);
 		
 		
 		
 		$sql = "SELECT DISTINCT h.legajo, h.plan, m.materia, m.nombre_materia, m.anio_de_cursada as anio, m.periodo_dictado, h.resultado, h.forma_aprobacion, h.fecha, h.nota, c.nombre as carrera 
				,tipo_acta, CASE WHEN tipo_acta = 'N' THEN acta WHEN tipo_acta = 'P'  THEN '' || acta_promocion ELSE '' END as acta
 				from vw_hist_academica h, sga_atrib_mat_plan m, sga_carreras c 
 				where 
 				h.unidad_academica = m.unidad_academica 
 				%s 
 				%s
 				
 				and h.carrera = m.carrera
 				and h.plan = m.plan
 				and h.version = m.version
 				and h.materia = m.materia
 				and m.unidad_academica = c.unidad_academica
 				and m.carrera = c.carrera
 				
 				into temp tmp_histoaca_acta 
 				";
		
 		$extra = " ";
		
 		
 		$cars = $this->data['ConsultaAlumnosHistoaca']['carrera'];
		if (is_array($cars) && (count($cars)>0)) {
 			
 			
 			$lista_cars = implode(' , ', array_map(wrap_coma,$cars));
 		
 			
 			$extra .= " and h.carrera IN (".$lista_cars.") ";
 			
 		}
 		
 		if (trim($this->data['ConsultaAlumnosHistoaca']['legajo']) != "") {
 			$extra .= sprintf(" and h.legajo = '%s'", $this->data['ConsultaAlumnosHistoaca']['legajo']);
 		}
 		
 		if ($this->data['ConsultaAlumnosHistoaca']['resultado'] != 'T') {
 			$extra .= sprintf(" and h.resultado = '%s'",$this->data['ConsultaAlumnosHistoaca']['resultado']);
 		}
 		
 		/*
	 	//Forma ap.
 		if (trim($this->data['ConsultaAlumnosHistoaca']['formaap']) != 'T') {
 			$extra .= sprintf(" and h.forma_aprobacion = '%s' ", $this->data['ConsultaAlumnosHistoaca']['formaap']);
 		}
 		*/
 		
 		//Nota
 		if (trim($this->data['ConsultaAlumnosHistoaca']['notad']) != "") {
 			$extra .= sprintf(" and h.nota + 0 >= '%s' ", $this->data['ConsultaAlumnosHistoaca']['notad']);
 		}
 		
 		if (trim($this->data['ConsultaAlumnosHistoaca']['notah']) != "") {
 			$extra .= sprintf(" and h.nota + 0 <= '%s' ", $this->data['ConsultaAlumnosHistoaca']['notah']);
 		}
 		
 		//Fecha
 		if (trim($this->data['ConsultaAlumnosHistoaca']['fechad']) != "") {
 			$extra .= sprintf(" and h.fecha >= datetime(%s) YEAR TO DAY ", $this->data['ConsultaAlumnosHistoaca']['fechad']);
 		}
 		
 		if (trim($this->data['ConsultaAlumnosHistoaca']['fechah']) != "") {
 			$extra .= sprintf(" and h.fecha <= datetime(%s) YEAR TO DAY ", $this->data['ConsultaAlumnosHistoaca']['fechah']);
 		}
 		
 		//Plan
		if (trim($this->data['ConsultaAlumnosHistoaca']['plan']) != "") {
 			$extra .= sprintf(" and h.plan = '%s' ", $this->data['ConsultaAlumnosHistoaca']['plan']);
 		}
 		
	 	//Materia
 		if(trim($this->data['ConsultaAlumnosHistoaca']['materia']) != "") {
 			$extra .= sprintf(" and m.materia = '%s' ", trim($this->data['ConsultaAlumnosHistoaca']['materia']) );
 		}
 		
 		//Anio carrera
		$anio = "";
 		if($this->data['ConsultaAlumnosHistoaca']['anio'] <> 0) {
 			$anio = sprintf(" and m.anio_de_cursada = %s ", $this->data['ConsultaAlumnosHistoaca']['anio'] );
 		}
 		
 		
 		
 		$sql = sprintf($sql,
 						$anio,
 						$extra
 						);
 		
 		$Model->query($sql);
 		
 		
 		$sql = "SELECT a.anio_academico, a.turno_examen, a.mesa_examen, h.* 
 				FROM tmp_histoaca_acta h  , outer tmp_actas_exaprom a
 				WHERE h.tipo_acta = a.tipo_acta
 				and h.acta = a.acta 
 				into temp tmp_histo_result 				
 		"; 
 		
 		
 		$Model->query($sql);
 		
 		$sql = "SELECT * 
 				FROM tmp_histo_result 
 				WHERE 1 = 1 
 		";
 		
 		
 		
 		if($this->data['ConsultaAlumnosHistoaca']['lectivod'] <> 1959) {
 			$sql .= sprintf(" and anio_academico + 0 >= %s ", $this->data['ConsultaAlumnosHistoaca']['lectivod'] );
 		}
 		
 		if($this->data['ConsultaAlumnosHistoaca']['lectivoh'] <> 1959) {
 			$sql .= sprintf(" and anio_academico + 0 <= %s ", $this->data['ConsultaAlumnosHistoaca']['lectivoh'] );
 		}
 		
 		if(trim($this->data['ConsultaAlumnosHistoaca']['acta']) != "") {
 			$sql .= sprintf(" and acta = '%s' ", $this->data['ConsultaAlumnosHistoaca']['acta'] );
 		}
 		
 		
 		$res = $Model->query($sql);
 		
		return $res;
 		
 	}
 		
 	function mapRow($row) {
 		
 		if ($row[0]["resultado"] == "R") {
 			$resultado = "Reprobado";
 		}
 		elseif ($row[0]["resultado"] == "A") {
 			$resultado = "Aprobado";
 		}
 		else {
 			$resultado = $row[0]["resultado"];
 		}
 		
 		
 		return array(
 			"Legajo" => $row[0]["legajo"],
 			"Carrera" => $row[0]["carrera"],
 			"Año" => $row[0]["anio"],
 			"Cod. Mat." => $row[0]["materia"],
 			"Materia" => $row[0]["nombre_materia"],
 			"Plan" => $row[0]["plan"],
 			"Lectivo"  => $row[0]["anio_academico"],
 			"Período" => $row[0]["periodo_dictado"],
 			"Resultado" => $resultado,
 			"Forma Ap." => $row[0]["forma_aprobacion"],
 			"Nota." => $row[0]["nota"],
 			"Fecha Aprob."  => $row[0]["fecha"],
 			"Turno Exam."  => $row[0]["turno_examen"],
 			"Mesa Exam."  => $row[0]["mesa_examen"],
 			"Acta"  => $row[0]["acta"]
 			
 		);
 	}
 	
 	function getInputParams() {
 		
 		App::import("Model","Carrera");
 		$MC = new Carrera();
 		
 		$MC->useDbConfig = $this->getDbFac($this->data['ConsultaAlumnosHistoaca']['facultad']);
 		
 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
 		
 		 
 		
 		$opt_car = array(
				'field' => 'ConsultaAlumnosHistoaca.carrera',
				'type' => 'select',
 				'multiple'=>true,
 				'size' => 10,
				'label' => 'Carrera',
				'title'  => 'Filtro por carrera',
				'options' => $cars,
 				'data-cascade-parent' => 'ConsultaAlumnosHistoacaFacultad'
		);
 		
		$keys = range(0,6);
		$vals = range(0,6);
		$years = array_combine($keys,$vals);
		$years[0] = "Todos";
		$opt_anio = array(
				'field' => 'ConsultaAlumnosHistoaca.anio',
				'type' => 'select',
				'label' => 'Año Carrera',
				'title'  => 'Filtro por año',
				'options' => $years,
		);
		
		$opt_fac = array(
				'field' => 'ConsultaAlumnosHistoaca.facultad',
				'type' => 'select',
				'label' => 'Facultad',
				'title'  => 'Facultad que desea consultar.',
				'options' =>$this->session_usr["lista_fac"],
		);
		
		$opt_plan = array(
				'field' => 'ConsultaAlumnosHistoaca.plan',
				'type' => 'text',
				'label' => 'Plan',
				'title'  => 'Restringir a un unico plan',
				'options' => array(),
		);
		
 		$this->input_params[] = $opt_fac;
		$this->input_params[] = $opt_car;
		$this->input_params[] = $opt_plan;
 		$this->input_params[] = $opt_anio;
 		
 		return $this->input_params;
 		
 	}
 	
 	
 	function getFilterOptions() {
 		
 		$keys = range(1959,date("Y"));
		$vals = range(1959,date("Y"));
		$years = array_combine($keys,$vals);
		$years[1959] = "Todos";
		
 		$this->filter_options[] = array(
				'field' => 'ConsultaAlumnosHistoaca.lectivod',
				'type' => 'select',
				'label' => 'Lectivo Desde',
				'title'  => 'Lectivo Desde',
				'options' => $years
			);

		$this->filter_options[] = array(
				'field' => 'ConsultaAlumnosHistoaca.lectivoh',
				'type' => 'select',
				'label' => 'Lectivo Hasta',
				'title'  => 'Lectivo Hasta',
				'options' => $years
			);	
			
		$this->filter_options[] = array(
				'field' => 'ConsultaAlumnosHistoaca.materia',
				'type' => 'text',
				'label' => 'Cod. Mat.',
				'title'  => 'Materia',
			);	

		$this->filter_options[] = array(
				'field' => 'ConsultaAlumnosHistoaca.acta',
				'type' => 'text',
				'label' => 'Acta',
				'title'  => 'Acta',
			);

			
			
		return $this->filter_options;
 	}
 	
 	function cascade_parent_change($child_id, $parent_value) {
		
 		App::import('Vendor', 'cake_util/data_cleaner');
 		
 		if ($child_id = "ConsultaAlumnosHistoacaCarrera") {
 			
 			$EL = array();
 			
 			App::import("Model","Carrera");
	 		$MC = new Carrera();
	 		$MC->useDbConfig = $this->getDbFac($parent_value);
	 		
	 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
	 		
	 		$Cleaner = new DataCleaner;
			
	 		foreach ($cars as $k=>$c) {
	 			$e = new CascadeElement;
	 			$e->When = $Cleaner->iso2utf8($parent_value);
	 			$e->Value = $Cleaner->iso2utf8($k);
	 			$e->Text = $Cleaner->iso2utf8($c);
	 			$EL[] = $e;
	 		}
	 		
	 		return $EL;
 			
 		}
 		
 		
 	}
	
}
?>