<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

class SampleOrders extends ConsultaModel {
	
	var $name = "SampleOrders";
	var $cacheLifeTime = 0;
	var $a_prod = array(" ","Motorcycles", "Classic Cars", "Trucks and Buses", "Vintage Cars", "Planes", "Ships", "Trains"	);
	
	
	
	var $filter_options = array (
			
			array(
				'field' => 'SampleOrders.numero_ord',
				'type' => 'text',
				'label' => 'Nro. Orden',
				'tip'  => 'Puede filtrar por el nro de orden (valor exacto)',
				'options' => array(),
			),
			array(
				'field' => 'SampleOrders.numero',
				'type' => 'text',
				'label' => 'Nro. Cliente',
				'tip'  => 'Puede filtrar por el nro de cliente (valor exacto)',
				'options' => array(),
			),
			array(
				'field' => 'SampleOrders.nombre',
				'type' => 'text',
				'label' => 'Nombre Cliente',
				'tip'  => 'Puede agregar un filtro por nombre',
				'options' => array(),
			),
						
			array(
				'field' => 'SampleOrders.codigo',
				'type' => 'text',
				'label' => 'Código Prod.',
				'tip'  => 'Puede filtrar por el código del producto (valor exacto)',
				'options' => array(),
			),
			array(
				'field' => 'SampleOrders.nombre',
				'type' => 'text',
				'label' => 'Nombre Prod.',
				'tip'  => 'Puede agregar un filtro por nombre',
				'options' => array(),
			),
			
			array(
				'field' => 'SampleOrders.linea',
				'type' => 'select',
				'label' => 'Línea Prod.',
				'tip'  => '',
				'options' => array(
					" ","Motorcycles", "Classic Cars", "Trucks and Buses", "Vintage Cars", "Planes", "Ships", "Trains"					
				),
			),
			
	);

	var $validate = array(    
		
	);
	
	
	
	function getData() {
 		
 		$sql = "select o.orderNumber, p.productCode, p.productName, o.customerNumber, c.customerName, d.quantityOrdered, d.priceEach , o.orderDate 
				from sample_orderdetails d 
				    join sample_orders o on (d.orderNumber = o.orderNumber) 
				    join sample_customers c on (o.customerNumber = c.customerNumber )
				    join sample_products p on (d.productCode = p.productCode)
				where 1=1 %s
				order by o.orderDate
				 
 				";
		
 		$extra = " ";
 		
		if(trim($this->data['SampleOrders']['codigo']) != "") {
			$extra .=  " and d.productCode =  '" .$this->data['SampleOrders']['codigo'] . "'";
		}
		
		if(trim($this->data['SampleOrders']['nombre']) != "") {
			$extra .=  " and p.productName like  '%" .$this->data['SampleOrders']['nombre'] ."%' " ;
		}
		
				
		if(trim($this->data['SampleOrders']['linea']) != 0) {
			$extra .=  sprintf(" and p.productLine = '%s'", $this->a_prod[$this->data['SampleOrders']['linea']]);
		}
		
		if(trim($this->data['SampleOrders']['numero_ord']) != "") {
			$extra .=  " and o.orderNumber =  " .$this->data['SampleOrders']['numero_ord'] ;
		}
		
		if(trim($this->data['SampleOrders']['numero']) != "") {
			$extra .=  " and o.customerNumber =  " .$this->data['SampleOrders']['numero'] ;
		}
		
		if(trim($this->data['SampleOrders']['nombre']) != "") {
			$extra .=  " and c.customerName like  '%" .$this->data['SampleOrders']['nombre'] ."%' " ;
		}
		
 		$sql = sprintf($sql,$extra);
 		
 		//print_r($sql);
						
 		App::import('Model', 'Facultad');
		$Model = new Facultad();
		$res = $Model->query($sql);
		return $res;
 		
 	}
 	
 	
	function mapRow($row) {		
		$res = array();
		foreach ($row as $tfields){
			foreach($tfields as $k=>$v) {
				$res[Inflector::humanize($k)] = $v;
			}
		}
		return $res;
 	}

}
?>