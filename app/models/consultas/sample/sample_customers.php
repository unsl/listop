<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

class SampleCustomers extends ConsultaModel {
	
	var $name = "SampleCustomers";
	var $cacheLifeTime = 0;
	var $a_pais = array(" ","Australia","Austria","Belgium","Canada","Denmark","Finland","France","Germany","Hong Kong","Ireland","Israel","Italy","Japan","Netherlands","New Zealand","Norway","Philippines","Poland","Portugal","Russia","Singapore","South Africa","Spain","Sweden","Switzerland","UK","USA"	);
	var $filter_options = array (
						
			array(
				'field' => 'SampleCustomers.numero',
				'type' => 'text',
				'label' => 'Nro. Cliente',
				'tip'  => 'Puede filtrar por el nro de cliente (valor exacto)',
				'options' => array(),
			),
			array(
				'field' => 'SampleCustomers.nombre',
				'type' => 'text',
				'label' => 'Nombre',
				'tip'  => 'Puede agregar un filtro por nombre',
				'options' => array(),
			),
			
			array(
				'field' => 'SampleCustomers.ciudad',
				'type' => 'text',
				'label' => 'Ciudad',
				'tip'  => 'Por ejemplo pruebe escribir San',
				'options' => array(),
			),
			
			array(
				'field' => 'SampleCustomers.pais',
				'type' => 'select',
				'label' => 'País',
				'tip'  => '',
				'options' => array(
					" ","Australia","Austria","Belgium","Canada","Denmark","Finland","France","Germany","Hong Kong","Ireland","Israel","Italy","Japan","Netherlands","New Zealand","Norway","Philippines","Poland","Portugal","Russia","Singapore","South Africa","Spain","Sweden","Switzerland","UK","USA"					
				),
			),
			
	);

	var $validate = array(    
		
	);
	
	
	
	function getData() {
 		
 		$sql = "select
				    customerNumber, customerName, phone, city, state, postalCode, country, creditLimit 
				from sample_customers
				where  1 = 1 %s
				order by customerName 
 				";
		
 		$extra = " ";
 		
		if(trim($this->data['SampleCustomers']['numero']) != "") {
			$extra .=  " and customerNumber =  " .$this->data['SampleCustomers']['numero'] ;
		}
		
		if(trim($this->data['SampleCustomers']['nombre']) != "") {
			$extra .=  " and customerName like  '%" .$this->data['SampleCustomers']['nombre'] ."%' " ;
		}
		
		if(trim($this->data['SampleCustomers']['ciudad']) != "") {
			$extra .=  " and city like  '%" .$this->data['SampleCustomers']['ciudad'] ."%' " ;
		}
 		
		if(trim($this->data['SampleCustomers']['pais']) != 0) {
			$extra .=  sprintf(" and country = '%s'", $this->a_pais[$this->data['SampleCustomers']['pais']]);
		}
		
 		$sql = sprintf($sql,$extra);
 		
 		//print_r($sql);
						
 		App::import('Model', 'Facultad');
		$Model = new Facultad();
		$res = $Model->query($sql);
		return $res;
 		
 	}
 	
 	
	function mapRow($row) {		
		$res = array();
		foreach($row["sample_customers"] as $k=>$v) {
			$res[Inflector::humanize($k)] = $v;
		}
		return $res;
 	}

}
?>