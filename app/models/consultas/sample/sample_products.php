<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

class SampleProducts extends ConsultaModel {
	
	var $name = "SampleProducts";
	var $cacheLifeTime = 0;
	var $a_prod = array(" ","Motorcycles", "Classic Cars", "Trucks and Buses", "Vintage Cars", "Planes", "Ships", "Trains"	);
	
	
	
	var $filter_options = array (
						
			array(
				'field' => 'SampleProducts.codigo',
				'type' => 'text',
				'label' => 'Código',
				'tip'  => 'Puede filtrar por el código del producto (valor exacto)',
				'options' => array(),
			),
			array(
				'field' => 'SampleProducts.nombre',
				'type' => 'text',
				'label' => 'Nombre',
				'tip'  => 'Puede agregar un filtro por nombre',
				'options' => array(),
			),
			
			array(
				'field' => 'SampleProducts.linea',
				'type' => 'select',
				'label' => 'Línea',
				'tip'  => '',
				'options' => array(
					" ","Motorcycles", "Classic Cars", "Trucks and Buses", "Vintage Cars", "Planes", "Ships", "Trains"					
				),
			),
			
	);

	var $validate = array(    
		
	);
	
	
	
	function getData() {
 		
 		$sql = "select
				    * 
				from sample_products
				where  1 = 1 %s
				order by productName 
 				";
		
 		$extra = " ";
 		
		if(trim($this->data['SampleProducts']['codigo']) != "") {
			$extra .=  " and productCode =  '" .$this->data['SampleProducts']['codigo'] . "'";
		}
		
		if(trim($this->data['SampleProducts']['nombre']) != "") {
			$extra .=  " and productName like  '%" .$this->data['SampleProducts']['nombre'] ."%' " ;
		}
		
				
		if(trim($this->data['SampleProducts']['linea']) != 0) {
			$extra .=  sprintf(" and productLine = '%s'", $this->a_prod[$this->data['SampleProducts']['linea']]);
		}
		
 		$sql = sprintf($sql,$extra);
 		
 		//print_r($sql);
						
 		App::import('Model', 'Facultad');
		$Model = new Facultad();
		$res = $Model->query($sql);
		return $res;
 		
 	}
 	
 	
	function mapRow($row) {		
		$res = array();
		foreach($row["sample_products"] as $k=>$v) {
			$res[Inflector::humanize($k)] = $v;
		}
		return $res;
 	}

}
?>