<?php


/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


class GuaraniDocentes extends ConsultaModel {
	
	var $name = "GuaraniDocentes";
	var $cacheLifeTime = 0; //24 horas (en segundos)
	
	
	var $filter_options = array (
			
			array(
				'field' => 'GuaraniDocentes.inscripcion',
				'type' => 'text',
				'label' => 'Nro de Inscripción',
				'title'  => 'Inscripcón',
			),
			array(
				'field' => 'GuaraniDocentes.legajo',
				'type' => 'text',
				'label' => 'Legajo',
				'title'  => 'Legajo',
			),
			array(
				'field' => 'GuaraniDocentes.apellido',
				'type' => 'text',
				'label' => 'Apellido',
				'tip'  => 'Puede agregar un filtro por apellido',
				'options' => array(),
			),
			array(
				'field' => 'GuaraniDocentes.nombre',
				'type' => 'text',
				'label' => 'Nombre',
				'tip'  => 'Puede agregar un filtro por nombre',
				'options' => array(),
			),
			array(
				'field' => 'GuaraniDocentes.documento',
				'type' => 'text',
				'label' => 'Documento',
				'tip'  => 'Puede agregar un filtro por el DNI',
				'options' => array(),
			),
			array(
				'field' => 'GuaraniDocentes.sexo',
				'type' => 'select',
				'label' => 'Sexo',
				'tip'  => '',
				'options' => array(
					'0' => 'TODOS',
					'1' => 'Masculino',
					'2' => 'Femenino',
				),
			),
	);
	
	function getData() {
 		
 		$sql = "select d.nro_inscripcion,d.legajo, p.nro_documento, p.apellido, p.nombres, p.sexo 
				from sga_docentes d, sga_personas p 
				where d.nro_inscripcion = p.nro_inscripcion 
				%s 
 				order by p.apellido, p.nombres
 				";
		
 		$extra = " ";
 		
		if (trim($this->data['GuaraniDocentes']['inscripcion']) != "") {
 			$extra .= sprintf(" and d.nro_inscripcion = '%s' ", $this->data['GuaraniDocentes']['inscripcion']);
 		}
 		
		if (trim($this->data['GuaraniDocentes']['legajo']) != "") {
 			$extra .= sprintf(" and d.legajo = '%s' ", $this->data['GuaraniDocentes']['legajo']);
 		}
 		
		if(trim($this->data['GuaraniDocentes']['apellido']) != "") {
			$extra .=  " and p.apellido LIKE '" . $this->data['GuaraniDocentes']['apellido'] ."%' ";
		}
			
		if(trim($this->data['GuaraniDocentes']['nombre']) != "") {
			$extra .= " and p.nombres LIKE '%" . $this->data['GuaraniDocentes']['nombre'] ."%' ";
		}

		if(trim($this->data['GuaraniDocentes']['documento']) != "") {
			$extra .= " and p.nro_documento = '" . $this->data['GuaraniDocentes']['documento'] ."' ";
		}
		
		if($this->data['GuaraniDocentes']['sexo'] != 0) {
			$extra .= "and p.sexo = " . $this->data['GuaraniDocentes']['sexo'];
		}
 		
 		$sql = sprintf($sql,
 						$extra
 						);
 		
		
		$this->useDbConfig = $this->getDbFac($this->data['GuaraniDocentes']['facultad']);
 		
 		$res = $this->query($sql);
 		
		return $res;
 		
 	}
 	
	function mapRow($row) {
	
 		switch ($row[0]["sexo"]) {
 			case 1 : { 
 				$sexo = "M";
 				break;		
 			}
 			case 2 : { 
 				$sexo = "F";
 				break;		
 			}	
 		}
 		
 		
 		$res =  array(
 			"Inscripción" => $row[0]["nro_inscripcion"],
 			"Legajo" => $row[0]["legajo"],
 			"Documento" => $row[0]["nro_documento"],
 			"Apellido" => $row[0]["apellido"],
 			"Nombres" => $row[0]["nombres"],
 			"Sexo"	=> $sexo,
 			
 		);
 		return $res;
 		
 	}
 	
	function getInputParams() {
 	
		$opt_fac = array(
				'field' => 'GuaraniDocentes.facultad',
				'type' => 'select',
				'label' => 'Facultad',
				'title'  => 'Facultad que desea consultar.',
				'options' =>$this->session_usr["lista_fac"],
		);
		
 		$this->input_params[] = $opt_fac;
		
 		return $this->input_params;
 		
 	}
	
}