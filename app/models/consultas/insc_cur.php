<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


class InscCur extends ConsultaModel {
	
	var $name = "InscCur";
	var $cacheLifeTime = 864000; //24 horas (en segundos)
	
	var $filter_options = array (
			
			array(
				'field' => 'InscCur.legajo',
				'type' => 'text',
				'label' => 'Legajo',
				'title'  => 'Legajo',
			),
			array(
				'field' => 'InscCur.apellido',
				'type' => 'text',
				'label' => 'Apellido',
				'tip'  => 'Puede agregar un filtro por apellido',
				'options' => array(),
			),
			array(
				'field' => 'InscCur.nombre',
				'type' => 'text',
				'label' => 'Nombre',
				'tip'  => 'Puede agregar un filtro por nombre',
				'options' => array(),
			),
			array(
				'field' => 'InscCur.documento',
				'type' => 'text',
				'label' => 'Documento',
				'tip'  => 'Puede agregar un filtro por el DNI',
				'options' => array(),
			),
			array(
				'field' => 'InscCur.sexo',
				'type' => 'select',
				'label' => 'Sexo',
				'tip'  => '',
				'options' => array(
					'0' => 'TODOS',
					'1' => 'Masculino',
					'2' => 'Femenino',
				),
			),
			array(
				'field' => 'InscCur.fechad',
				'type' => 'text',
				'label' => 'Fecha Desde',
				'title'  => 'Fecha desde donde filtrar',
				'data-input-date' => 'yy-mm-dd'
			),
			array(
				'field' => 'InscCur.fechah',
				'type' => 'text',
				'label' => 'Fecha Hasta',
				'title'  => 'Fecha hasta donde filtrar',
				'data-input-date' => 'yy-mm-dd'
			),
			
	);
	
	var $validate = array(
		'carrera' => array(
				'una_o_mas'=> array(
					'rule' => array('multiple', array('min' => 1)),
					'message' => 'Seleccione al menos una carrera.'
				),    
		),

 	);
	
	function getData() {
		
		
		App::import('Model', 'MateGuarani');
		$Model = new MateGuarani();
		$Model->useDbConfig = $this->getDbFac($this->data['InscCur']['facultad']);
 		
		//Por una cuestion de performance se hace primero inscripciones join comisiones y se guarda en un temporal
		$sql = "select i.legajo, i.unidad_academica, i.carrera, i.plan, i.version, i.fecha_inscripcion, i.calidad_insc, c.anio_academico, c.periodo_lectivo, c.comision, c.materia
						from sga_insc_cursadas i, sga_comisiones c
						where i.comision = c.comision
						
						%s 
						
						into temp tmp_insc_com
						";
		$extra = " ";
 		
		$cars = $this->data['InscCur']['carrera'];
		if (is_array($cars) && (count($cars)>0)) {
 			
 			$lista_cars = implode(' , ', array_map(wrap_coma,$cars));
 		
 			$extra .= " and i.carrera IN (".$lista_cars.") ";
 			
 		}
		
		if (trim($this->data['InscCur']['legajo']) != "") {
 			$extra .= sprintf(" and i.legajo = '%s'", $this->data['InscCur']['legajo']);
 		}
 		
		//Fecha
 		if (trim($this->data['InscCur']['fechad']) != "") {
 			$extra .= sprintf(" and i.fecha_inscripcion >= datetime(%s) YEAR TO DAY ", $this->data['InscCur']['fechad']);
 		}
 		
 		if (trim($this->data['InscCur']['fechah']) != "") {
 			$extra .= sprintf(" and i.fecha_inscripcion <= datetime(%s) YEAR TO DAY ", $this->data['InscCur']['fechah']);
 		}
 		
 		//Plan
		if (trim($this->data['InscCur']['plan']) != "") {
 			$extra .= sprintf(" and i.plan = '%s' ", $this->data['InscCur']['plan']);
 		}
 		
		//Lectivo
 		if($this->data['InscCur']['lectivod'] <> 1959) {
 			$extra .= sprintf(" and c.anio_academico + 0 >= %s ", $this->data['InscCur']['lectivod'] );
 		}
 		
 		if($this->data['InscCur']['lectivoh'] <> 1959) {
 			$extra .= sprintf(" and c.anio_academico + 0 <= %s ", $this->data['InscCur']['lectivoh'] );
 		}
 		
		//Materia
 		if(trim($this->data['InscCur']['materia']) != "") {
 			$extra .= sprintf(" and c.materia = '%s' ", $this->data['InscCur']['materia'] );
 		}
 		
		
 		$sql = sprintf($sql,
 						$extra
 						);
 						
 		$Model->query($sql);
 		
 		
 		//////////////////////////////////////////////////////////////////////////////////////////////
 		//Ahora el resultado final.
 		
 		$sql = "
 				select t.* 
				        ,m.materia, m.nombre_materia, m.plan, m.version, ca.nombre as nombre_carrera  
				        ,p.apellido, p.nombres, p.nro_documento, p.sexo 
				from tmp_insc_com t, sga_atrib_mat_plan m , sga_carreras ca, sga_alumnos a, sga_personas p
				where t.unidad_academica = m.unidad_academica
				and t.carrera = m.carrera
				and t.materia = m.materia
				and t.plan = m.plan
				and t.version = m.version
				and m.unidad_academica = ca.unidad_academica
				and m.carrera = ca.carrera
				and t.legajo = a.legajo 
				and a.nro_inscripcion = p.nro_inscripcion
				
 				%s
 				
 				order by ca.nombre, m.nombre_materia 
 				";
		
 		$extra = " ";
 		
		if(trim($this->data['InscCur']['apellido']) != "") {
			$extra .=  " and p.apellido LIKE '" . $this->data['InscCur']['apellido'] ."%' ";
		}
			
		if(trim($this->data['InscCur']['nombre']) != "") {
			$extra .= " and p.nombres LIKE '%" . $this->data['InscCur']['nombre'] ."%' ";
		}

		if(trim($this->data['InscCur']['documento']) != "") {
			$extra .= " and p.nro_documento = '" . $this->data['InscCur']['documento'] ."' ";
		}
		
		if($this->data['InscCur']['sexo'] != 0) {
			$extra .= "and p.sexo = " . $this->data['InscCur']['sexo'];
		}
 		
		
 		$sql = sprintf($sql,
 						$extra
 						);
 						
 		$res = $Model->query($sql);
 		
 		//echo $sql;
 		
		return $res;
 		
 	}
 	
 	
	function mapRow($row) {
	
 		switch ($row[0]["sexo"]) {
 			case 1 : { 
 				$sexo = "M";
 				break;		
 			}
 			case 2 : { 
 				$sexo = "F";
 				break;		
 			}	
 		}
 		
		switch ($row[0]["calidad_insc"]) {
 			case 'R' : { 
 				$calidad = "Regular";
 				break;		
 			}
 			case 'P' : { 
 				$calidad = "Promoción";
 				break;		
 			}
 			default: {
 				$calidad = $row[0]["calidad_insc"];
 			}	
 		}
 		
 		//i.calidad_insc, c.anio_academico, c.periodo_lectivo, c.comision
 		
 		return array(
 			
 			"Legajo" => $row[0]["legajo"],
 			"Apellido" => $row[0]["apellido"],
 			"Nombres" => $row[0]["nombres"],
 			"Documento" => $row[0]["nro_documento"],
 			"Sexo"	=> $sexo,
 			"Calidad Insc." => $calidad,
 			"Fecha" => $row[0]["fecha_inscripcion"],
 			"Lectivo" => $row[0]["anio_academico"],
 			"Período"  => $row[0]["periodo_lectivo"],
 			"Carrera" => $row[0]["nombre_carrera"],
 			"Cod. Mat." => $row[0]["materia"],
 			"Materia." => $row[0]["nombre_materia"],
 			"Plan" => $row[0]["plan"],
 			"Comisión" => $row[0]["comision"],
 		);
 	}
 	
 	
	function getInputParams() {
 		
 		App::import("Model","Carrera");
 		$MC = new Carrera();
 		$MC->useDbConfig = $this->getDbFac($this->data['InscCur']['facultad']);
 		
 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
 		
 		 
 		
 		$opt_car = array(
				'field' => 'InscCur.carrera',
				'type' => 'select',
 				'multiple'=>true,
 				'size' => 10,
				'label' => 'Carrera',
				'title'  => 'Filtro por carrera',
				'options' => $cars,
 				'data-cascade-parent' => 'InscCurFacultad'
		);
		
		$opt_fac = array(
				'field' => 'InscCur.facultad',
				'type' => 'select',
				'label' => 'Facultad',
				'title'  => 'Facultad que desea consultar.',
				'options' =>$this->session_usr["lista_fac"],
		);
		
		$opt_plan = array(
				'field' => 'InscCur.plan',
				'type' => 'text',
				'label' => 'Plan',
				'title'  => 'Restringir a un unico plan',
				'options' => array(),
		);
		
 		$this->input_params[] = $opt_fac;
		$this->input_params[] = $opt_car;
		$this->input_params[] = $opt_plan;
 		
 		return $this->input_params;
 		
 	}
 	
function getFilterOptions() {
 		
 		$keys = range(1959,date("Y"));
		$vals = range(1959,date("Y"));
		$years = array_combine($keys,$vals);
		$years[1959] = "Todos";
		
 		$this->filter_options[] = array(
				'field' => 'InscCur.lectivod',
				'type' => 'select',
				'label' => 'Lectivo Desde',
				'title'  => 'Lectivo Desde',
				'options' => $years
			);

		$this->filter_options[] = array(
				'field' => 'InscCur.lectivoh',
				'type' => 'select',
				'label' => 'Lectivo Hasta',
				'title'  => 'Lectivo Hasta',
				'options' => $years
			);	
			
		$this->filter_options[] = array(
				'field' => 'InscCur.materia',
				'type' => 'text',
				'label' => 'Cod. Mat.',
				'title'  => 'Materia',
			);
			
		return $this->filter_options;
 	}
 	
	function cascade_parent_change($child_id, $parent_value) {
		
 		App::import('Vendor', 'cake_util/data_cleaner');
 		App::import('Vendor', 'cake_util/cascade_element');
 		
 		if ($child_id = "AlumnosCarrera") {
 			
 			$EL = array();
 			
 			App::import("Model","Carrera");
	 		$MC = new Carrera();
	 		$MC->useDbConfig = $this->getDbFac($parent_value);
	 		
	 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
	 		
	 		$Cleaner = new DataCleaner;
			
	 		foreach ($cars as $k=>$c) {
	 			$e = new CascadeElement;
	 			$e->When = $Cleaner->iso2utf8($parent_value);
	 			$e->Value = $Cleaner->iso2utf8($k);
	 			$e->Text = $Cleaner->iso2utf8($c);
	 			$EL[] = $e;
	 		}
	 		
	 		return $EL;
 			
 		}
 		
 		
 	}
	
	
}

?>