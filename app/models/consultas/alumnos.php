<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


class Alumnos extends ConsultaModel {
	
	var $name = "Alumnos";
	var $cacheLifeTime = 864000; //24 horas (en segundos)
	
	var $filter_options = array (
			array(
				'field' => 'Alumnos.incluir_domicilios',
				'type' => 'checkbox',
				'label' => 'Incluir Domicilio',
				'title'  => 'Incluir datos del domicilio en los resultados.',
				'options' => array(),
			),
			array(
				'field' => 'Alumnos.regulares',
				'type' => 'checkbox',
				'label' => 'Sólo alumnos regulares',
				'title'  => 'Solo alumnos en condicion regular.',
				'options' => array(),
			),
			array(
				'field' => 'Alumnos.calidad',
				'type' => 'select',
				'multiple'=>true,
 				'size' => 4,
				'label' => 'Calidad',
				'tip'  => '',
				'options' => array(
					'A' => 'Activo',
					'E' => 'Egresado',
					'N' => 'No Activo',
					'P' => 'Pasivo',
				),
			),
			array(
				'field' => 'Alumnos.legajo',
				'type' => 'text',
				'label' => 'Legajo',
				'title'  => 'Legajo',
			),
			array(
				'field' => 'Alumnos.apellido',
				'type' => 'text',
				'label' => 'Apellido',
				'tip'  => 'Puede agregar un filtro por apellido',
				'options' => array(),
			),
			array(
				'field' => 'Alumnos.nombre',
				'type' => 'text',
				'label' => 'Nombre',
				'tip'  => 'Puede agregar un filtro por nombre',
				'options' => array(),
			),
			array(
				'field' => 'Alumnos.documento',
				'type' => 'text',
				'label' => 'Documento',
				'tip'  => 'Puede agregar un filtro por el DNI',
				'options' => array(),
			),
			array(
				'field' => 'Alumnos.sexo',
				'type' => 'select',
				'label' => 'Sexo',
				'tip'  => '',
				'options' => array(
					'0' => 'TODOS',
					'1' => 'Masculino',
					'2' => 'Femenino',
				),
			),
			array(
				'field' => 'Alumnos.fechad',
				'type' => 'text',
				'label' => 'Ingreso Desde',
				'title'  => 'Fecha desde donde filtrar',
				'data-input-date' => 'yy-mm-dd'
			),
			array(
				'field' => 'Alumnos.fechah',
				'type' => 'text',
				'label' => 'Ingreso Hasta',
				'title'  => 'Fecha hasta donde filtrar',
				'data-input-date' => 'yy-mm-dd'
			),
			array(
				'field' => 'Alumnos.promediod',
				'type' => 'text',
				'label' => 'Promedio Desde',
			),
			array(
				'field' => 'Alumnos.promedioh',
				'type' => 'text',
				'label' => 'Promedio Hasta',
			),
			array(
				'field' => 'Alumnos.promediosad',
				'type' => 'text',
				'label' => 'Promedio (s/a) Desde',
			),
			array(
				'field' => 'Alumnos.promediosah',
				'type' => 'text',
				'label' => 'Promedio (s/a) Hasta',
			),
			
	);
		
	var $validate = array(    
		/*
		'carrera' => array(
				'una_o_mas'=> array(
					'rule' => array('multiple', array('min' => 1)),
					'message' => 'Seleccione al menos una carrera.'
				),    
		)
		*/
	);
	
	function getData() {
 		
 		$sql = "SELECT DISTINCT pi.anio_academico, a.unidad_academica, p.nro_documento, p.apellido, p.nombres, p.fecha_nacimiento, a.legajo, a.plan, c.nombre as carrera, a.fecha_ingreso
 				,sp_prom_alumnos(a.unidad_academica,a.carrera,a.legajo,'S') as promedio_ca
				,sp_prom_alumnos(a.unidad_academica,a.carrera,a.legajo,'N') as promedio_sa 
				,d.e_mail, d.te_per_lect, d.te_proc, d.te_pers_alleg, p.sexo
				,d.calle_per_lect, d.numero_per_lect, d.piso_per_lect, d.dpto_per_lect
				,a.regular, a.carrera as cod_car  
 				from sga_alumnos a, sga_personas p, sga_carreras c, sga_carrera_aspira ca, sga_periodo_insc pi
 				 , outer sga_datos_censales d 
 				where 
 				 
 				ca.situacion_asp in ('IL','IC') 
 				and ca.carrera = a.carrera
    			and ca.nro_inscripcion = a.nro_inscripcion 
    			and ca.periodo_inscripcio = pi.periodo_inscripcio
 				%s
 				and a.carrera = c.carrera
 				and a.nro_inscripcion = p.nro_inscripcion
 				and d.unidad_academica = p.unidad_academica 
				and d.nro_inscripcion = p.nro_inscripcion
				and d.fecha_relevamiento = (
					SELECT max(fecha_relevamiento) FROM sga_datos_censales d2
					WHERE d2.unidad_academica = d.unidad_academica
					AND d2.nro_inscripcion = d.nro_inscripcion
				)
 				order by p.apellido, p.nombres
 				";
		
 		$extra = " ";
 		
 		$cars = $this->data['Alumnos']['carrera'];
		if (is_array($cars) && (count($cars)>0)) {

 			$lista_cars = implode(' , ', array_map(wrap_coma,$cars));
 		
 		
 			$extra .= " and a.carrera IN (".$lista_cars.") ";
 			
 		}
 		
 		
 		
 		
		if (trim($this->data['Alumnos']['legajo']) != "") {
 			$extra .= sprintf(" and a.legajo = '%s' ", $this->data['Alumnos']['legajo']);
 		}
 		
		
		$calis = $this->data['Alumnos']['calidad'];
		if (is_array($calis) && (count($calis)>0)) {
 			$lista_calis = implode(' , ', array_map(wrap_coma,$calis));
 			$extra .= " and a.calidad IN (".$lista_calis.") ";
 		}
 		
 		
 		if ($this->data['Alumnos']['regulares'] == 1) {
 			$extra .= " and a.regular = 'S' ";
 		} 
 		
 		
 		if($this->data['Alumnos']['anio_ing'] > 1979) {
 			$extra .= sprintf(" and YEAR(a.fecha_ingreso) = %s ", $this->data['Alumnos']['anio_ing'] );
 		}
 		
		if(trim($this->data['Alumnos']['apellido']) != "") {
			$extra .=  " and p.apellido LIKE '" . $this->data['Alumnos']['apellido'] ."%' ";
		}
			
		if(trim($this->data['Alumnos']['nombre']) != "") {
			$extra .= " and p.nombres LIKE '%" . $this->data['Alumnos']['nombre'] ."%' ";
		}

		if(trim($this->data['Alumnos']['documento']) != "") {
			$extra .= " and p.nro_documento = '" . $this->data['Alumnos']['documento'] ."' ";
		}
		
		if($this->data['Alumnos']['sexo'] != 0) {
			$extra .= "and p.sexo = " . $this->data['Alumnos']['sexo'];
		}
 		
		//Fecha
 		if (trim($this->data['Alumnos']['fechad']) != "") {
 			$extra .= sprintf(" and a.fecha_ingreso >= datetime(%s) YEAR TO DAY ", $this->data['Alumnos']['fechad']);
 		}
 		
 		if (trim($this->data['Alumnos']['fechah']) != "") {
 			$extra .= sprintf(" and a.fecha_ingreso <= datetime(%s) YEAR TO DAY ", $this->data['Alumnos']['fechah']);
 		}
 		
 		//Promedios
		if (trim($this->data['Alumnos']['promediod']) != "") {
 			$extra .= sprintf(" and sp_prom_alumnos(a.unidad_academica,a.carrera,a.legajo,'S') + 0  >= %s ", $this->data['Alumnos']['promediod']);
 		}
		if (trim($this->data['Alumnos']['promedioh']) != "") {
 			$extra .= sprintf(" and sp_prom_alumnos(a.unidad_academica,a.carrera,a.legajo,'S') + 0 <= %s ", $this->data['Alumnos']['promedioh']);
 		}
 		
		if (trim($this->data['Alumnos']['promediosad']) != "") {
 			$extra .= sprintf(" and sp_prom_alumnos(a.unidad_academica,a.carrera,a.legajo,'N') + 0 >= %s ", $this->data['Alumnos']['promediosad']);
 		}
		if (trim($this->data['Alumnos']['promediosah']) != "") {
 			$extra .= sprintf(" and sp_prom_alumnos(a.unidad_academica,a.carrera,a.legajo,'N') + 0 <= %s ", $this->data['Alumnos']['promediosah']);
 		}
 		
 		//Plan
		if (trim($this->data['Alumnos']['plan']) != "") {
 			$extra .= sprintf(" and a.plan = '%s' ", $this->data['Alumnos']['plan']);
 		}
 		
 		
		//Lectivo
 		if($this->data['Alumnos']['lectivod'] <> 1959) {
 			$extra .= sprintf(" and pi.anio_academico + 0 >= %s ", $this->data['Alumnos']['lectivod'] );
 		}
 		
 		if($this->data['Alumnos']['lectivoh'] <> 1959) {
 			$extra .= sprintf(" and pi.anio_academico + 0 <= %s ", $this->data['Alumnos']['lectivoh'] );
 		}
 		
 		
 		$sql = sprintf($sql,
 						$extra
 						);
 		
 						
 		App::import('Model', 'MateGuarani');
		$Model = new MateGuarani();
		
		$Model->useDbConfig = $this->getDbFac($this->data['Alumnos']['facultad']);
 		
 		$res = $Model->query($sql);
 		
 		//echo $sql;
 		
		return $res;
 		
 	}
 	
 	
	function mapRow($row) {
	
 		$atel = array();
 		
 		if($row[0]["te_per_lect"] != "" ) {
 			$atel[] = $row[0]["te_per_lect"];
 		}
 		
 		if($row[0]["te_proc"] != "" ) {
 			$atel[] = $row[0]["te_proc"];
 		}
 		
 		if($row[0]["te_pers_alleg"] != "" ) {
 			$atel[] = $row[0]["te_pers_alleg"];
 		}
 		
 		$tel = implode(" ; ",$atel);
 		
 		switch ($row[0]["sexo"]) {
 			case 1 : { 
 				$sexo = "M";
 				break;		
 			}
 			case 2 : { 
 				$sexo = "F";
 				break;		
 			}	
 		}
 		
 		
 		$res =  array(
 			
 			"Apellido" => $row[0]["apellido"],
 			"Nombres" => $row[0]["nombres"],
 			"Documento" => $row[0]["nro_documento"],
 			"Sexo"	=> $sexo,
 			"Fecha Nac." => $row[0]["fecha_nacimiento"],
 			"Fecha de Ingreso" => $row[0]["fecha_ingreso"],
 			"Lectivo" => $row[0]["anio_academico"],
 			"Legajo" => $row[0]["legajo"],
 			"Cod. Car." => $row[0]["cod_car"],
 			"Carrera" => $row[0]["carrera"],
 			"Plan" => $row[0]["plan"],
 			"Regular" => $row[0]["regular"],
 			"Promedio" => $row[0]["promedio_ca"],
 			"Promedio S/A" => $row[0]["promedio_sa"],
 			"E-mail" => $row[0]["e_mail"],
 			"Teléfonos" => $tel
 		);
 		
 		//d.calle_per_lect, d.numero_per_lect, d.piso_per_lect, d.dpto_per_lect
 		if ($this->data["Alumnos"]["incluir_domicilios"] == 1 ) {
 			$res["Calle"] = $row[0]["calle_per_lect"];
 			$res["Nro"] = $row[0]["numero_per_lect"];
 			$res["Piso"] = $row[0]["piso_per_lect"];
 			$res["Dpto."] = $row[0]["dpto_per_lect"];
 		}
 		
 		
 		return $res;
 		
 	}
 	
 	
	function getInputParams() {
 		
 		App::import("Model","Carrera");
 		$MC = new Carrera();
 		
 		$MC->useDbConfig = $this->getDbFac($this->data['Alumnos']['facultad']);
 		
 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
 		
 		$opt_car = array(
				'field' => 'Alumnos.carrera',
				'type' => 'select',
 				'multiple'=>true,
 				'size' => 10,
				'label' => 'Carrera',
				'title'  => 'Filtro por carrera',
				'options' => $cars,
 				'data-cascade-parent' => 'AlumnosFacultad'
		);
		
		$keys = range(1979,date("Y"));
		$vals = range(1979,date("Y"));
		$yearsAlu = array_combine($keys,$vals);
		$yearsAlu[1979] = "Todos";
		$opt_anio_alu = array(
				'field' => 'Alumnos.anio_ing',
				'type' => 'select',
				'label' => 'Año de Ingreso',
				'title'  => 'Año de ingreso del alumno',
				'options' => $yearsAlu,
				//'selected' => date("Y")
		);
		
		$opt_fac = array(
				'field' => 'Alumnos.facultad',
				'type' => 'select',
				'label' => 'Facultad',
				'title'  => 'Facultad que desea consultar.',
				'options' =>$this->session_usr["lista_fac"],
		);
		
		$opt_plan = array(
				'field' => 'Alumnos.plan',
				'type' => 'text',
				'label' => 'Plan',
				'title'  => 'Restringir a un unico plan',
				'options' => array(),
		);
		
 		$this->input_params[] = $opt_fac;
		$this->input_params[] = $opt_car;
		$this->input_params[] = $opt_plan;
 		//$this->input_params[] = $opt_anio_alu;
 		
 		return $this->input_params;
 		
 	}
 	
function getFilterOptions() {
 		
 		$keys = range(1959,date("Y"));
		$vals = range(1959,date("Y"));
		$years = array_combine($keys,$vals);
		$years[1959] = "Todos";
		
 		$this->filter_options[] = array(
				'field' => 'Alumnos.lectivod',
				'type' => 'select',
				'label' => 'Lectivo Desde',
				'title'  => 'Lectivo Desde',
				'options' => $years
			);

		$this->filter_options[] = array(
				'field' => 'Alumnos.lectivoh',
				'type' => 'select',
				'label' => 'Lectivo Hasta',
				'title'  => 'Lectivo Hasta',
				'options' => $years
			);	
			
		return $this->filter_options;
 	}
 	
	function cascade_parent_change($child_id, $parent_value) {
		
 		App::import('Vendor', 'cake_util/data_cleaner');
 		App::import('Vendor', 'cake_util/cascade_element');
 		
 		if ($child_id = "AlumnosCarrera") {
 			
 			$EL = array();
 			
 			App::import("Model","Carrera");
	 		$MC = new Carrera();
	 		$MC->useDbConfig = $this->getDbFac($parent_value);
	 		
	 		switch($parent_value){
	 			case 2: {
	 				$MC->useDbConfig = "quim_guarani";
	 				break;
	 			}
	 			case 3: {
	 				$MC->useDbConfig = "mate_guarani";
	 				break;
	 			}
	 			case 5: {
	 				$MC->useDbConfig = "fices_guarani";
	 				break;
	 			}
	 		}
	 		
	 		$cars = $MC->find("list",array("order"=>"nombre_reducido"));
	 		
	 		 
	 		
	 		$Cleaner = new DataCleaner;
			
	 		foreach ($cars as $k=>$c) {
	 			$e = new CascadeElement;
	 			$e->When = $Cleaner->iso2utf8($parent_value);
	 			$e->Value = $Cleaner->iso2utf8($k);
	 			$e->Text = $Cleaner->iso2utf8($c);
	 			$EL[] = $e;
	 		}
	 		
	 		return $EL;
 			
 		}
 		
 		
 	}
	
	
}