<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


class ConsultaModel extends AppModel {
	
		var $name = "ConsultaModel";
		var $useTable = false;
		var $input_params = array ();
		var $filter_options = array();
		var $cache_param = array(
				'field' => 'ConsultaModel.refresh',
				'type' => 'checkbox',
				'label' => 'Refrescar Cache',
			);
		var $results = array(); //Donde deben quedar los resultados obtenidos con Run();
		var $cacheLifeTime = 0;
		var $cacheId;
		var $cacheTime;
		
		var $session_usr; //Aca debe estar disponible el usuario de la sesion.
				
		//este metodo deberia ser final
		function Run() {
			
			$this->cacheId = $this->_cacheId();
			
			if($this->data["ConsultaModel"]["refresh"] == 1 ) {
				$this->ResetCache();
			}
			
			if(!$this->_readCache()) {
				$data = $this->getData();
				$this->results = array();
				foreach ($data as $row) {
					$this->results[] = $this->mapRow($row);
				}
				
				$this->_saveCache();
			}
			
			return count($this->results);
		}
		
		// abstracto
		function getData() {
			// Debe ser sobreescrita y obtener los datos reales.
		}
		
		// abstracto
		function mapRow($row) {
			// Debe ser sobreescrit para mapear un registro de getData al formato
			// array(Columna => valor)
		}
		
		
		function ViewResults(){
			return "consulta_results";
		}
		
		function ViewInput(){
			return "consulta_input";
		}
		
		// privado
		function _cacheId() {
			$id = md5(implode('-',$this->data[$this->name]).$this->name);
			return $id;
		}
		
		// privado
		function _readCache() {
			
			Cache::set(array('duration' => $this->cacheLifeTime));	
			
			$cacheData = Cache::read($this->cacheId);
			
			if ($cacheData !== false) {
				$this->results = $cacheData['results'];
				$this->cacheTime = $cacheData['time'];
				return true;
			} else {
				return false;
			}
			
		}
		
		// privado
		function _saveCache(){
			
			$this->cacheTime = date("d-M-Y g:i a");
			$cacheData['results'] = $this->results;
			$cacheData['time'] = $this->cacheTime;

			Cache::set(array('duration' => $this->cacheLifeTime));
						
			return Cache::write($this->cacheId,$cacheData);
			
		}
		
		
		function getCacheTime() {
			return $this->cacheTime;
		}
		
		function ResetCache() {
			Cache::delete($this->cacheId);
		}
		
		function getInputParams() {
			return $this->input_params;
		}
		
		function getFilterOptions() {
			return $this->filter_options;
		}
		
		function cascade_parent_change($child_id, $parent_value) { 
			return false;
		}
		
		//utilidad para seleccionar la base de datos segun la facultad.
		function getDbFac($fac){
			
			
			$nros_fac = array();
			if (count($this->session_usr["lista_fac"]) > 0) {
					$lista_con = $this->session_usr["lista_con"];
					$nros_fac = array_keys($this->session_usr["lista_fac"]);
			} else {
				return false;
			}
			
			
			if(trim($fac)!="") {
				//Controla que el usuario realmente tenga acceso a esa fac
				if (!in_array($fac,$nros_fac)) {
					return false;
				}
			} else {
				$fac = $nros_fac[0];
			}
			
			if(isset($lista_con[$fac])) {
				return $lista_con[$fac];
			} else {
				return false;
			}
			
			/*
			switch($fac){
		 			case 2: {
		 				return "quim_guarani";
		 			}
		 			case 3: {
		 				return "mate_guarani";
		 			}
					case 4: {
		 				return "huma_guarani";
		 			}
		 			case 5: {
		 				return "fices_guarani";
		 			}
					case 6: {
		 				return "ipau_guarani";
		 			}
		 			default: {
		 				return false;
		 			}
		 	}
			*/
		}
	
}

?>