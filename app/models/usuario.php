<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


class Usuario extends AppModel {
	
	var $name = "Usuario";
 	var $useTable = "usuarios";
 	
 	var $hasAndBelongsToMany = array(        
 				'Facultad' => array(
 					'className' => 'Facultad',
 	                'joinTable' => 'usuarios_facultades',
 	                'foreignKey'=> 'usuario_id',
 	                'associationForeignKey' => 'facultad_id',
 	                'unique'    => true,
 	                'conditions'=> "Facultad.habilitada = 'S'",
 	                'fields'    => '',
 	                'order'     => '',
 	                'limit'     => '',
 	                'offset'    => '',
 	                'finderQuery'=> '',
 	                'deleteQuery'=> '',
 	                'insertQuery' => ''
 	                )
 	     );
	
 	function afterFind(array $results) {
 		
 		
 		foreach ($results as $k => $r){
 			//Armamos la lista de facultades.
	 		$r["lista_fac"] = array();
			$r["lista_con"] = array();
	 		if(isset($r["Facultad"]) && (count($r["Facultad"])>0)) {
	 			foreach ($r["Facultad"] as $f) {
	 				$r["lista_fac"][$f["id"]] = $f["facultad"];
	 				$r["lista_con"][$f["id"]] = $f["conexion"];	
	 			}	
	 		}
	 		$results[$k] = $r;
 		}
 		
 		return $results;
 		
 	}     
 	     
}
?>