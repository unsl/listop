<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/



class ListaConsultasController extends AppController {
	
	var $name = "ListaConsultas";
	var $uses = array("Consulta", "Listado");
	
	
	function index() {
		
		$usr = $this->Session->read("Usuario");
		
		$sqlG = sprintf ("select cg.consulta_id  from consultasgrupos cg, usuariosgrupos ug, grupos g 
				where cg.grupo_id = ug.grupo_id 
				and ug.grupo_id = g.id
				and g.habilitado = 'S'
				and ug.usuario_id = %s "
				,$usr["Usuario"]["id"]
				);
				
		$condG[] = " habilitada = 'S' ";
		$condG[] = " id IN (" . $sqlG . ") ";
		
		$cond[] = " publica = 'S'";
		$cond[] = " habilitada = 'S'";
		$cond[] = " id NOT IN (" . $sqlG . ") ";
		
		$consultasGrupos = $this->Consulta->find("all",array("conditions"=>$condG));
		$consultas = $this->Consulta->find("all",array("conditions"=>$cond));
		
		
		//Listados del usuario:
		$condL = array(
			"usuario_id" => $usr["Usuario"]["id"],	
		);
		$listados = $this->Listado->find("all", array("conditions"=>$condL, "fields"=>array("id","nombre","descripcion",)));
		
		$this->set("consultasGrupos",$consultasGrupos);
		$this->set("consultas",$consultas);
		$this->set("listados",$listados);
		
	}
	
}

?>