<?php
class VoipController extends AppController {
	

	var $name = "Voip";
	var $uses = array("VoipFacturacion","VoipSumaLlamadas");

	
	function cruzar_facturacion($mes,$anio) {
		ini_set("memory_limit", "512M");
		ini_set("max_execution_time", "600");

		$this->VoipFacturacion->procesar_facturas($mes,$anio);
		
		$this->set('con_factura',$this->VoipFacturacion->llamadas_con_factura);
		$this->set('sin_factura',$this->VoipFacturacion->llamadas_sin_factura);
		$this->set('sin_llamada',$this->VoipFacturacion->facturas_sin_llamada);
		
		$this->render('cruzar_facturacion');
		
	}
	
	
	function agrupar_llamadas($mes, $anio) {
		
		ini_set("memory_limit", "512M");
		ini_set("max_execution_time", "600");
		
		$G = array();
		
		$sql = sprintf("SELECT * from cdr_facturas WHERE periodo_mes = %s and periodo_anio = %s", $mes, $anio);
		$res = $this->VoipFacturacion->query($sql);
		
		foreach ($res as $r) {
			$l = $r['cdr_facturas'];
			
			if(trim($l['src']) == ''){
				continue;
			}
			
			$nro_origen = trim($l['src']);
			
			if (!isset($G[$nro_origen])) {
				$G[$nro_origen] = array(
					'Local' => array (
								'loc_llamadas' => 0,
								'loc_segundos' => 0,
								'loc_monto' => 0,
								),
					'Nacional' => array (
								'nac_llamadas' => 0,
								'nac_segundos' => 0,
								'nac_monto' => 0,
								),
					'Internacional' => array (
								'internac_llamadas' => 0,
								'internac_segundos' => 0,
								'internac_monto' => 0,
								),
					'Celular' => array (
								'cel_llamadas' => 0,
								'cel_segundos' => 0,
								'cel_segundos_cobrados' => 0,
								'cel_monto' => 0,
								),
					'Interno' => array (
								'int_llamadas' => 0,
								'int_segundos' => 0,
								'int_monto' => 0,
								),
					'Otros' => array (
								'otros_llamadas' => 0,
								'otros_segundos' => 0,
								'otros_monto' => 0,
								),
				);
			}
			
			//llamada facturada
			if ($l['clasificacion'] == 'Externa') {
				
				switch ($l['tipo']) {
					case 'L' : { //local
						$G[$nro_origen]['Local']['loc_llamadas']++;
						$G[$nro_origen]['Local']['loc_segundos'] += $l['duracion'];  		
						$G[$nro_origen]['Local']['loc_monto'] += $l['importeml'];
						break;
					}
					case 'N' : { //Nacional
						$G[$nro_origen]['Nacional']['nac_llamadas']++;
						$G[$nro_origen]['Nacional']['nac_segundos'] += $l['duracion'];  		
						$G[$nro_origen]['Nacional']['nac_monto'] += $l['importeml'];
						break;
					}
					case 'I' : { //Internacional
						$G[$nro_origen]['Internacional']['internac_llamadas']++;
						$G[$nro_origen]['Internacional']['internac_segundos'] += $l['duracion'];  		
						$G[$nro_origen]['Internacional']['internac_monto'] += $l['importeml'];
						break;
					}
						 
				}
				
			}
			
			//llamada sin factura
			if ( (trim($l['src'])!= '') && (trim($l['numfactura']) == '') ) {
				
				
				if ($l['clasificacion']=='Celular') { //celular?
					$G[$nro_origen]['Celular']['cel_llamadas']++;	
					$G[$nro_origen]['Celular']['cel_segundos'] += $l['duration'];
					$G[$nro_origen]['Celular']['cel_segundos_cobrados'] += $l['duracion_cobrada'];  		
					//$G[$l['src']]['Celular']['monto'] += $l['importeml'];
					
				} elseif ($l['clasificacion']=='Interna') { // Interno?
					$G[$nro_origen]['Interno']['int_llamadas']++;
					$G[$nro_origen]['Interno']['int_segundos'] += $l['duration'];
				} else {
					$G[$nro_origen]['Otros']['otros_llamadas']++;
					$G[$nro_origen]['Otros']['otros_segundos'] += $l['duration'];
				}
			}
			
			//facturada sin llamada
			if (($l['src']== '') && ($l['numfactura']!= '')) {
				
			}
			
		}
		
		$this->_guardar_suma_llamadas($G, $mes, $anio);
		
		
		echo "<pre>";
		echo "Cant Nros: " . count($G);
		echo "</pre>";
		
		
		echo "<pre>";
		print_r($G);
		echo "</pre>";
		
		
		
		die;
		
	}

	
	function _guardar_suma_llamadas($SL,$mes,$anio) {
		
		$sql = sprintf("DELETE FROM suma_llamadas WHERE periodo_mes = %s and periodo_anio = %s", $mes, $anio );
		$this->VoipSumaLlamadas->query($sql);
		
		$periodo = array(
			'periodo_mes' => $mes,
			'periodo_anio' => $anio,
		);
		
		foreach ($SL as $nro => $resumen) {
			$n = am(
					array('numero' => $nro),
					$resumen['Local'],
					$resumen['Nacional'],
					$resumen['Internacional'],
					$resumen['Celular'],
					$resumen['Interno'],
					$resumen['Otros'],
					$periodo
				);
				
			unset($this->VoipSumaLlamadas->id);
			unset($this->VoipSumaLlamadas->data);
			$this->VoipSumaLlamadas->set($n);
			$this->VoipSumaLlamadas->save(NULL,false);
			
		}
		
	}

}