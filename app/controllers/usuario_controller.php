<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


 class UsuarioController extends AppController
 {

 	var $name = "Usuario";
 	var $uses = array("Usuario");
 	var $components = array("Session", "RequestHandler");
 	var $controlarSesion = false;	

	function index()
	{
		if($this->RequestHandler->isAjax()){
            $this->render("index","ajax");
        } else {
          	$this->render("index","login");
        }
	}

 	/**
 	 * TODO: cargar un plan por defecto para el usuario, o el que tenga asociado.
 	 */
 	function login()
 	{
		if (isset($this->data)) {
			$usuario = $this->data["Usuario"]["usuario"];
			$password = $this->data["Usuario"]["password"];
			
			$cond = array("Usuario.usuario" => $usuario);
			
			$usr = $this->Usuario->find("first",array("conditions"=>$cond));
			
//			print_r($usr);
//			exit;
			
			if ($usr and $usr["Usuario"]["clave"] == $password ) {
				$this->Session->write("Usuario",$usr);
				$this->redirect("/");

			} else {
				$this->set("ERROR", "Usuario o clave incorrecta.");
			}
		}


        //$this->render("login","ajax");
        $this->render("login","login");

 	}

 	function logout()
 	{
		$this->Session->destroy();
		$this->redirect("/");
 	}
			
 	function redireccionar_cliente($cake_url)
 	{
 		$this->set("url",urldecode($cake_url));
 		$this->render("redireccionar_cliente","ajax");
 	}
 	
 	function encabezado() {
 		$usr = $this->Session->read("Usuario");
 		
 		$this->set("usuario",$usr["Usuario"]["usuario"]);
 		$this->render("encabezado","ajax");
 		
 	}


 }


?>