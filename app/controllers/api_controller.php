<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/



class ApiController extends AppController {
	var $name = "Api";
	var $autoRender = false;
	var $uses=array('Usuario','Consulta');
	var $components = array('Security','RequestHandler');
	var $controlarSesion = false;
	   

	var $Usuario; //Los datos de la cuenta del usuario.
	
	function beforeFilter() {        
		$this->Security->loginOptions = array(
		         'type'=>'basic',
		         'realm'=>'UNSL - Estadísticas',
		         'login'=>'authenticate'        
		);        
		
		$this->Security->requireLogin();
		//$this->Security->requireSecure();    
		
	}
	
	
	
	function authenticate($args) {

		$usuario = $args['username'];
		$password = $args['password'];
			
		$cond = array("Usuario.usuario" => $usuario);
			
		$usr = $this->Usuario->find("first",array("conditions"=>$cond));
		$this->Usuario = $usr;
		
		if ($usr and $usr["Usuario"]["clave"] == $password ) {
			return true;
		} else {
			$this->Security->blackHole($this); 
			return false;
		}
		 
	} 
	
	
	function index($params){
		pr($params);
		pr($this->params);
		pr($_REQUEST);
		
		/*
		Pasos para obtener los datos solicitados:
			1) Identificar y cargar la consulta;
			2) Pasar los parametros y opciones de filtro;
			3) Obtener los datos que devuelve la consulta;
			4) Quedarse con la cantidad de datos deseada (pagina solicitada);
			5) Retornar los datos segun formato: JSON, PHP serializado, etc. ;
		*/
		
	}

	
	
	
	function rest($api_name){
		$cond = array("Consulta.apiname"=>$api_name);
		$consulta = $this->Consulta->find("first",array("conditions"=>$cond));
		if ($consulta) {
			$modelName = $consulta["Consulta"]["modelo"];
			$this->_cargarConsulta($modelName);
			$model = & $this->{$modelName};
			$model->session_usr = $this->Usuario;
			
			$format = "json";
			if(isset($_REQUEST["format"])) {
				$format = $_REQUEST["format"];
			}
			
			
			//$p[$modelName]=$_REQUEST;
			//print_r($p);
			$model->set($_REQUEST);
			//print_r($model->data);
			
			if($model->validates()){

				$model->Run();
				$data["results"] = $model->results;
				$data["time"] = $model->getCacheTime();

				$this->set("response",$data);
				
				switch($format) {
					
					case 'html' : {
						$this->render("html","blank");
						break;
					}
					
					case 'php' : {
						$this->RequestHandler->setContent('json', 'text/php');
						$this->render("php","php_serial");
						break;
					}
					
					case 'json' : 
					default :	{
						$this->RequestHandler->setContent('json', 'text/x-json');
						$this->render("json","json");
						break;
					}
					
				}

			} else {
				//Si no valida hay que retornar un arreglo con los errores.	
				//$errors = $model->validate_errors();
			}
			
		} else {
			//Si no hay consulta retornar un 404
			$this->cakeError('error', array('code'=>404));
		}
		
		return false;
		
	}
	
	
	function _cargarConsulta($modelName) {
		$file = Inflector::underscore($modelName).".php";
			
		$path = MODELS."consultas".DS.$file;
		if (file_exists($path)) {
			require_once($path);
		} else {
			
			$un = split("_",$file);
			if (count($un)>1) {
				$prefijo = $un[0];
				$path = MODELS."consultas".DS.$prefijo.DS.$file;
				if (file_exists($path)) {
					require_once($path);
				}	
			} 	
			
		}
			
		if (class_exists($modelName)) {
			$this->loadModel($modelName);
			return true;
		} else {
			return false;
		}
	}
		
	
};