<?php

/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/



App::import('Vendor', 'cake_util/json_response');

class LabController extends AppController {
	
	var $name = "Lab";
	var $uses = array("Listado"); 
	var $controlarSesion = false;
	
	function index() {
		$this->autorender = false;
		return $this->lab();
	}
	
	function lab() {
		
		$usr = $this->Session->read("Usuario");
		
		$condL = array(
			"usuario_id" => $usr["Usuario"]["id"],	
		);
		$listados = $this->Listado->find("all", array("conditions"=>$condL, "fields"=>array("id","nombre","descripcion","campos")));
		
		$this->set("listados",$listados);
		
		$this->render("lab");
		
	
	}
	
	function procesar() {
		$this->autorender = false;
		
		//print_r($this->params["form"]);
		
		$pres = $this->params["form"]["res"];
		$pmas = $this->params["form"]["mas"];
		$pmenos = $this->params["form"]["menos"];
		$pcampos_mas = $this->params["form"]["campos_mas"];
		$pconector_mas  = $this->params["form"]["conector_mas"];
		
		$RES = $pres[0];
		
		$MAS = array();
		if (is_array($pmas)) {
			foreach($pmas as $p) {
				if($p["value"] != -1) {
					list($lista,$campo) = split("_",$p["name"]);
					$MAS[$lista][$campo] = $p["value"];
				}
			}
		}
		
		$MENOS = array();
		if (is_array($pmenos)) {
			foreach($pmenos as $p) {
				if($p["value"] != -1) {
					list($lista,$campo) = split("_",$p["name"]);
					$MENOS[$lista][$campo] = $p["value"];
				}
			}
		}
		
		$CAMPOS_MAS = array();
		if (is_array($pcampos_mas)) {
			foreach($pcampos_mas as $p) {
				//if($p["value"] == 1) {
					list($lista,$campo) = split("_",$p["name"]);
					$CAMPOS_MAS[$lista][$campo] = $p["value"];
				//}
			}
		}
		
		$CONECTOR_MAS = $pconector_mas[0]["value"];
		
		//print_r($MAS);
		//print_r($MENOS);
		if ($CONECTOR_MAS == "JOIN") {
			$LRES = $this->Listado->cruzar_listas_x_join($RES,$MAS,$CAMPOS_MAS, $MENOS);
		} else {
			$LRES = $this->Listado->cruzar_listas($RES,$MAS,$CAMPOS_MAS, $CONECTOR_MAS, $MENOS);	
		}
		
		$respuesta = new JsonResponse();
		if (is_array($LRES) && (count($LRES) > 0)) {
			$this->Session->write('Lab.results', $LRES);
		} else {
			$this->Session->delete('Lab.results');
			$respuesta->status = "ERROR";  
		}
		
		//print_r($LRES);
		$this->set("respuesta",$respuesta, false);
		$this->render("json","ajax");
		
	}
	
	function listado() {
		$R = $this->Session->read('Lab.results'); //Se cae con resultados grandes!!!!
		$results[] = $R[0];
		$this->set("results",$results);
		$this->render("listado","ajax");
	}
	
	function listado_results() {

		$data = $this->Session->read('Lab.results');
		$from = $this->params['form']['iDisplayStart'];
		$len = $this->params['form']['iDisplayLength'];
		$sEcho = $this->params['form']['sEcho'];

		$results = array_slice($data,$from,$len);
		$results = array_map(array_values,$results);

		$response = array (
	   				"sEcho" => (int) $sEcho,
	    			"iTotalRecords" => count($data),
				    "iTotalDisplayRecords" => count($data),
					"aaData" => $results,
					//"aaData" => null,
		);

		$this->set('listado_id',$listado_id);
		$this->set("respuesta",$response);
		$this->render("json","ajax");
	}
	
	
function excel() {

			//ini_set("memory_limit", "280M");
			//ini_set("max_execution_time", "600");

			//Para que funcione pear
	 		ini_set('include_path', APP.'vendors'.DS.'pear'.DS. PATH_SEPARATOR . ini_get('include_path'));

	 		$this->autorender = false;

	 		require_once 'Spreadsheet/Excel/Writer.php';
			//$sesionData = $this->Session->read('Model.results_'.$consulta_id);
			$data = $this->Session->read('Lab.results');
			

			// Creating a workbook
			$workbook = new Spreadsheet_Excel_Writer();
			$workbook->setVersion(8);

			// sending HTTP headers
			$workbook->send('lab.xls');

			// Creating a worksheet
			$worksheet =& $workbook->addWorksheet('Lista Resultado');
			$worksheet->setInputEncoding('UTF-8');
			$format_bold =& $workbook->addFormat();
			$format_bold->setBold();


			$Cleaner = new DataCleaner;
			$i = 0;
			foreach($data[0] as $c => $v) {
				$worksheet->write(0,$i,$Cleaner->iso2utf8($c),$format_bold);
				$i++;
			}

			$f = 1;
	 		foreach ($data as $k=>$r) {
	 			$i = 0;
				foreach ($r as $c=>$v) {
					$worksheet->write($f,$i,$Cleaner->iso2utf8($v));
					$i++;
				}
				$f++;
			}

			$workbook->close();
	}
	
function guardar($consulta_id) {
		
		if (isset($this->data)) {
			$this->Listado->set($this->data["Listado"]);
			if ($this->Listado->validates()) {
				
				$usr = $this->Session->read("Usuario");
				$this->Listado->set("usuario_id",$usr["Usuario"]["id"]);
				$this->Listado->set("origen",'L');
				$this->Listado->set("consulta_id",$consulta_id);
				
				$data = $this->Session->read('Lab.results');
				
				//$fields = array_keys($data["results"][0]);
				$fields = array_keys($data[0]);
				
				$Cleaner = new DataCleaner;
				
				$serial = $Cleaner->base64_serialize(array("results"=>$data));
				$campos = $Cleaner->base64_serialize($fields);
				//echo $serial;
				$this->Listado->set("listado",$serial);
				$this->Listado->set("campos",$campos);
				
				$this->Listado->cleanData = false;
				
				$this->Listado->save();
				$this->render("guardar_ok","ajax");
				return;
			} else {
				
			}
		} else {
			// mostrar el dialogo form 
		}
		
		$this->set("consulta_id", $consulta_id);
		$this->render("form","ajax");
		
		
	}
	
	
}


?>