<?php


/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/



	class ConsultaController extends AppController {
		var $name = "Consulta";
		var $autoRender = false;
		var $uses = array("Consulta");

		function index($consulta_id){
			$this->set("consulta_id",$consulta_id);
			$this->render("consulta");
		}

		function path($consulta_id) {

			$this->Consulta->id = $consulta_id;
			$consulta = $this->Consulta->read();

			$this->set('consulta',$consulta);
			$this->render("path_consulta","ajax");
		}

		function input($consulta_id){
			$vista = "";

			$this->Consulta->id = $consulta_id;
			$consulta = $this->Consulta->read();
			$modelName = $consulta["Consulta"]["modelo"];
			//$this->loadModel($modelName);
			$this->_cargarConsulta($modelName);
			$model = & $this->{$modelName};
			
			//Antes que nada deja disponible el usr de la sesion.
			$model->session_usr = $this->Session->read("Usuario");

			if (isset($this->data[$modelName])){
				//$model->set($this->data[$modelName]);
				$model->set($this->data);

				if($model->validates()){

					$model->Run();
					$data["results"] = $model->results;
					$data["time"] = $model->getCacheTime();

					$this->Session->write('Model.results_'.$consulta_id,$data);
					$vista = $model->ViewInput();
					$this->set('showResults',true);
					$this->set('showNoData',(count($model->results)==0));

				} else {
					$this->set('showResults',false);
				}
			} else {
				$this->set('showResults',false);
			}

			if($model->cacheLifeTime > 0) {
				$this->set('cache_param',$model->cache_param);
			}

			$vista = $model->ViewInput();
			$this->set('consulta_id',$consulta_id);
			$this->set('input_par', $model->getInputParams(), true);
			$this->set('filter_options', $model->getFilterOptions(), true);
			$this->set('consulta',$modelName);
			$this->render($vista, 'ajax');

		}

		//Dibuja el esqueleto de la tabla
		function results($consulta_id){
			$data = $this->Session->read('Model.results_'.$consulta_id);
			$results[] = $data["results"][0];
			$this->set('results',$results);
			$this->set('cachetime',$data["time"]);
			$this->set('consulta_id',$consulta_id);
			$this->render("consulta_results","ajax");
		}


		//Obtiene los datos a mostrar en la tabla
		function dataTableResults($consulta_id) {

			$sesionData = $this->Session->read('Model.results_'.$consulta_id);

			$data = $sesionData["results"];
			$from = $this->params['form']['iDisplayStart'];
			$len = $this->params['form']['iDisplayLength'];
			$sEcho = $this->params['form']['sEcho'];

			$results = array_slice($data,$from,$len);
			$results = array_map(array_values,$results);

			$response = array (
	   				"sEcho" => (int) $sEcho,
	    			"iTotalRecords" => count($data),
				    "iTotalDisplayRecords" => count($data),
					"aaData" => $results,
					//"aaData" => null,
			);

			$this->set('consulta_id',$consulta_id);
			$this->set("response",$response);
			$this->render("dataTableResults","ajax");
		}


		//Genera la descarga de todos los datos en un archivo excel.
		function excel($consulta_id) {

			ini_set("memory_limit", "512M");
			ini_set("max_execution_time", "600");

			//Para que funcione pear
	 		ini_set('include_path', APP.'vendors'.DS.'pear'.DS. PATH_SEPARATOR . ini_get('include_path'));

	 		$this->autorender = false;

	 		require_once 'Spreadsheet/Excel/Writer.php';
			$sesionData = $this->Session->read('Model.results_'.$consulta_id);
			$data = $sesionData["results"];

			// Creating a workbook
			$workbook = new Spreadsheet_Excel_Writer();
			$workbook->setVersion(8);

			// sending HTTP headers
			$workbook->send('consulta.xls');

			// Creating a worksheet
			$worksheet =& $workbook->addWorksheet('Resultados Consulta');
			$worksheet->setInputEncoding('UTF-8');
			$format_bold =& $workbook->addFormat();
			$format_bold->setBold();


			$Cleaner = new DataCleaner;
			$i = 0;
			foreach($data[0] as $c => $v) {
				$worksheet->write(0,$i,$Cleaner->iso2utf8($c),$format_bold);
				$i++;
			}

			$f = 1;
	 		foreach ($data as $k=>$r) {
	 			$i = 0;
				foreach ($r as $c=>$v) {
					$worksheet->write($f,$i,$Cleaner->iso2utf8($v));
					$i++;
				}
				$f++;
			}

			$workbook->close();
		}
		
		function cascade_parent_change($consulta_id,$child_id) { 
			
			$parent_value = $this->params["url"]["val"];
			
			$this->Consulta->id = $consulta_id;
			$consulta = $this->Consulta->read();
			$modelName = $consulta["Consulta"]["modelo"];
			$this->loadModel($modelName);
			$model = & $this->{$modelName};
			//Antes que nada deja disponible el usr de la sesion.
			$model->session_usr = $this->Session->read("Usuario");
			
			$data = $model->cascade_parent_change($child_id, $parent_value);
			
			$this->set("respuesta",$data);
			$this->render("/lab/json","ajax");
			
		}
		
		
		function _cargarConsulta($modelName) {
			$file = Inflector::underscore($modelName).".php";
			
			$path = MODELS."consultas".DS.$file;
			if (file_exists($path)) {
				require_once($path);
			} else {
			
				$un = split("_",$file);
				if (count($un)>1) {
					$prefijo = $un[0];
					$path = MODELS."consultas".DS.$prefijo.DS.$file;
					if (file_exists($path)) {
						require_once($path);
					}	
				} 	
				
			}
			
			if (class_exists($modelName)) {
				$this->loadModel($modelName);
				return true;
			} else {
				return false;
			}
		}
}
?>