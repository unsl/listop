<?php


/*************************************************************************
Copyright (C) 2012 Universidad Nacional de San Luis (UNSL)
Author: Diego Quiroga <diegoq@unsl.edu.ar>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/



class UicontrolController extends AppController {
	
	var $name = "Uicontrol";
	var $uses = array();
	
	function combo_carrera(){
		$this->loadModel("Carrera");
		$carreras = $this->Carrera->find("all");
		$this->set("carreras",$carreras);
	}
	
	function combo_plan($carrera){
		$this->loadModel("Plan");
		$planes = $this->Plan->find("all",array("conditions"=>array("carrera"=>$carrera)));
		$this->set("planes",$planes);
	}
		
}


?>