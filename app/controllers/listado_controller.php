<?php 

class ListadoController extends AppController {
		
	var $name = "Listado";
	var $uses = array("Listado"); 
	
	function guardar($consulta_id) {
		
		if (isset($this->data)) {
			$this->Listado->set($this->data["Listado"]);
			if ($this->Listado->validates()) {
				
				$usr = $this->Session->read("Usuario");
				$this->Listado->set("usuario_id",$usr["Usuario"]["id"]);
				$this->Listado->set("origen",'C');
				$this->Listado->set("consulta_id",$consulta_id);
				
				$data = $this->Session->read('Model.results_'.$consulta_id);
				
				$fields = array_keys($data["results"][0]);
				
				//$ds = serialize($data);
				//$d = unserialize($ds);
				
				$Cleaner = new DataCleaner;
				$serial = $Cleaner->base64_serialize($data);
				$campos = $Cleaner->base64_serialize($fields);
				
				//echo $serial;
				$this->Listado->set("listado",$serial);
				$this->Listado->set("campos",$campos);
				
				$this->Listado->cleanData = false;
				
				$this->Listado->save();
				$this->render("guardar_ok","ajax");
				return;
			} else {
				$this->render("guardar_error","ajax");
				return;
			}
		} else {
			// mostrar el dialogo form 
		}
		
		$this->set("consulta_id", $consulta_id);
		$this->render("form","ajax");
		
		
	}
	
	
	function borrar($listado_id) {
		
		$usr = $this->Session->read("Usuario");
		$cond =  array(
			"id" => $listado_id,
			"usuario_id" => $usr["Usuario"]["id"]
		);
		
		$this->Listado->deleteAll($cond);
		
		$this->lista_usuario();
	}
	
	
	function ver($listado_id) {

		$listado = $this->Listado->read(null, $listado_id);
		$serial = $listado["Listado"]["listado"];
		
		//$datos = unserialize($serial);
		$Cleaner = new DataCleaner;
		$datos = $Cleaner->base64_unserialize($serial);
		
		//echo $serial;
		$this->Session->write('Listado.results_'.$listado_id, $datos);
		
		$results[] = $datos["results"][0];
		
		$this->set("listado_id",$listado_id);
		$this->set('results',$results);
		$this->render("listado_results","ajax");
		
	}
	
	function dataTableResults($listado_id) {

		$sesionData = $this->Session->read('Listado.results_'.$listado_id);

		$data = $sesionData["results"];
		$from = $this->params['form']['iDisplayStart'];
		$len = $this->params['form']['iDisplayLength'];
		$sEcho = $this->params['form']['sEcho'];

		$results = array_slice($data,$from,$len);
		$results = array_map(array_values,$results);
		
		$response = array (
	   				"sEcho" => (int) $sEcho,
	    			"iTotalRecords" => count($data),
				    "iTotalDisplayRecords" => count($data),
					"aaData" => $results,
					//"aaData" => null,
		);

		$this->set('listado_id',$listado_id);
		$this->set("response",$response);
		$this->render("dataTableResults","ajax");
	}
	
	function lista_usuario() {
		
		$usr = $this->Session->read("Usuario");
		$condL = array(
			"usuario_id" => $usr["Usuario"]["id"],	
		);
		$listados = $this->Listado->find("all", array("conditions"=>$condL, "fields"=>array("id","nombre","descripcion","created"), "order"=>array("Listado.created DESC")));
		$this->set("listados",$listados);
		
		$this->render("lista_usuario","ajax");
	}
	
	
	function excel($listado_id) {

			//ini_set("memory_limit", "280M");
			//ini_set("max_execution_time", "600");

			//Para que funcione pear
	 		ini_set('include_path', APP.'vendors'.DS.'pear'.DS. PATH_SEPARATOR . ini_get('include_path'));

	 		$this->autorender = false;

	 		require_once 'Spreadsheet/Excel/Writer.php';
			//$sesionData = $this->Session->read('Model.results_'.$consulta_id);
			$sesionData = $this->Session->read('Listado.results_'.$listado_id);
			$data = $sesionData["results"];

			// Creating a workbook
			$workbook = new Spreadsheet_Excel_Writer();
			$workbook->setVersion(8);

			// sending HTTP headers
			$workbook->send('consulta.xls');

			// Creating a worksheet
			$worksheet =& $workbook->addWorksheet('Resultados Consulta');
			$worksheet->setInputEncoding('UTF-8');
			$format_bold =& $workbook->addFormat();
			$format_bold->setBold();


			$Cleaner = new DataCleaner;
			$i = 0;
			foreach($data[0] as $c => $v) {
				$worksheet->write(0,$i,$Cleaner->iso2utf8($c),$format_bold);
				$i++;
			}

			$f = 1;
	 		foreach ($data as $k=>$r) {
	 			$i = 0;
				foreach ($r as $c=>$v) {
					$worksheet->write($f,$i,$Cleaner->iso2utf8($v));
					$i++;
				}
				$f++;
			}

			$workbook->close();
		}
	
}
?>