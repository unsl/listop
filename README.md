# Listop: Generaci�n y Operaci�n de Listados #



### Motivaci�n ###


Ante la diversidad de pedidos de datos y estad�sticas desde los diversos 
�mbitos de la UNSL, surge la necesidad de unificar esfuerzos y criterios, 
tratando de reutilizar las soluciones existente, y de combinarlas para 
generar nuevas soluciones, evitando reprogramar una y otra vez consultas 
muy similares. Se plantea entonces la idea de implementar un sistema que 
centralice las consultas y facilite su programaci�n, parametrizaci�n y 
rehuso, unificando adem�s la interfaz del usuario y la forma en que se 
obtienen los resultados, permitiendo descargarlos y combinarlos en forma de 
listados.

### Objetivos del Sistema


* Centralizar las consultas
* Parametrizarlas, permitiendo reutilizarlas en pedidos similares
* Unificar los criterios a tener en cuenta para la obtenci�n de datos
* Facilitar la generaci�n, extraci�n, y combinaci�n de datos
* Evolucionar a una herramienta de utilidad para quienes necesitan los datos, 
para que puedan obtenerlos independientemente de los administradores de los sistemas



## 1 - REQUISITOS DEL SISTEMA:


Apache 2, con mod-rewrite habilitado.
PHP 5.3+ , con PDO_INFORMIX funcionando (no es necesario si no hay accesos a bases informix).
MySQL 5.3+


## 2 - INSTALACION BASICA:


1) Copiar la carpeta "listop" al directorio ra�z de apache.

2) Configuraci�n de la base de datos y las conexiones:
        
        - Abrir una sesi�n MySql y crear una base de datos en blanco.
        
        - importar la base de datos de db_model/listop.sql
         
                Ej: mysql -u user -p listop < listop.sql
        
        - Abrir el archivo app/config/database.php y configurar la conexi�n a la 
        base reci�n creada.
        
        Habilitar el modulo mod_rewrite en el server apache
        y configurar el apache para que tome los parametros del .htaccess del directorio listop

3) OPCIONAL: Acceso a los sistemas guarani:

        Configurar en el archivo app/config/database.php las distintas conexiones 
        a los sistemas guarani.
        
        
4) Abrir el navegador web, acceder al sistema (http://dominio/listop) 
e identificarse con el usuario y pass "admin" y "admin"



## 3 - USUARIOS Y CONEXIONES ADICIONALES:


- los usuarios del sistema estan registrados en la base del sistema, en 
        la tabla usuarios, se agregan usuarios adicionales.        
- La configuraci�n de las facultades esta en la tabla facultades, y ahi 
        mismo se configura el nombre de la conexi�n que se utiliza. Este 
        nombre es el que esta definido en app/config/database.php.
        



## 4 - CONCEPTOS GENERALES FUNCIONAMIENTO Y PROGRAMACION 


### Instalaci�n y Configuraci�n


El sistema esta implementado con CakePHP 1.3 (www.cakephp.org). Hay que 
configurar adecuadamente la directiva RewriteBase en los archivos 
/.htaccess, /app/.htaccess, y /app/webroot/.htaccess .- Si el sistema esta 
instalado en el document root de apache, esta directiva se debe comentar.

La configuraci�n de la conexion a la base de datos del sistema se indica en 
el archivo /app/config/database.php. Tambien en este archivo se deben 
configurar las conexiones a los distintos sistemas Guaran�, y cualquier 
base de datos que se utilice como fuente de datos en alguna consulta.

Para poder acceder al sistema, hay que estar registrado como usuario en la 
tabla usuarios, y si el usuario necesita acceder a consultas que utilizan 
datos de las bases Guarani, deber� relacionarse el usuario con las 
facultades en la tabla usuarios_facultades.


## Conceptos Generales


*        Consultas: Las consultas son la fuente primaria de datos. Para obtener 
un conjunto de datos, primero hay que ejecutar alguna consulta, 
configurando adecuadamente sus par�metros. Es la consulta la que accede a 
la fuente real de los datos, que podr� ser una base de datos (por ej. sistema 
Guaran�) o cualquier otra. Los resultados se devuelven en forma de listado, 
que se visualiza en pantalla, y se puede descargar o guardar en una lista.

*        Listas o Listados de Usuario: Los listados, son resultados (datos) 
est�ticos que se obtuvieron de una consulta, de un archivo excel o de la combinaci�n de varios 
listados. Son propios de cada usuario, solo el usuario que los genera los 
puede ver por pantalla, descargarlos o manipularlos para combinarlos con 
otros datos.


*        Combinaci�n de Listados: El sistema cuenta con funcionalidad para 
combinar varios listados. Un listado principal, se puede .filtrar. y 
.aumentar. con otros listados. El mecanismo permite filtrar elementos de la 
lista principal, dependiendo de si se encuentra o no en alguna otra lista. 
Adem�s, si esta presente en alguna otra lista, el elemento se puede 
enriquecer con datos de las otras listas.

(mas adelante hay ejemplos de estos puntos)


## Programaci�n de Consultas


Desde el punto de vista de la programaci�n, una consulta es un Modelo de 
datos tal como se define en CakePHP, con la particularidad de que debe 
heredar de la clase ConsultaModel y redefinir algunos m�todos puntuales. 
Los pasos m�nimos son:

1. Crear un Modelo que herede de ConsultaModel. 
2. Definir los par�metros. 
3. Implementar (como m�nimo) los m�todos getData() y mapRow($row). 
4. Registrar la consulta en la base de datos: tabla consultas. 

EL nombre del archivo debe seguir las convenciones de nombre del framework 
(CakePHP) y para mantener ordenado el sistema, los modelos de consultas se 
deben poner en el directorio app/models/consultas/

A su vez este directorio puede tener subdirectorios que agrupen consultas 
segun el prefijo del Modelo. Por ejemplo, si quisieramos agrupar todas las 
consultas de becas, creamos el directorio app/models/consultas/becas/ y 
dentro del directorio ponemos los archivos con nombre becas_xxx.php . 
Siguiendo la idea de CakePHP, dentro de este archivo debe estar definida la 
clase BecasXxx.

Como norma, hay que tratar de que las consultas devuelvan siempre alg�n 
dato de utilidad para combinar con listas de otras consultas. Por ejemplo, 
cuando hacemos consultas sobre alumnos o cosas relacionadas con alumnos, 
devolver siempre el Legajo y el Documento.

### API


El API da la posibilidad de que otros sistemas accedan a los datos que 
genera una consulta haciendo una llamada remota v�a HTTP. Todas las 
consultas que est�n definidas en el sistema tienen acceso automatico 
mediante el API sin ninguna programaci�n adicional (solo hay que ponerle un 
nombre de api en la bd).


### Mantenimientos Periodicos 


Custiones a tener en cuenta son:

Principalmente la configuraci�n de las conexiones a las bases de donde se 
obtiene los datos. Archivo app/config/database.php

Mantener actualizadas las consultas, y coordinar el agregado de nuevas 
consultas.

Actualizar la configuraci�n de los usuarios: A que consultas acceden, en 
que grupos est�n, etc.


## Mejoras Previstas


Redise�ar la interface de usuario mejorando la usabilidad y amigabilidad.

Autenticar al usuario con xmlrpc, openid, etc.

Implementar funcionalidades para el manejo de las listas de usuario: 
borrarlas, renombrarlas, ordenarlas, etc.

Terminar de DEFINIR y documentar API: cache, estructuras de datos a 
retornar, paginado de datos, par�metros de control, etc.

Ir agregando nuevas operaciones para combinar listas: Por ejemplo serian de 
utilidad la UNION, si se pudiera algun tipo de DIVISION, etc.

Guardar los parametros de las consultas con que se genero una lista, para  poder regenerarla

Listas compartidas entre usuarios (solo el owner la puede borrar, read only y recompartir para el resto)

API sobre listas: poder acceder a las listas mediante API.

En la interface generar una ayuda de API con la URL y todos los parametros

Wizard para generar la consulta: insert de consulta en el mysql sanidad de colision de nombres generar codigo php del modelo etc.

Evaluar alguna forma de usar expresiones arbritrarias para el matching, si no hay compatibilidad, siempre las puede preprocesar en excel y reimportar

Registro automatico de nuevas consultas: deteccion automatica de un nuevo archivo con la definicion de una consulta y se encarge de configurar la base para que todo funcione.

Incorporar un modo debug para que al momento de usarlo nos permitiera ver la/s consulta/s ejecutadas en la base.


# GUIA Y EJEMPLOS :


## Combinaci�n de listas desde las consultas de ejemplo:

**Objetivo:** Lista de clientes que compraron autos y  tambien camiones, pero que no compraron una moto.

1. Ir a la consulta de ejemplo Pedidos, seleccionar como filtro L�nea Prod. = Classic Cars.
2. Ejecutar la consulta, guardar el resultado con nombre cli-autos.
3. Ejecutar nuevamente la consulta, pero poniendo como filtro L�nea Prod. = Trucks and Buses y guardar con nombre cli-camion.
4. Ejecutar nuevamente la consulta, pero poniendo como filtro L�nea Prod. = Motorcycles y guardar con nombre cli-moto.
5. Ejecutar la consulta Clientes sin filtro, y guardar con nombre clientes. 

Hasta aca tenemos tres listados con los clientes que compraron autos, los que compraron camiones, y los que compraron motos.

6. Volver a consultas, y en la solapa Listados, acceder a Lab (boton Lab)
7. Arrastrar la lista clientes a al cuadrante "Principal".
8. Arrastrar la lista cli-autos y cli-camion al cuadrante "Que este en"
9. Desplegar las dos ultimas listas y donde esta el campo customerNumber seleccionar en el combo "customerNumber" (El combo tiene los campos de la lista principal, y lo que hacemos es definir que campos va a comparar para comparar elementos de la lista principal)
10. Arrastrar la lista cli-moto al cuadrante "Que no este en estas listas" igualando customerNumber con customerNumber (combo).
11. Ejecutar, y se genera la lista buscada. Esta lista se puede volver a guardar para seguir combinando con otras listas. 

-----------------------------------------------------


#UNSL 

Ejemplos para:

* Ejecutar una consulta y guardar los resultados en una lista o descargarlos en un xls.
* Combinar listas. (and, or, menos).
* Subir un archivo propio y combinarlo con otras listas.
* Descargar un resultado y cruzarlo con excel (ooffice).
* Programar una consulta.
* Acceso a datos mediante el API.

### Alumnos que tienen aprobada la materia Programaci�n II de la Lic. en Cs. de la Comp. desde el a�o 2010

1) Buscar c�digo de la materia: Materias -> plan vigente.  (cod. mat. 204)

2) Buscar aprobados: Ex�menes -> materia 204, lectivo desde 2010, resultado 
aprobado.

3) Guardar resultados en una lista: apro_prog2.

4) Buscar todos los alumos de Comp.:Alumnos -> regulares, computacion. y 
guardar la lista: alu_comp.

5) Combinamos lista de alumnos restringiendo a la de regulares: agregar al 
resultado la fecha y la nota.


### Que tengan aprobada Programacion II y no tengan aprobado Algebra II.

1) Buscar aprobados Algebra II: Ex�menes -> cod mat 16, lectivo desde 2010, resultado aprobado.

2) Guardar los resultados en apro_calculo2.

3) Combinar las tres listas: (alu_comp IN apro_prog2) AND (alu_comp NOT IN apro_calculo2)  


### Lista de todos los alumnos que tienen regularizada o aprobada la materia Programaci�n II de la Lic. en Cs. de la Comp. (usando la anterior)

1) Buscar regularidades vigentes: Cursadas -> Aprobado o Promocion con reg 
desde fecha actual. Guardar la lista: reg_prog2.

2) Combinar las tres listas  (alu_comp IN apro_prog2) OR (alu_comp IN 
reg_prog2).  Incluir fecha reg, nota aprob y fecha aprob.


### Subir un archivo excel con  lista arbitraria de alumnos (de computacion) y combinarlo con otra lista para obtener mas informaci�n.

1) Subir Archivo: Consulta Excel -> Ejecutar y guardar los resultados en 
alu_xls.

2) Combinar la lista con la de alumnos de la carrera alu_comp para sacar 
promedio, mail, etc.


###Cantidad de materias aprobadas por a�o, de los alumnos de la lista anterior (obtenida desde el archivo) ayudandonos con el OOffice (o Excel)  y el manejo de tablas pivot.

1) Obtener todos los examenes aprobados en Cs. de la Comp.: Ex�menes->resultado: aprobado, lectivo desde: 2010. ->Guardar resultados.

2) Acotar el listado con alu_xls y descargar el resultado en formato xls.

3) Armar la tabla pivot en OOffice (o Excel). 


## Programaci�n de una consulta b�sica:

1) Crear un Modelo que herede de ConsultaModel.

2) Definir los par�metros.

3) Implementar (como m�nimo) los m�todos getData() y mapRow($resu).

4) Registrar la consulta en la base de datos: tabla consultas.

**Ejemplo:**


	class BecasAdjudicadas extends ConsultaModel {
        
        var $name = "BecasAdjudicadas";
        var $cacheLifeTime = 0; //sin cache
        var $useDbConfig = "becas";
        
        var $filter_options = array (
                        
                        array(
                                'field' => 'BecasAdjudicadas.tipo_alu',
                                'type' => 'select',
                                 'size' => 1,
                                'label' => 'Tipo Solicitante',
                                'tip'  => '',
                'options' => array('T'=>'Todos', 'I'=>'Ingresante','N'=>'No Ingresante' ),

	function getData() {

                 $sql = "select 
                                
                                s.tipo_solicitante, s.nro_documento, s.apellido, s.nombre, s.email 
                                ,ba.fecha, b.nombre as beca, e.nombre as estado 
                                
                                from solicitudes s, becas_adjudicadas ba, becas b, estados_becas e
                                where s.solicitud = ba.solicitud
                                and ba.beca = b.beca
                                and ba.estado_beca = e.estado_beca
                                
                                %s
                                
                                order by s.apellido, s.nombre
                                
                                ";
                 
                 
                 $extra = "";
                 
                if (trim($this->data['BecasAdjudicadas']['tipo_alu']) != "T") {
                         $extra .= sprintf(" AND s.tipo_solicitante = '%s' ", $this->data['BecasAdjudicadas']['tipo_alu']);
                 }
        
	$sql = sprintf($sql,$extra);
                 $res = $this->query($sql);
                 
                return $res;
	}

	function mapRow($row) {
                 
                 $r = array (
                         "Apellido" => $row['s']['apellido'],
                         "Nombre" => $row['s']['nombre'],
                         "Documento"=> $row['s']['nro_documento'],
                         "Tipo Sol."=> $row['s']['tipo_solicitante'] == 'N' ? "No Ingresante" : "Ingresante" ,
                         "Fecha"=> $row['ba']['fecha'],
                         "Beca" => $row['b']['beca'],
                         "Estado" => $row['e']['estado'],
                 );
                 
                 return $r;
	}



## Acceso al API usando php y curl:


### Consideraciones: 

URL de acceso al api de la forma <ruta al sistema>/api/<consulta>?par1=val1&par2=val2 ...

o si es por POST, url de la forma 
<ruta al sistema>/api/<consulta> 
y todos los parametros seteados en la petici�n post. 

<ruta al sistema> es donde se encuentre el sistema o mapedo el api: Ej. localhost/listop

<consulta> es el nombre definido para el api que tiene la consulta: se 
define en el campo .apiname. de la tabla .consultas..

La autenticaci�n es con HTTP AUTH (los mismos usuarios del sistema). 
Mediante el api el usuario tiene acceso a las mismas consultas que tiene 
acceso mediante la web.

Los resultados se retornan en formato JSON o PHP serializado. 
Ejemplo PHP de acceso via API:



	function urlRequest($url) {

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL,            $url);
        curl_setopt($ch, CURLOPT_HEADER,         0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "usuario:clave");

        
        $data = curl_exec($ch); 
        $info_http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        $content_type = curl_getinfo($ch,CURLINFO_CONTENT_TYPE);
        curl_close($ch); 

        if($data){
                return array('data'=>$data, 'http_code'=>$info_http_code,'content_type'=>$content_type);
        } else {
                return false;
        }
	}

	$r = urlRequest('http://localhost/listop/api/siu_carreras?facultad=3&estado=T&format=php');


	echo "<h1>Respuesta</h1>";
	echo '<pre>';
	print_r($r);
	echo '</pre>';


	echo "<h1>Datos decodificados:</h1>";
	echo '<pre>';
        if ($r['http_code']== 200) { //OK
                print_r(unserialize($r['data']));
                //print_r(json_decode($r['data']));
        }
	echo '</pre>';


### Copyright (C) 2012 Universidad Nacional de San Luis (UNSL) <listop@unsl.edu.ar>
### Author: Diego Quiroga <diegoq@unsl.edu.ar>
